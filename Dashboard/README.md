# HivMonitorDashboard

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.


# Local SetUp and Run
```
git clone https://inst-arch.visualstudio.com/HivMonitor/_git/Dashboard
cd Dashboard
npm i
ng serve -o
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.


## Configurations

Run `--configuration=prod` to build/run the project in *production* environment configuration.
Run `--configuration=test` to build/run the project in *test* environment configuration.
Run `--configuration=dev` to build/run the project in *development* environment configuration.


## Folder Structure

App folder structure are base in this article by *Mathis Garberg*:
https://itnext.io/choosing-a-highly-scalable-folder-structure-in-angular-d987de65ec7


## Styles

CSS styles are developed with SCSS as preprocessing language.
Naming convention used for class names is BEM convention:
- .block
- .block__element
- .block__element--modificator

More info: http://getbem.com/naming/


## SVG ICON SET
To add svg icons to default icon set:
1- Edit file \src\assets\icons\icon-set.svg
2- Add SVG marckup add the end of the file with an unique id.
```
  <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" id="[UNIQUE_ID]">
    <path fill="currentColor" d="..."></path>
    ...
  </svg>
```
3- Icon will be accesible using SvgIconComponent selector:
```
<app-svg-icon svgIcon="[UNIQUE_ID]"></app-svg-icon>
```
