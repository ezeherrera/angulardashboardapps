// import { GoogleAnalyticsVendorService } from '../app/google-analytics/google-analytics-vendor.service';

// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.service.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  name: 'test',
  localeFileUrl: '/locale/messages.${locale}.xlf',
  settingsFileUrl: '/assets/settings.json',
  googleAnalytics: {
    userId: 'UA-49811334-9'
  },
  // googleAnalyticsServiceType: GoogleAnalyticsVendorService,
  clientId: 'ReportingDashboard',
  redirectUri: 'https://staging-dashboard.projectschweitzer.net/callback',
  issuer: 'https://identitystaging.projectschweitzer.net',
  apiReportingServiceUrl: 'https://staging-reporting.projectschweitzer.net',
  responseType: 'id_token code',
  scope: 'openid',
  responseMode: 'fragment',
  state: '75BCNvRlEGHpQRCT',
  nonce: 'nonce',
  disableAtHashCheck: true,
  requireHttps: false,
  showDebugInformation: true,
  version: require('../../package.json').version,
};
