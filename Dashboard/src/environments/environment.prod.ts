export const environment = {
  production: true,
  name: 'production',
  settingsFileUrl: '/assets/settings.json',
  localeFileUrl: '/locale/messages.${locale}.xlf',
  googleAnalytics: {
    userId: 'UA-135241-9'
  },

  clientId: 'ReportingDashboard',
  redirectUri: 'https://dashboard.projectschweitzer.net/callback',
  issuer: 'https://identity.projectschweitzer.net',
  apiReportingServiceUrl: 'https://reporting.projectschweitzer.net',
  responseType: 'id_token code',
  scope: 'openid',
  responseMode: 'fragment',
  state: '75BCNvRlEGHpQRCT',
  nonce: 'nonce',
  disableAtHashCheck: true,
  requireHttps: false,
  showDebugInformation: true,
  version: require('../../package.json').version,
};
