import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../core/guards/auth.guard';
import { PgCigaretteChartComponent } from './pages/cigarette-chart/pg-cigarette-chart.component';
import { PgLeveyJenningsComponent } from './pages/levey-jennings-chart/pg-levey-jennings-chart.component';
import { PgLineChartComponent } from './pages/line-chart/pg-line-chart.component';
import { PgPieChartComponent } from './pages/pie-chart/pg-pie-chart.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { IconsComponent } from './pages/icons/pg-icons.component';
import { PgGaugeChartComponent } from './pages/gauge-chart/pg-gauge-chart.component';

const routes: Routes = [
  /* PENDENT */
  { path: 'playground',
    canActivate: [AuthGuard],
    children: [
      { path: '',
        component: PlaygroundComponent,
      },
      { path: 'cigarette',
        component: PgCigaretteChartComponent,
      },
      { path: 'levey-jennings',
        component: PgLeveyJenningsComponent,
      },
      { path: 'pie',
        component: PgPieChartComponent,
      },
      { path: 'line',
        component: PgLineChartComponent,
      },
      { path: 'icons',
        component: IconsComponent,
      },
      { path: 'gauges',
        component: PgGaugeChartComponent,
      },
      { path: '**', redirectTo: '', pathMatch: 'full' },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class PlaygroundRoutingModule {}
