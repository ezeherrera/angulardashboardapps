import { Component, OnInit } from '@angular/core';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';

@Component({
  selector: 'app-pg-cigarette-chart',
  templateUrl: './pg-cigarette-chart.component.html',
  styleUrls: ['./pg-cigarette-chart.component.scss'],
})
export class PgCigaretteChartComponent implements OnInit {

  dataset: object;

  constructor(
    private palette: ChartsPaletteService,
  ) {}

  ngOnInit(): void {

    const colors: string[] = this.palette.getColors(5);

    /* MOCK DATA */
    const legend = {
      captions: [
        {
          id: 0,
          caption: 'Sample Transfer',
          color: colors[0],
        },
        {
          id: 1,
          caption: 'Transit',
          color: colors[1],
        },
        {
          id: 2,
          caption: 'Sample Prep',
          color: colors[2],
        },
        {
          id: 3,
          caption: 'Amplification Detection',
          color: colors[3],
        },
        {
          id: 4,
          caption: 'Calculation',
          color: colors[4],
        },
      ]
    };

    const xAxisLabel = 'Time in hours';

    const data = [
      // device 1
      {
        sections:
        [
          {
            caption: 'Sample Transfer',
            label: '0h 35m',
            value: 0.59,
            fill: colors[0],
          },
          {
            caption: 'Transit',
            label: '2h 15m',
            value: 2.25,
            fill: colors[1],
          },
          {
            caption: 'Sample Prep',
            label: '7h 0m',
            value: 5.01,
            fill: colors[2],
          },
          {
            caption: 'Amplification Detection',
            label: '5h 04m',
            value: 8.08,
            fill: colors[3],
          },
          {
            caption: 'Calculation',
            label: '15h 08m',
            value: 15.15,
            fill: colors[4],
          },
        ],
        title: 'Cobas 4800'
      },

      // device 2
      /* {
        sections:
        [
          {
            caption: 'Sample Transfer',
            label: '1h 0m',
            value: 1.00,
            fill: colors[0],
          },
          {
            caption: 'Transit',
            label: '3h 15m',
            value: 3.25,
            fill: colors[1],
          },
          {
            caption: 'Sample Prep',
            label: '5h 06m',
            value: 5.10,
            fill: colors[2],
          },
          {
            caption: 'Amplification Detection',
            label: '5h 04m',
            value: 8.08,
            fill: colors[3],
          },
          {
            caption: 'Calculation',
            label: '10h 0m',
            value: 10.00,
            fill: colors[4],
          },
        ],
        title: 'Sample PrepAmplification Detection'
      },

      // device 3
      {
        sections:
        [
          {
            caption: 'Sample Transfer',
            label: '2h 30m',
            value: 2.5,
            fill: colors[0],
          },
          {
            caption: 'Transit',
            label: '4h 15m',
            value: 4.25,
            fill: colors[1],
          },
          {
            caption: 'Sample Prep',
            label: '6h 45m',
            value: 6.75,
            fill: colors[2],
          },
          {
            caption: 'Amplification Detection',
            label: '7h 00m',
            value: 7.00,
            fill: colors[3],
          },
          {
            caption: 'Calculation',
            label: '8h 36m',
            value: 8.60,
            fill: colors[4],
          },
        ],
        title: 'Cobas 6800/8800'
      }, */
    ];

    this.dataset = {
      data,
      legend,
      xAxisLabel,
    };
  }
}
