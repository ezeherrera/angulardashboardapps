import { Component, OnInit } from '@angular/core';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';
import { ChartsLegendModel } from '../../../../shared/charts/legend/models/legend.model';
import { ChartsPieModel } from '../../../../shared/charts/pie/models/pie.model';

@Component({
  selector: 'app-pg-pie-chart',
  templateUrl: './pg-pie-chart.component.html',
  styleUrls: ['./pg-pie-chart.component.scss'],
})
export class PgPieChartComponent implements OnInit {

  dataset: {
    legend: ChartsLegendModel,
    data: ChartsPieModel[],
    isDonut?: boolean,
    title?: string,
    legendInColumns?: boolean,
  };

  constructor(
    private palette: ChartsPaletteService
  ) {}

  ngOnInit(): void {

    /* MOCK DATA */
    const colors: string[] = this.palette.getColors(5);


    /* MOCK DATA */
    const legend = {
      captions: [
        { id: 0,
          caption: 'Pre Analytics',
          color: colors[0],
        },
        { id: 1,
          caption: 'Transit',
          color: colors[1],
        },
        { id: 2,
          caption: 'CAP',
          color: colors[2],
        },
        { id: 3,
          caption: 'CTM',
          color: colors[3],
        },
        { id: 4,
          caption: 'Time to Release',
          color: colors[4],
        },
      ]
    };

    const data = [
      { id: 1,
        caption: 'Pre Analytics',
        label: '0h 35m',
        value: 0.59,
        fill: colors[0],
      },
      { id: 2,
        caption: 'Transit',
        label: '2h 15m',
        value: 2.25,
        fill: colors[1],
      },
      { id: 3,
        caption: 'CAP',
        label: '7h 0m',
        value: 5.01,
        fill: colors[2],
      },
      { id: 4,
        caption: 'CTM',
        label: '5h 04m',
        value: 8.08,
        fill: colors[3],
      },
      { id: 5,
        caption: 'Time to Release',
        label: '15h 08m',
        value: 15.15,
        fill: colors[4],
      },
    ];

    this.dataset = {
      data,
      isDonut: true,
      title: 'Sample Pie Chart Title',
      legendInColumns: true,
      legend,
    };
  }
}
