import { Component, OnInit } from '@angular/core';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';
import { ChartsGaugeModel } from '../../../../shared/charts/gauge/models/gauge.model';

@Component({
  selector: 'app-pg-gauge-chart',
  templateUrl: './pg-gauge-chart.component.html',
  styleUrls: ['./pg-gauge-chart.component.scss'],
})
export class PgGaugeChartComponent implements OnInit {

  dataset: ChartsGaugeModel[];

  constructor(
    private palette: ChartsPaletteService
  ) {}

  ngOnInit(): void {

    /* MOCK DATA */
    const colors: string[] = this.palette.getColors(5);

    this.dataset = [
      {
        title: 'Simple Gauge Chart',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '125',
            value: 125,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Multiple Gauge Chart',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '95',
            value: 95,
            fill: colors[2],
          },
          { id: 2,
            caption: 'Not detected',
            label: '30',
            value: 30,
            fill: colors[8],
          },
          { id: 3,
            caption: 'Failed',
            label: '8',
            value: 8,
            fill: colors[5],
          },
        ],
      },
      {
        title: 'Huge Sections Gauge Chart',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '55',
            value: 55,
            fill: colors[2],
          },
          { id: 2,
            caption: 'Not Detected',
            label: '35',
            value: 35,
            fill: colors[8],
          },
          { id: 1,
            caption: 'Interrupt',
            label: '30',
            value: 30,
            fill: colors[1],
          },
          { id: 1,
            caption: 'Canceled',
            label: '20',
            value: 20,
            fill: colors[10],
          },
          { id: 2,
            caption: 'Corrupt',
            label: '15',
            value: 15,
            fill: colors[11],
          },
          { id: 3,
            caption: 'Failed',
            label: '8',
            value: 8,
            fill: colors[5],
          },
        ],
      },
      {
        title: 'Half Gauge Chart',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '125',
            value: 125,
            fill: colors[2],
          },
        ],
        halfCircle: true,
      },
      {
        title: 'Multiple Half Gauge Chart',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '95',
            value: 95,
            fill: colors[2],
          },
          { id: 2,
            caption: 'Not Detected',
            label: '30',
            value: 30,
            fill: colors[8],
          },
          { id: 3,
            caption: 'Failed',
            label: '8',
            value: 8,
            fill: colors[5],
          },
        ],
        halfCircle: true,
      },
    ];
  }
}
