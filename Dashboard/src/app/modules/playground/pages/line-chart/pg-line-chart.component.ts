import { Component, OnInit } from '@angular/core';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';
import { ChartsLineGraphDataModel, ChartsLineGraphModel } from '../../../../shared/charts/line/models/line.model';
import MOCKDATA from './mock';

@Component({
  selector: 'app-pg-line-chart',
  templateUrl: './pg-line-chart.component.html',
  styleUrls: ['./pg-line-chart.component.scss'],
})
export class PgLineChartComponent implements OnInit {

  dataset: ChartsLineGraphModel;
  legend: object;

  constructor(
    private palette: ChartsPaletteService,
  ) {}

  ngOnInit(): void {

    const fetchedData = MOCKDATA;

    const { maxValue,
            minValue,
            startDate,
            endDate,
          } = fetchedData;
    const startDateObj = new Date(startDate);
    const endDateObj = new Date(endDate);
    const title = 'Processing Time Per Week for Site: CMJAH';
    const xAxisLabel = 'Year, Week No.';
    const yAxisLabel = 'Processing time (in hours)';
    const { data, legend } = this.parseData(fetchedData.results);

    this.dataset = new ChartsLineGraphModel(
      data,
      title,
      maxValue,
      minValue,
      startDateObj,
      endDateObj,
      xAxisLabel,
      yAxisLabel
    );

    this.legend = legend;

  }

  parseData(data) {
    const colors: string[] = this.palette.getColors(data.length);
    const legend = {
      captions: []
    };

    data = data.reduce( (parsedData, item, index): ChartsLineGraphDataModel[] => {
      let order: number = null;
      let itemObject = { caption: '', fill: colors[index], values: [], };

      itemObject = item.reduce( (parsedItem, value) => {
        parsedItem.caption = parsedItem.caption || value.caption;
        order = order || value.order;
        parsedItem.values.push({
          date: new Date(value.referenceDate),
          label: `${value.referenceDate}`,
          value: value.value
        });
        return parsedItem;
      }, itemObject);

      legend.captions[order - 1] = {
        id: order - 1,
        caption: itemObject.caption,
        color: itemObject.fill
      };
      parsedData[order - 1] = itemObject;
      return parsedData;
    }, []);

    return { data, legend };
  }
}
