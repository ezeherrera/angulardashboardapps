import { Component, OnInit } from '@angular/core';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';
import { ChartsLegendModel } from '../../../../shared/charts/legend/models/legend.model';
import { ChartsLeveyJenningsGraphModel } from '../../../../shared/charts/levey-jennings/models/levey-jennings.model';

@Component({
  selector: 'app-pg-levey-jennings',
  templateUrl: './pg-levey-jennings.component.html',
  styleUrls: ['./pg-levey-jennings.component.scss'],
})
export class PgLeveyJenningsComponent implements OnInit {

  dataset: any;
  legend: ChartsLegendModel;

  constructor(
    private palette: ChartsPaletteService,
  ) {}

  ngOnInit(): void {


    const controlMean = 200;
    const standardDeviation = 4;
    const controlLimit = 2;
    const title = 'HPC';
    const yAxisLabel = 'Log (Results)';
    const xAxisLabel = 'Date';
    let data: ChartsLeveyJenningsGraphModel = null;

    const MOCKDATA = this.generateMockData([
      { title: '393791 (393791)' },
      { title: '393794 (393794)' },
      { title: '393797 (393797)' },
    ], controlMean, standardDeviation);

    const colors: string[] = this.palette.getColors(MOCKDATA.length);

    [data, this.legend ] = this.formatData(MOCKDATA, colors);

    this.dataset = {
      controlLimit,
      controlMean,
      data,
      standardDeviation,
      title,
      xAxisLabel,
      yAxisLabel,
    };

  }

  formatData(rawData, colors) {
    const legend = {
      captions: []
    };
    return [
      rawData.map( (item, index) => {
        legend.captions.push({
          id: index,
          caption: item.title,
          color: colors[index],
        });
        return {
          caption: item.title,
          fill: colors[index],
          values: item.results
        };
      }, legend
    ), legend];
  }

  generateMockData(controlSources, mean, stdDev) {
    return controlSources.map(
      (item) => ({
        title: item.title,
        results: Array.from({ length: 100 }, () => ({
          measurement: new Date(
              (new Date(2016, 0, 1)).getTime()
              + Math.random()
              * ( (new Date()).getTime() - (new Date(2016, 0, 1)).getTime() )),
          value: Math.floor(Math.random() * ((mean + stdDev + 1) - (mean - stdDev))) + (mean - stdDev)
        })).sort(function(a, b) {return (a.measurement > b.measurement) ? 1 : ((b.measurement > a.measurement) ? -1 : 0); } )
      }),
      [mean, stdDev]
    );
  }

}
