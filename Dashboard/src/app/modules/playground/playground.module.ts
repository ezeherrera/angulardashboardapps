import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../../shared/material/material.module';

import { PlaygroundRoutingModule } from './playground-routing.module';
import { ChartsModule } from '../../shared/charts/charts.module';
import { SvgIconModule } from '../../shared/svg-icon/svg-icon.module';

import { PgCigaretteChartComponent } from './pages/cigarette-chart/pg-cigarette-chart.component';
import { PgLeveyJenningsComponent } from './pages/levey-jennings-chart/pg-levey-jennings-chart.component';
import { PgPieChartComponent } from './pages/pie-chart/pg-pie-chart.component';
import { PlaygroundComponent } from './pages/playground/playground.component';
import { PgLineChartComponent } from './pages/line-chart/pg-line-chart.component';
import { IconsComponent } from './pages/icons/pg-icons.component';
import { PgGaugeChartComponent } from './pages/gauge-chart/pg-gauge-chart.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ChartsModule,
    PlaygroundRoutingModule,
    SvgIconModule,
  ],
  declarations: [
    PlaygroundComponent,
    PgCigaretteChartComponent,
    PgLeveyJenningsComponent,
    PgLineChartComponent,
    PgPieChartComponent,
    IconsComponent,
    PgGaugeChartComponent,
  ],
  exports: [],
})
export class PlaygroundModule { }
