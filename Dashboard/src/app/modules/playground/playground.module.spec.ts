import { PlaygroundModule } from './playground.module';

describe('PlaygroundModule', () => {
  let playground: PlaygroundModule;

  beforeEach(() => {
    playground = new PlaygroundModule();
  });

  it('should create an instance', () => {
    expect(playground).toBeTruthy();
  });
});
