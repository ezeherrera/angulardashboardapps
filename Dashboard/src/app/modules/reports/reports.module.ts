import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';

/* shared modules */
import { ChartsModule } from '../../shared/charts/charts.module';
import { MaterialModule } from '../../shared/material/material.module';
import { PagedDataSourceModule } from '../../shared/paged-data-source/paged-data-source.module';

/* Services */
import { ProcessingTimeService } from './services/processing-time/preocessing-time.service';
import { QcService } from './services/qc/qc.service';
import { ReportService } from './services/report/report.service';
import { ResultsService } from './services/results/results.service';
import { SiteService } from './services/site/site.service';
import { TestTypeService } from './services/test-type/test-type.service';
import { DeviceService } from './services/device/device.service';
import { ApiService } from './services/api/api.service';

/* Feature modules */
import { ReportsFormComponent } from './pages/form/reports-form.component';
import { ReportsResultsComponent } from './pages/results/reports-results.component';
import { QcDataComponent } from './pages/results/qc-data/qc-data.component';
import { ProcessingTimeComponent } from './pages/results/processing-time/processing-time.component';
import { ReportsFormFilterDateRangeComponent } from './pages/form/filter-date-range/reports-form-filter-date-range.component';
import { ReportsFormFilterDevicesComponent } from './pages/form/filter-devices/reports-form-filter-devices.component';
import { ReportsFormFilterTestTypesComponent } from './pages/form/filter-test-types/reports-form-filter-test-types.component';
import { ReportsFormFilterSitesComponent } from './pages/form/filter-sites/reports-form-filter-sites.component';
import { ResultsGraphComponent } from './pages/results/results-graph/results-graph.component';
import { ResultsTableComponent } from './pages/results/results-table/results-table.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { ShowViewsComponent } from './pages/results/show-views/show-views.component';
import { EventLoop } from '../../shared/utils/event-loop';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    ChartsModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PagedDataSourceModule,
  ],
  declarations: [
    ReportsFormComponent,
    ReportsFormFilterDevicesComponent,
    ReportsFormFilterDateRangeComponent,
    ReportsFormFilterSitesComponent,
    ReportsFormFilterTestTypesComponent,
    ReportsResultsComponent,
    ResultsGraphComponent,
    ResultsTableComponent,
    QcDataComponent,
    ProcessingTimeComponent,
    ShowViewsComponent,
  ],
  providers: [
    EventLoop,
    DeviceService,
    ProcessingTimeService,
    QcService,
    ReportService,
    ApiService,
    ResultsService,
    SiteService,
    TestTypeService,
  ],
})
export class ReportsModule { }
