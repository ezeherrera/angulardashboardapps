// Angular imports
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ApiProcessingTimeInput,
  ApiProcessingTimeOutput,
  ApiProcessingTimeTrendsInput,
  ApiProcessingTimeTrendsOutput,
} from '../../models/apiProcessingTime.model';
import { APIQcDataInput, APIQcDataOutput } from '../../models/apiQcData.model';
import { APIDevice } from '../../models/apiDevice.model';
import { APISite } from '../../models/apiSite.model';
import { ApiReportInput, ApiReportOutput } from '../../models/apiReport.model';
import { APITestType } from '../../models/apiTestType.model';
import { AuthenticationService as AuthService } from '../../../../core/authentication/authentication.service';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }
)};

@Injectable()
export class ApiService {
  httpOptions;
  constructor(private http: HttpClient, authService: AuthService) {
  }

  // GET: /sites
  sites(): Observable<APISite[]> {
    return this.http.get<APISite[]>(environment.apiReportingServiceUrl + '/sites').pipe(map(t => t));
  }

  // GET: /devices
  devices(): Observable<APIDevice[]> {
    return this.http.get<APIDevice[]>(environment.apiReportingServiceUrl + '/devices').pipe(map(t => t));
  }

  // GET: /testtypes
  testtypes(): Observable<APITestType[]> {
    return this.http.get<APITestType[]>(environment.apiReportingServiceUrl + '/testtypes').pipe(map(t => t));
  }

  // POST: /sitedata
  sitedata(filters: ApiReportInput): Observable<ApiReportOutput> {
    // tslint:disable-next-line:no-debugger
    const data = JSON.stringify(filters);
    return this.http.post<ApiReportOutput>(environment.apiReportingServiceUrl + '/sitedata', data, httpOptions).pipe(map(t => t));
  }

  // POST: /processingtime
  processingtime(filters: ApiProcessingTimeInput): Observable<ApiProcessingTimeOutput[]> {
    const data = JSON.stringify(filters);
    const url = environment.apiReportingServiceUrl + '/processingtime';
    return this.http.post<ApiProcessingTimeOutput[]>(url, data, httpOptions).pipe(map(t => t));
  }

  // POST: /processingtimeweekly
  processingtimetrends(filters: ApiProcessingTimeTrendsInput): Observable<ApiProcessingTimeTrendsOutput> {
    const data = JSON.stringify(filters);
    const url = environment.apiReportingServiceUrl + '/processingtimeweekly';
    return this.http.post<ApiProcessingTimeTrendsOutput>(url, data, httpOptions).pipe(map(t => t));
  }

  // POST: /processingtime
  qcdata(filters: APIQcDataInput): Observable<APIQcDataOutput[]> {
    const data = JSON.stringify(filters);
    const url = environment.apiReportingServiceUrl + '/qualitycontrol';
    return this.http.post<APIQcDataOutput[]>(url, data, httpOptions).pipe(map(t => t));
  }
}
