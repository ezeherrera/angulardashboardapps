import { inject, TestBed } from '@angular/core/testing';

import { ProcessingTimeService } from './preocessing-time.service';

describe('ProcessingTimeService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProcessingTimeService]
    });
  });

  it('should be created', inject([ProcessingTimeService], (service: ProcessingTimeService) => {
    expect(service).toBeTruthy();
  }));
});
