import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ApiService } from '../api/api.service';
import {
  ApiProcessingTimeData,
  ApiProcessingTimeInput,
  ApiProcessingTimeOutput,
  ApiProcessingTimeTrendsData,
  ApiProcessingTimeTrendsInput,
  ApiProcessingTimeTrendsOutput,
} from '../../models/apiProcessingTime.model';
import { ApiReportInput } from '../../models/apiReport.model';
import { ReportService } from '../report/report.service';


@Injectable()
export class ProcessingTimeService {

  // private output = new ApiProcessingTimeData();
  // private outputTrends = new ApiProcessingTimeTrendsData();
  private observationProcessingTimeOutput$ = new BehaviorSubject<ApiProcessingTimeData>(null);
  private observationProcessingTimeTrendsOutput$ = new BehaviorSubject<ApiProcessingTimeTrendsData>(null);
  private reportObserver: BehaviorSubject<ApiReportInput> = this.reportService.currentApiReportInputFilter;

  constructor(
    private reportService: ReportService,
    private reportingService: ApiService,
  ) {
    this.reportObserver.subscribe( filter => {
      const { deviceType, sites, testTypes, from, to } = filter;
      this.clearApiProcessingTimeObservation();
      this.fetchProcessingTimeByFilterCriteria(deviceType, sites, testTypes, from, to);
      this.clearApiProcessingTimeTrendsObservation();
      this.fetchProcessingTimeTrendsByFilterCriteria(deviceType, sites, testTypes, from, to);
    });
  }

  public getApiProcessingTimeOutputObservation(): BehaviorSubject<ApiProcessingTimeData> {
    return this.observationProcessingTimeOutput$;
  }
  private clearApiProcessingTimeObservation(): void {
    this.observationProcessingTimeOutput$ = new BehaviorSubject<ApiProcessingTimeData>(null);
  }
  private fetchProcessingTimeByFilterCriteria (
    deviceType: string,
    sites: string[],
    testtypes: string[],
    from: Date,
    to: Date): void {
      const input = new ApiProcessingTimeInput(deviceType, sites, testtypes, from, to);
      const observation$: Observable<ApiProcessingTimeOutput[]> = this.reportingService
        .processingtime(input)
        .pipe(shareReplay(1));
      observation$.subscribe(output => {
        this.observationProcessingTimeOutput$.next({ output, input } );
      });
    }

  // Processing Time Trends - Line Chart
  public getApiProcessingTimeTrendsOutputObservation(): BehaviorSubject<ApiProcessingTimeTrendsData> {
    return this.observationProcessingTimeTrendsOutput$;
  }
  private clearApiProcessingTimeTrendsObservation(): void {
    this.observationProcessingTimeTrendsOutput$ = new BehaviorSubject<ApiProcessingTimeTrendsData>(null);
  }
  private fetchProcessingTimeTrendsByFilterCriteria (
    deviceType: string,
    sites: string[],
    testtypes: string[],
    from: Date,
    to: Date): void {
      const input = new ApiProcessingTimeTrendsInput(deviceType, sites, testtypes, from, to);
      const observation$:  Observable<ApiProcessingTimeTrendsOutput> = this.reportingService
        .processingtimetrends(input)
        .pipe(shareReplay(1));
      observation$.subscribe(output => {
        this.observationProcessingTimeTrendsOutput$.next({ output, input });
      });
    }

}
