import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ApiReportInput, ApiReportOutput } from '../../models/apiReport.model';
import { ApiService } from '../api/api.service';
import { ReportService } from '../report/report.service';


@Injectable()
export class ResultsService {

  private output: ApiReportOutput = null;
  private reportObserver: Subject<ApiReportInput> = this.reportService.currentApiReportInputFilter;
  private observationSiteDataOutput$ = new BehaviorSubject<ApiReportOutput>(this.output);

  public constructor(
    private reportService: ReportService,
    private apiService: ApiService
  ) {
    this.reportObserver.subscribe( filter => {
      this.clearApiReportOutputObservation();
      this.fetchResultsByFilterCriteria(
        filter.deviceType,
        filter.sites,
        filter.testTypes,
        filter.from,
        filter.to
      );
    });
  }

  public getApiReportOutputObservation(): BehaviorSubject<ApiReportOutput> {
    return this.observationSiteDataOutput$;
  }
  private clearApiReportOutputObservation(): void {
    this.output = new ApiReportOutput;
    this.observationSiteDataOutput$ = new BehaviorSubject<ApiReportOutput>(this.output);
  }
  private fetchResultsByFilterCriteria (
    deviceType: string,
    sites: string[],
    testtypes: string[],
    from: Date,
    to: Date): void {
    const observation$ = this.apiService
      .sitedata(new ApiReportInput(deviceType, sites, testtypes, from, to))
      .pipe(shareReplay(1));
    observation$.subscribe(result => {
      this.observationSiteDataOutput$.next(result);
    });
  }
}
