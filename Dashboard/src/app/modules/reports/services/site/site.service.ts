import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { Site } from '../../models/apiSite.model';
import { ApiService } from '../api/api.service';

@Injectable()
export class SiteService {

  observationSitesAvailable$ = new ReplaySubject<Site[]>();
  loaded = false;
  constructor(private apiService: ApiService) {}

  public getSiteObservable(): Observable<Site[]> {
    if (this.loaded) {
      return this.observationSitesAvailable$.asObservable();
    } else {
      return this.fetchData();
    }
  }

  public clearSiteObservable(): void {
    this.observationSitesAvailable$ = new ReplaySubject<Site[]>();
  }

  private fetchData(): Observable<Site[]> {
    const observation$ = this.apiService.sites();

    observation$.subscribe(result => {
      this.observationSitesAvailable$.next(result.map(t => Site.parseSite(t)));
      this.loaded = true;
    });

    return this.observationSitesAvailable$.asObservable();
  }
}
