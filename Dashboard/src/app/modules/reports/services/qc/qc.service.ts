import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { shareReplay } from 'rxjs/operators';
import { ApiService } from '../api/api.service';
import { APIQcDataInput, APIQcDataOutput, APIQcDataParsedOutput } from '../../models/apiQcData.model';
import { ApiReportInput } from '../../models/apiReport.model';
import { ReportService } from '../report/report.service';


@Injectable()
export class QcService {

  private reportObserver: BehaviorSubject<ApiReportInput> = this.reportService.currentApiReportInputFilter;
  private observationQcDataOutput$ = new BehaviorSubject<APIQcDataParsedOutput>(null);

  constructor(
    private reportService: ReportService,
    private apiService: ApiService,
  ) {
    this.reportObserver.subscribe( filter => {
      this.clearApiQcDataObservation();
      this.fetchQcDataFilterCriteria(
        filter.deviceType,
        filter.sites,
        filter.testTypes,
        filter.from,
        filter.to
      );
    });
  }

  public getApiQcDataOutputObservation(): BehaviorSubject<APIQcDataParsedOutput> {
    return this.observationQcDataOutput$;
  }
  private clearApiQcDataObservation(): void {
    this.observationQcDataOutput$ = new BehaviorSubject<APIQcDataParsedOutput>(null);
  }
  private fetchQcDataFilterCriteria (
    deviceType: string,
    sites: string[],
    testtypes: string[],
    from: Date,
    to: Date): void {
      const observation$: Observable<APIQcDataOutput[]> = this.apiService
        .qcdata(new APIQcDataInput(deviceType, sites, testtypes, from, to))
        .pipe(shareReplay(1));
      observation$.subscribe(result => {
        // parsing server data to component format data
        const parsedResult = result.reduce( (previous, current) => {
          if (typeof previous.lots[current.lotNumber] === 'undefined') {
            previous.lots[current.lotNumber] = previous.results.length;
            previous.results.push({
              caption: current.lotNumber,
              values: []
            });
          }
          const index = previous.lots[current.lotNumber];
          previous.results[index].values.push({ measurement: new Date(current.timestamp), value: current.ct });
          previous.total += current.ct;
          return previous;
        }, { lots: {}, results: [], total: 0, });
        // ordering lots values
        parsedResult.results = parsedResult.results.map( (lot) => {
          lot.values.sort((a, b) => (a.measurement > b.measurement) ? 1 : ((b.measurement > a.measurement) ? -1 : 0));
          return lot;
        } );

        const data = parsedResult.results;
        const controlMean = parsedResult.total / result.length;
        const standardDeviation = .4; // demo SD of 5%
        const controlLimit = 2;
        const yAxisLabel = 'Log (Results)';
        const xAxisLabel = 'Dates';
        const dataset: APIQcDataParsedOutput = {
          controlMean,
          standardDeviation,
          controlLimit,
          data,
          xAxisLabel,
          yAxisLabel,
        };

        this.observationQcDataOutput$.next(dataset);
      });
    }

}
