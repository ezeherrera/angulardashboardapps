import { TestBed, inject } from '@angular/core/testing';

import { QcService } from './qc.service';

describe('QcService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QcService]
    });
  });

  it('should be created', inject([QcService], (service: QcService) => {
    expect(service).toBeTruthy();
  }));
});
