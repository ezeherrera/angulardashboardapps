import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { Device } from '../../models/apiDevice.model';

@Injectable()
export class DeviceService {
  observationDevicesAvailable$ = new ReplaySubject<Device[]>();
  loaded = false;
  constructor(private reportingService: ApiService) {}

  public getDeviceObservable(): Observable<Device[]> {
    if (this.loaded) {
      return this.observationDevicesAvailable$.asObservable();
    } else {
      return this.fetchData();
    }
  }

  public clearDeviceObservable(): void {
    this.observationDevicesAvailable$ = new ReplaySubject<Device[]>();
  }

  private fetchData(): Observable<Device[]> {
    const observation$ = this.reportingService.devices();

    observation$.subscribe(result => {
      this.observationDevicesAvailable$.next(result.map(t => Device.parseDevice(t)));
      this.loaded = true;
    });

    return this.observationDevicesAvailable$.asObservable();
  }
}
