import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiReportInput } from '../../models/apiReport.model';
import { SessionStorageService } from '../../../../core/session-storage/session-storage.service';

@Injectable()
export class ReportService {

  private ApiReportInputFilter = new BehaviorSubject<ApiReportInput>(
    new ApiReportInput('', [], [], new Date(), new Date())
  );

  public currentApiReportInputFilter: BehaviorSubject<ApiReportInput> = this.ApiReportInputFilter;

  constructor (
    private sessionStorage: SessionStorageService,
  ) {
    const filter = this.sessionStorage.getData('report');
    if (filter) {
      this.ApiReportInputFilter.next(filter);
    }
  }

  setFilter(filter: ApiReportInput): void {
    this.ApiReportInputFilter.next(filter);
    this.sessionStorage.setData('report', filter);
  }

  resetFilter(): void {
    const filter = new ApiReportInput('', [], [], new Date(), new Date());
    this.ApiReportInputFilter.next(filter);
    this.sessionStorage.removeData('report');
  }

}
