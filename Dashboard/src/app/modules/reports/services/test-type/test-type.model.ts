import { APITestType } from '../../models/apiTestType.model';

export class TestType {
  category: string;
  tests: string[];

  public constructor(init?: Partial<TestType>) {
    Object.assign(this, init);
  }

  public static parseTestType(apiTestType: APITestType): TestType {
    return new TestType({
      category: apiTestType ? apiTestType.category : null,
      tests: apiTestType ? apiTestType.tests : null
    });
  }
}
