import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { ApiService } from '../api/api.service';
import { TestType } from './test-type.model';

@Injectable()
export class TestTypeService {
  observationTestTypesAvailable$ = new ReplaySubject<TestType[]>();
  loaded = false;
  constructor(private apiService: ApiService) {}

  public getTestTypeObservable(): Observable<TestType[]> {
    if (this.loaded) {
      return this.observationTestTypesAvailable$.asObservable();
    } else {
      return this.fetchData();
    }
  }

  public clearTestTypeObservable(): void {
    this.observationTestTypesAvailable$ = new ReplaySubject<TestType[]>();
  }

  private fetchData(): Observable<TestType[]> {
    const observation$ = this.apiService.testtypes();

    observation$.subscribe(result => {
      this.observationTestTypesAvailable$.next(result.map(t => TestType.parseTestType(t)));
      this.loaded = true;
    });

    return this.observationTestTypesAvailable$.asObservable();
  }

}
