import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../core/guards/auth.guard';
import { ReportsFormComponent } from './pages/form/reports-form.component';
import { ReportsResultsComponent } from './pages/results/reports-results.component';
import { ResultsTableComponent } from './pages/results/results-table/results-table.component';
import { ResultsGraphComponent } from './pages/results/results-graph/results-graph.component';
import { ProcessingTimeComponent } from './pages/results/processing-time/processing-time.component';
import { QcDataComponent } from './pages/results/qc-data/qc-data.component';

const routes: Routes = [
  {
    path: 'reports',
    canActivate: [AuthGuard],
    children: [
      {
        path: '',
        component: ReportsFormComponent,
      },
      {
        path: 'results',
        component: ReportsResultsComponent,
        children: [
          {
            path: 'table',
            component: ResultsTableComponent,
          },
          {
            path: 'graph',
            component: ResultsGraphComponent,
          },
          { path: '**', redirectTo: 'graph', pathMatch: 'full' },
        ],
      },
      {
        path: 'invalid-results',
        component: ReportsResultsComponent,
        children: [
          {
            path: 'table',
            component: ResultsTableComponent,
          },
          {
            path: 'graph',
            component: ResultsGraphComponent,
          },
          { path: '**', redirectTo: 'graph', pathMatch: 'full' },
        ]
      },
      {
        path: 'totals',
        component: ReportsResultsComponent,
        children: [
          {
            path: 'table',
            component: ResultsTableComponent,
          },
          {
            path: 'graph',
            component: ResultsGraphComponent,
          },
          { path: '**', redirectTo: 'graph', pathMatch: 'full' },
        ]
      },
      {
        path: 'processing-time',
        component: ReportsResultsComponent,
        children: [
          {
            path: '',
            component: ProcessingTimeComponent,
          },
        ]
      },
      {
        path: 'qc-data',
        component: ReportsResultsComponent,
        children: [
          {
            path: '',
            component: QcDataComponent,
          },
        ]
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [ RouterModule ]
})

export class ReportsRoutingModule { }
