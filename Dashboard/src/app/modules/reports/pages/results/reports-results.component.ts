import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ReportService } from '../../services/report/report.service';

import { ApiReportInput, ApiReportOutput } from '../../models/apiReport.model';


@Component({
  selector: 'app-reports-results',
  templateUrl: './reports-results.component.html',
  styleUrls: ['./reports-results.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsResultsComponent implements OnInit {
  public result: ApiReportOutput;
  private filter: ApiReportInput;

  public constructor(
    private router: Router,
    private reportService: ReportService,
  ) {}

  public ngOnInit(): void {
    this.reportService.currentApiReportInputFilter.subscribe(
      filter => {
        this.filter = filter;
        // if no filters then redirects to form
        if (!this.filter.deviceType || !this.filter.testTypes.length) {
          this.router.navigate(['/reports']);
        }
      }
    );


  }

  public getByResultsRouterLink(): string {
    return ['/reports/results', this.getActiveView()].join('/');
  }

  public getByInvalidResultsRouterLink(): string {
    return ['/reports/invalid-results', this.getActiveView()].join('/');
  }

  public getByTotalRouterLink(): string {
    return ['/reports/totals', this.getActiveView()].join('/');
  }

  private getActiveView(): string {
    if (
      this.router.url.toLowerCase().endsWith('table') ||
      this.router.url.toLowerCase().endsWith('graph')
    ) {
      return this.router.url.split('/').slice(-1)[0];
    }
    return '';
  }
}
