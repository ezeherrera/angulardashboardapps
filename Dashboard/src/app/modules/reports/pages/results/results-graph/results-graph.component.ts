import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { ApiReportData } from '../../../models/apiReport.model';
import { ResultsService } from '../../../services/results/results.service';
import { ChartsLegendModel } from '../../../../../shared/charts/legend/models/legend.model';
import { ChartsPieModel } from '../../../../../shared/charts/pie/models/pie.model';
import { ChartsPaletteService } from '../../../../../shared/charts/palette/palette.service';
import { isNil } from '../../../../../shared/utils/utils';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-result-graph',
  templateUrl: './results-graph.component.html',
  styleUrls: ['./results-graph.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ResultsGraphComponent implements OnInit, OnDestroy {
  result$: Subscription;
  currentRouteContext = 'results';
  dataset: {
    data: ChartsPieModel[],
    legend: ChartsLegendModel,
  };
  loading: boolean;

  constructor(private router: Router,
    private resultsService: ResultsService,
    private palette: ChartsPaletteService,
  ) { }

  ngOnInit() {
    this.dataset = {
      data: [],
      legend: {
        header: 'Result Range',
        captions: [],
      },
    };
    this.loading = true;

    if (this.router.url.indexOf('invalid-results') > -1) {
      this.currentRouteContext = 'invalidResults';
      this.dataset.legend.header = 'Flags';
    } else if (this.router.url.indexOf('totals') > -1) {
      this.currentRouteContext = 'totals';
      this.dataset.legend.header = 'Successful';
    }

    this.result$ = this.resultsService.getApiReportOutputObservation()
    .subscribe(result => {
      if (result[this.currentRouteContext]) {
        const currentResult = result[this.currentRouteContext];
        this.dataset = this.formatData(currentResult, this.dataset.legend);
        this.loading = false;
      }
    });
  }

  /**
   * @description Unsubscribe on element destroy
   */
  ngOnDestroy(): void {
    if (!isNil(this.result$)) {
      this.result$.unsubscribe();
    }
  }

  formatData = (rawdata: ApiReportData[], legend: ChartsLegendModel): { data: ChartsPieModel[] , legend: ChartsLegendModel } => {
    legend.captions = [];
    const color: string[] = this.palette.getColors(rawdata.length);
    const data: ChartsPieModel[] = rawdata.map( (item, index) => {
      if (item) {
        const id = index;
        const value = item.percentage;
        const label = item.numberOfResults.toString();
        const caption = item.remark;
        const fill = color[index];
        if (!legend.captions[index]) {
          legend.captions[index] = {
            id: index,
            caption,
            color: fill,
          };
        }
        return { id, caption, label, value, fill };
      }
      return null;
    });
    return { data, legend };
  }

}
