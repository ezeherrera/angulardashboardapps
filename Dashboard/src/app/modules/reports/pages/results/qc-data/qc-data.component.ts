import { Component, OnInit } from '@angular/core';
import { QcService } from '../../../services/qc/qc.service';
import { ChartsLeveyJenningsModel,
         ChartsLeveyJenningsGraphModel } from '../../../../../shared/charts/levey-jennings/models/levey-jennings.model';
import { ChartsLegendModel, ChartsLegendCaptionModel } from '../../../../../shared/charts/legend/models/legend.model';
import { ChartsPaletteService } from '../../../../../shared/charts/palette/palette.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-qc-data',
  templateUrl: './qc-data.component.html',
  styleUrls: ['./qc-data.component.scss']
})
export class QcDataComponent implements OnInit {

  dataset: ChartsLeveyJenningsModel;
  legend: ChartsLegendModel = {
    header: 'Lot Numbers',
    captions: [],
  };
  loading: boolean;
  result$: Subscription;

  constructor(
    private qcService: QcService,
    private palette: ChartsPaletteService,
  ) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.result$ = this.qcService.getApiQcDataOutputObservation().subscribe( fetchedData => {
      this.loading = true;
      if (fetchedData) {
        // dataset vars
        const controlMean = fetchedData.controlMean;
        const standardDeviation = fetchedData.standardDeviation;
        const controlLimit = fetchedData.controlLimit;
        const title = fetchedData.title;
        const yAxisLabel = fetchedData.yAxisLabel;
        const xAxisLabel = fetchedData.xAxisLabel;
        let data: ChartsLeveyJenningsGraphModel[] = null;
        [data, this.legend.captions ] = this.formatData(fetchedData.data);
        // params to chart
        this.dataset = {
          controlLimit,
          controlMean,
          data,
          standardDeviation,
          title,
          xAxisLabel,
          yAxisLabel,
        };
        this.loading = false;
      }
    });

  }

  formatData(rawData) {
    const captions: ChartsLegendCaptionModel[] = [];
    const colors: string[] = this.palette.getColors(rawData.length);
    return [
      rawData.map( (item, index) => {
        captions.push({
          id: index,
          caption: item.caption,
          color: colors[index],
        });
        return {
          caption: item.caption,
          fill: colors[index],
          values: item.values
        };
      }, captions
    ), captions];
  }
}
