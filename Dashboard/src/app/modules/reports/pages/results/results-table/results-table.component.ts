import {
  Component, OnDestroy,
  OnInit,
  ViewEncapsulation} from '@angular/core';
import { Router } from '@angular/router';
import { isNil } from '../../../../../shared/utils/utils';
import { ResultsService } from '../../../services/results/results.service';

@Component({
  selector: 'app-results-table',
  templateUrl: './results-table.component.html',
  styleUrls: ['./results-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ResultsTableComponent implements OnInit, OnDestroy {
  currentRouteContext = 'results';
  data: any[];
  displayedColumns = ['numberOfResults', 'percentage', 'remark'];
  result$;
  loading: boolean;

  constructor(private router: Router,
    private resultService: ResultsService) {
  }

  ngOnDestroy(): void {
    if (!isNil(this.result$)) {
      this.result$.unsubscribe();
    }
  }

  ngOnInit() {
    this.loading = true;

    if (this.router.url.indexOf('invalid-results') > -1) {
      this.currentRouteContext = 'invalidResults';
    } else if (this.router.url.indexOf('totals') > -1) {
      this.currentRouteContext = 'totals';
    }

    this.result$ = this.resultService.getApiReportOutputObservation().subscribe(result => {

      if (result[this.currentRouteContext]) {
        if (this.currentRouteContext === 'results') {
          this.data = result.results.map(t => ({
            numberOfResults: t.numberOfResults,
            percentage: t.percentage,
            percentageTotal: t.percentageTotal,
            remark: t.remark
          }));

        } else if (this.currentRouteContext === 'invalidResults') {
          this.displayedColumns = ['numberOfResults', 'percentage', 'percentageTotal', 'remark'];
          this.data = result.invalidResults.map(t => ({
            numberOfResults: t.numberOfResults,
            percentage: t.percentage,
            percentageTotal: t.percentageTotal,
            remark: t.remark
          }));

        } else if (this.currentRouteContext === 'totals') {
          this.data = result.totals.map(t => ({
            numberOfResults: t.numberOfResults,
            percentage: t.percentage,
            percentageTotal: t.percentageTotal,
            remark: t.remark
          }));
        }

        this.loading = false;
      }
    });
  }

}
