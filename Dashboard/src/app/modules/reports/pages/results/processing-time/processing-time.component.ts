import { Component, OnDestroy, OnInit } from '@angular/core';
import { isNil } from '../../../../../shared/utils/utils';
import { ProcessingTimeService } from '../../../services/processing-time/preocessing-time.service';
import { ChartsLegendModel, ChartsLegendCaptionModel } from '../../../../../shared/charts/legend/models/legend.model';
import { ChartsPaletteService } from '../../../../../shared/charts/palette/palette.service';
import { ChartsLineGraphModel, ChartsLineGraphDataModel } from '../../../../../shared/charts/line/models/line.model';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-processing-time',
  templateUrl: './processing-time.component.html',
  styleUrls: ['./processing-time.component.scss'],
})
export class ProcessingTimeComponent implements OnInit, OnDestroy {

  dataset: object;
  datasetTrends: object;
  loading: boolean;
  loadingTrends: boolean;
  result$: Subscription;
  resultTrends$: Subscription;
  legend: ChartsLegendModel = {
    header: 'Processing steps',
    captions: [],
  };
  legendTrends: ChartsLegendModel = {
    header: 'Processing steps',
    captions: [],
  };

  constructor(
    private service: ProcessingTimeService,
    private palette: ChartsPaletteService,
  ) {
    this.loading = true;
  }

  ngOnInit(): void {

    this.loading = true;
    const formatData = this.formatData;
    const xAxisLabel = 'Time in hours';
    this.result$ = this.service.getApiProcessingTimeOutputObservation().subscribe( data => {
      this.loading = true;
      if (data) {
        const { sections, captions } = formatData(data.output);
        this.legend.captions = captions;
        const { from, to } = data.input;
        this.dataset = {
          data: [ {
              title: `Processing Time <br/>(from ${this.formatDate(from)} to ${this.formatDate(to)} )`,
              sections
            } ],
          legend: this.legend,
          xAxisLabel,
        };
        this.loading = false;
      }
    });

    this.loadingTrends = true;
    this.resultTrends$ = this.service.getApiProcessingTimeTrendsOutputObservation().subscribe( fetchedData => {
      this.loadingTrends = true;
      if (fetchedData) {
        const {
          maxValue: trendsMaxValue,
          minValue: trendsMinValue,
          startDate: trendsStartDate,
          endDate: trendsEndDate,
        } = fetchedData.output;
        const trendsStartDateObj = new Date(trendsStartDate);
        const trendsEndDateObj = new Date(trendsEndDate);
        const { from, to } = fetchedData.input;
        const forSitesTitle = (fetchedData.input.sites.length) ?
          `for sites: ${ fetchedData.input.sites.reduce( (r, s) => r += `, ${s}`) }` : '';
        const trendsTitle = `Processing Time Per Week ${forSitesTitle}
                            <br/>(from ${this.formatDate(from)} to ${this.formatDate(to)} )`;
        const trendsXAxisLabel = 'Year, Week No.';
        const trendsYAxisLabel = 'Processing time (in hours)';
        const { data: trendsData, captions: trendsCaptions } = this.formatTrendsData(fetchedData.output.results);
        this.datasetTrends = new ChartsLineGraphModel(
          trendsData,
          trendsTitle,
          trendsMaxValue,
          trendsMinValue,
          trendsStartDateObj,
          trendsEndDateObj,
          trendsXAxisLabel,
          trendsYAxisLabel
        );
        this.legendTrends.captions = trendsCaptions;

        this.loadingTrends = false;
      }
    });
  }


  ngOnDestroy(): void {
    if (!isNil(this.result$)) {
      this.result$.unsubscribe();
    }
  }

  formatTime = (value) => {
    let formattedValue = '';
    formattedValue += `${Math.floor(value)}h`;
    formattedValue += ` ${Math.floor(value % 1 * 60)}min`;
    return formattedValue;
  }

  formatDate = (date) => {
    date = new Date(date);
    const d = date.getDate();
    const m = date.getMonth() + 1;
    const Y = date.getFullYear();
    return `${Y}/${m}/${d}`;
  }


  formatData = (rawdata) => {
    const color: string[] = this.palette.getColors(rawdata.length);
    const captions: ChartsLegendCaptionModel[] = [];
    const sections = rawdata.map( (item, index) => {
      if (item) {
        const value = item.value;
        const label = this.formatTime(value);
        const caption = item.caption;
        const fill = color[index];
        if (!captions[index]) {
          captions[index] = {
            id: index,
            caption,
            color: fill,
          };
        }
        return { caption, label, value, fill };
      }
      return null;
    });

    return {
      sections,
      captions
    };
  }

  formatTrendsData(data) {

    if (!data) {
      return { data: [], captions: [] }; // if no data proviuded returns empty vars
    }

    const captions = [];
    const colors: string[] = this.palette.getColors(data.length);

    data = data.reduce( (parsedData, item, index): ChartsLineGraphDataModel[] => {
      let order: number = null;
      let itemObject = { caption: '', fill: colors[index], values: [], };

      itemObject = item.reduce( (parsedItem, value) => {
        parsedItem.caption = parsedItem.caption || value.caption;
        order = order || value.order;
        parsedItem.values.push({
          date: new Date(value.referenceDate),
          label: `${value.referenceDate}`,
          value: value.value
        });
        return parsedItem;
      }, itemObject);

      captions[order - 1] = {
        id: order - 1,
        caption: itemObject.caption,
        color: itemObject.fill
      };
      parsedData[order - 1] = itemObject;
      return parsedData;
    }, []);

    return { data, captions };
  }
}
