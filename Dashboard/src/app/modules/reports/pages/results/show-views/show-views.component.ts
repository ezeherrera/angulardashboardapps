import { Component, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-show-views',
  templateUrl: './show-views.component.html',
  styleUrls: ['./show-views.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ShowViewsComponent {
  public constructor(private route: ActivatedRoute, private router: Router) { }

  public showGraph(): void {
    this.router.navigate(['../graph'], {
      relativeTo: this.route
    });
  }

  public showTable(): void {
    this.router.navigate(['../table'], {
      relativeTo: this.route
    });
  }

  public isCurrentGraph(): boolean {
    return this.router.url.toLowerCase().includes('graph');
  }

  public isCurrentTable(): boolean {
    return this.router.url.toLowerCase().includes('table');
  }
}
