import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ShowViewsComponent } from './show-views.component';

describe('ShowViewsComponent', () => {
  let component: ShowViewsComponent;
  let fixture: ComponentFixture<ShowViewsComponent>;

  let router: Router;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [ReactiveFormsModule, FormsModule, RouterTestingModule.withRoutes([])],
      declarations: [ShowViewsComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(ShowViewsComponent);
    component = fixture.componentInstance;

    router = TestBed.get(Router);
  });

  it('should create', () => {
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should navigate when graph view is selected', () => {
    const spyNavigate = spyOn(router, 'navigate');

    fixture.detectChanges();

    component.showGraph();

    expect(spyNavigate).toHaveBeenCalledTimes(1);
  });

  it('should navigate when table view is selected', () => {
    const spyNavigate = spyOn(router, 'navigate');

    fixture.detectChanges();

    component.showTable();

    expect(spyNavigate).toHaveBeenCalledTimes(1);
  });
});
