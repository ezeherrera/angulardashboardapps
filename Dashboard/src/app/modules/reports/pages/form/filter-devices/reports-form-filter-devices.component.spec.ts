import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportsFormFilterDevicesComponent } from './reports-form-filter-devices.component';

describe('ReportsFormFilterDevicesComponent', () => {
  let component: ReportsFormFilterDevicesComponent;
  let fixture: ComponentFixture<ReportsFormFilterDevicesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsFormFilterDevicesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsFormFilterDevicesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
