import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation,
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Device } from '../../../models/apiDevice.model';
import { DeviceService } from '../../../services/device/device.service';

@Component({
  selector: 'app-reports-form-filter-devices',
  templateUrl: './reports-form-filter-devices.component.html',
  styleUrls: ['./reports-form-filter-devices.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsFormFilterDevicesComponent implements OnInit, OnDestroy {
  @Input() inputValue: string;
  @Output() public selectionEvent = new EventEmitter<Device>(true);
  @Output() public validityEvent = new EventEmitter<boolean>(true);

  public devices$: Observable<Device[]>;
  selectedDevice: Device = new Device();
  loading = true;
  private destroy$ = new Subject();

  public constructor(private deviceService: DeviceService) {}

  public ngOnInit(): void {

    this.devices$ = this.deviceService
      .getDeviceObservable()
      .pipe(takeUntil(this.destroy$));

    this.devices$.subscribe(devices => {

      if (this.inputValue) {
        devices.every( device => {
          if (device.DeviceType === this.inputValue) {
            this.selectedDevice = device;
            this.selectDevice(device);
            return false;
          }
          return true;
        });
      }
      this.loading = false;
    });
  }

  private selectDevice (device) {
    this.selectionEvent.emit(this.selectedDevice);
    this.validityEvent.emit(this.selectedDevice != null);
  }

  /**
   * Fires when the radiobutton changes
   * @param event
   */
  public onRadioChange(event: any) {
    this.selectDevice(this.selectedDevice);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
