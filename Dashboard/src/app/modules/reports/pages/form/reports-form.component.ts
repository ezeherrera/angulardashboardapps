import {
  Component,
  OnInit,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { ApiReportInput } from '../../models/apiReport.model';
import { ReportCriteriaTestTypesModel, TestTypesRowItemModel } from '../../models/test-type.model';
import { Site } from '../../models/apiSite.model';
import { Device } from '../../models/apiDevice.model';
import { ReportService } from '../../services/report/report.service';
import { ReportsFormFilterTestTypesComponent } from './filter-test-types/reports-form-filter-test-types.component';

@Component({
  selector: 'app-reports-form',
  templateUrl: './reports-form.component.html',
  styleUrls: ['./reports-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsFormComponent implements OnInit {

  public readonly DEVICE_LIST_PANEL = 0;
  public readonly DATE_RANGE_PANEL = 1;
  public readonly SITE_CRITERIA_PANEL = 2;
  public readonly CRITERIA_FILTERS_PANEL = 3;
  public devices: Device;
  public site: Site[];
  public fromDate: Date;
  public toDate: Date;
  public currentPanel = this.DEVICE_LIST_PANEL;
  public isDevicesSelectionValid = false;
  public isSitesSelectionValid = true;
  public isDateRangeValid = true;
  public isTestTypeSelected = false;
  public filter: ApiReportInput;
  private testTypes: string[] = [];

  @ViewChild('criteria') inputChild: ReportsFormFilterTestTypesComponent;

  public constructor(
    private route: Router,
    private reportService: ReportService
  ) {}

  ngOnInit(): void {
    this.reportService.currentApiReportInputFilter.subscribe(
      filter => {
        this.filter = filter;
      }
    );
    this.fromDate = new Date(this.filter.from) || moment().subtract(1, 'month').toDate();
    this.toDate = new Date(this.filter.to) || moment().toDate();
    this.site = [];
  }

  public openPanel(panel: number): void {
    this.currentPanel = panel;
  }

  public nextPanel(): void {
    this.currentPanel++;
  }

  public previousPanel(): void {
    this.currentPanel--;
  }

  public onSelectedDevices(devices: Device): void {
    this.devices = devices;
  }

  public onSelectedSites(site: Site[]): void {
    this.site = site;
  }

  public onDevicesValidity(isValid: boolean): void {
    this.isDevicesSelectionValid = isValid;
  }

  public onSitesValidity(isValid: boolean): void {
    // this.isSitesSelectionValid = isValid;
  }

  public onDateRangeValidity(isValid: boolean): void {
    this.isDateRangeValid = isValid;
  }

  public onSelectedCriteria(testTypesRowItemModels: TestTypesRowItemModel[]) {
    this.testTypes = [];
    this.SetCriteria();
    if (this.testTypes.length > 0) {
      this.isTestTypeSelected = true;
    } else {
      this.isTestTypeSelected = false;
    }
  }

  public createReport(): void {
    this.filter.from = this.fromDate;
    this.filter.to = this.toDate;
    this.filter.deviceType = this.devices.DeviceType;
    this.filter.sites = this.site.map(t => t.Name);
    this.filter.testTypes = this.testTypes;
    this.reportService.setFilter(this.filter);
    this.nextView();
  }

  public nextView(): void {
    this.route.navigate(['/reports/results']);
  }

  private SetCriteria() {
    const criteria = this.inputChild.testTypesCriteria;
    criteria.forEach(
      (
        testTypeObsrvable: ReportCriteriaTestTypesModel<TestTypesRowItemModel>[]
      ) => {
        testTypeObsrvable.forEach(
          (element: ReportCriteriaTestTypesModel<TestTypesRowItemModel>) => {
            const testTypes = element.checkBoxGroup.getSelected();
            testTypes.map(t => t.testName).forEach(y => this.testTypes.push(y));
          }
        );
      }
    );
  }
}
