import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { Site } from '../../../models/apiSite.model';
import { SiteService } from '../../../services/site/site.service';
import { CheckboxGroup } from '../../../../../shared/utils/checkbox-group';

@Component({
  selector: 'app-reports-form-filter-sites',
  templateUrl: './reports-form-filter-sites.component.html',
  styleUrls: ['./reports-form-filter-sites.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsFormFilterSitesComponent implements OnInit {
  @Input() private inputValues: string[];
  @Output() public selectionEvent = new EventEmitter<Site[]>(true);
  @Output() public validityEvent = new EventEmitter<boolean>(true);

  public sites$: Observable<Site[]>;
  public expand = true;
  checkBoxGroup: CheckboxGroup<Site>;
  loading = true;
  private destroy$ = new Subject();

  public constructor(private siteService: SiteService) {}

  public ngOnInit(): void {

    this.checkBoxGroup = new CheckboxGroup<Site>({
      selector: testTyping => testTyping.Name
    });

    this.sites$ = this.siteService
      .getSiteObservable()
      .pipe(takeUntil(this.destroy$));

    this.sites$.subscribe(sitesDto => {
      this.checkBoxGroup.clear();
      this.checkBoxGroup.setItems(sitesDto);
      if (this.inputValues) {
        sitesDto.forEach( site => {
         if (this.inputValues.indexOf(site.Name) >= 0) {
          this.checkBoxGroup.select(site);
         }
        });
      }
      this.checkBoxGroup.onSelection().subscribe(selection => {
        this.selectionEvent.emit(selection);
        this.validityEvent.emit(!!selection.length);
      });

      // Once data is loaded remove loading
      this.loading = false;
    });
  }
}
