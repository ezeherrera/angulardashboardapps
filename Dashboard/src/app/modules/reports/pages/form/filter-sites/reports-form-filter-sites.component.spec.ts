import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsFormFilterSitesComponent } from './reports-form-filter-sites.component';

describe('ReportsFormFilterSitesComponent', () => {
  let component: ReportsFormFilterSitesComponent;
  let fixture: ComponentFixture<ReportsFormFilterSitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsFormFilterSitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsFormFilterSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
