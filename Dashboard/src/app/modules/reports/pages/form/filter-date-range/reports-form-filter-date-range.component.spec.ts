import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDatepickerModule, MatNativeDateModule } from '@angular/material';
import { EventLoop } from '../../../../../shared/utils/event-loop';
import { ReportsFormFilterDateRangeComponent } from './reports-form-filter-date-range.component';

describe('ReportsFormFilterDateRangeComponent', () => {
  let component: ReportsFormFilterDateRangeComponent;
  let fixture: ComponentFixture<ReportsFormFilterDateRangeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ReportsFormFilterDateRangeComponent],
      providers: [EventLoop],
      imports: [ReactiveFormsModule, FormsModule, MatDatepickerModule, MatNativeDateModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ReportsFormFilterDateRangeComponent);
    component = fixture.componentInstance;
  });

  it('should create', () => {
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
