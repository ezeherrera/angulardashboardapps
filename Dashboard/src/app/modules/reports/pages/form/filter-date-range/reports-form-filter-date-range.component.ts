import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDatepicker } from '@angular/material';
import { EventLoop } from '../../../../../shared/utils/event-loop';
import { isNil } from '../../../../../shared/utils/utils';

@Component({
  selector: 'app-reports-form-filter-date-range',
  templateUrl: './reports-form-filter-date-range.component.html',
  styleUrls: ['./reports-form-filter-date-range.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsFormFilterDateRangeComponent implements OnInit {
  @ViewChild('fromInput') public fromInput: ElementRef;
  @ViewChild('toInput') public toInput: ElementRef;

  @Output() public fromDateChange = new EventEmitter<Date>(true);
  @Output() public toDateChange = new EventEmitter<Date>(true);
  @Output() public isValid = new EventEmitter<boolean>(true);

  @Input()
  public get fromDate() {
    return this.from;
  }
  public set fromDate(val: Date) {
    this.from = val;
    this.fromDateChange.emit(this.from);
  }

  @Input()
  public get toDate() {
    return this.to;
  }
  public set toDate(val: Date) {
    this.to = val;
    this.toDateChange.emit(this.to);
  }

  public from: Date;
  public to: Date;
  public dateRangeForm: FormGroup;

  public constructor(private formBuilder: FormBuilder, private eventLoop: EventLoop) {}

  public ngOnInit(): void {
    this.createForm();
    // this.from = moment()
    //   .subtract(1, 'month')
    //   .toDate();
    // this.to = moment().toDate();
    this.dateRangeForm.value.fromPicker = this.from;
    this.dateRangeForm.value.toPicker = this.to;
  }

  public createForm(): void {
    this.dateRangeForm = this.formBuilder.group({
      fromPicker: [this.from, Validators.required],
      toPicker: [this.to, Validators.required]
    });
  }

  public emitValidity(): void {
    this.from = this.dateRangeForm.value.fromPicker;
    this.to = this.dateRangeForm.value.toPicker;

    this.eventLoop.onNextTick().subscribe(() => {
      this.fromDateChange.emit(this.from);
      this.toDateChange.emit(this.to);
      this.isValid.emit(this.dateRangeForm.valid);
    });
  }

  public onFromInputFocus(picker: MatDatepicker<Date>): void {
    picker.open();
    this.eventLoop.onNextTick().subscribe(() => this.fromInput.nativeElement.focus());
  }

  public onToInputFocus(picker: MatDatepicker<Date>): void {
    picker.open();
    this.eventLoop.onNextTick().subscribe(() => this.toInput.nativeElement.focus());
  }

  public onOpenFromCalendar(): void {
    this.eventLoop.onNextTick().subscribe(() => this.fromInput.nativeElement.focus());
  }

  public onOpenToCalendar(): void {
    this.eventLoop.onNextTick().subscribe(() => this.toInput.nativeElement.focus());
  }

  public onCloseFromCalendar(): void {
    this.eventLoop.onNextTick().subscribe(() => this.fromInput.nativeElement.blur());
  }

  public onCloseToCalendar(): void {
    this.eventLoop.onNextTick().subscribe(() => this.toInput.nativeElement.blur());
  }

  public onTabAway(picker: MatDatepicker<Date>): void {
    if (!isNil(picker) && picker.opened === true) {
      picker.close();
    }
  }
}
