import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { TestType } from '../../../services/test-type/test-type.model';
import { TestTypeService } from '../../../services/test-type/test-type.service';
import { CheckboxGroup } from '../../../../../shared/utils/checkbox-group';
import { ReportCriteriaTestTypesModel, TestTypesRowItemModel } from '../../../models/test-type.model';

@Component({
  selector: 'app-reports-form-filter-test-types',
  templateUrl: './reports-form-filter-test-types.component.html',
  styleUrls: ['./reports-form-filter-test-types.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ReportsFormFilterTestTypesComponent implements OnInit, OnChanges {
  @Input() inputValues: string[];
  @Output() public selectionEvent = new EventEmitter<TestTypesRowItemModel[]>(true);
  @Output() public validityEvent = new EventEmitter<boolean>(true);

  public testTypesCriteria: Observable<ReportCriteriaTestTypesModel<TestTypesRowItemModel>[]>;

  public testTypes$: Observable<TestType[]> = new Observable<TestType[]>();

  public expand = true;
  public loading = true;
  private destroy$ = new Subject();
  private arrayIndex: string[] = [];

  public constructor(
    private testTypeService: TestTypeService
  ) {}

  public ngOnInit(): void {
    const array = new Array< ReportCriteriaTestTypesModel<TestTypesRowItemModel> >();

    this.testTypes$ = this.testTypeService
      .getTestTypeObservable()
      .pipe(takeUntil(this.destroy$));

    this.testTypes$.subscribe(testTypes => {
      testTypes.forEach( (testType) => {
        const newCriteria = new ReportCriteriaTestTypesModel< TestTypesRowItemModel >();
        newCriteria.checkBoxGroup = new CheckboxGroup<TestTypesRowItemModel>({
          selector: testTyping => testTyping.testName
        });
        const items = testType.tests.map(
          t => new TestTypesRowItemModel(t, testType.category)
        );
        newCriteria.checkBoxGroup.clear();
        newCriteria.checkBoxGroup.setItems(items);
        if (this.inputValues.length) {
          items.forEach( test => {
           if (this.inputValues.indexOf(test.testName) >= 0) {
            newCriteria.checkBoxGroup.select(test);
           }
          });
        } else {
          // All items selected by default
          items.forEach( test => {
            newCriteria.checkBoxGroup.select(test);
           });
        }
        newCriteria.checkBoxGroup.onSelection().subscribe(selectedTestTypes => {
          this.selectionEvent.emit(selectedTestTypes);
          this.validityEvent.emit(!!selectedTestTypes.length);
        });

        newCriteria.dataSource = of(items);
        newCriteria.testTypeCategory = testType.category;
        if ( this.arrayIndex.indexOf(newCriteria.testTypeCategory) < 0 ) {
          // if element doesn't exist on array
          // newCriteria is push on array
          this.arrayIndex.push(newCriteria.testTypeCategory);
          array.push(newCriteria);
        } else {
          // if element DOES exist on array
          // previous element is overwrited
          array[this.arrayIndex.indexOf(newCriteria.testTypeCategory)] = newCriteria;
        }
        this.testTypesCriteria = of(array);
      });
      this.loading = false;
    });
  }

  public ngOnChanges(changes: SimpleChanges): void {
  }
}
