import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReportsFormFilterTestTypesComponent } from './reports-form-filter-test-types.component';

describe('ReportsFormFilterTestTypesComponent', () => {
  let component: ReportsFormFilterTestTypesComponent;
  let fixture: ComponentFixture<ReportsFormFilterTestTypesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsFormFilterTestTypesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsFormFilterTestTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
