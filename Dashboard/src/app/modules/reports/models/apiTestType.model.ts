export interface APITestType {
    category: string;
    tests: string[];
}
