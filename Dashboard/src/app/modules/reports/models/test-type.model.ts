import { Observable } from 'rxjs';
import { CheckboxGroup } from '../../../shared/utils/checkbox-group';

export class ReportCriteriaTestTypesModel <T> {
    checkBoxGroup: CheckboxGroup<T>;
    dataSource: Observable<T[]>;
    testTypeCategory: string;
  }

  export class TestTypesRowItemModel {
    testName: string;
    categoryName: string;
    constructor(testName: string, categoryName: string) {
      this.testName = testName;
      this.categoryName = categoryName;
    }
  }
