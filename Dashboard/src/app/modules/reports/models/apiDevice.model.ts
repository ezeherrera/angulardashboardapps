export interface APIDevice {
  deviceType: string;
}

export class Device {
  public DeviceType: string;

  public constructor(init?: Partial<Device>) {
    Object.assign(this, init);
  }

  public static parseDevice(apiDevice: APIDevice): Device {
    return new Device({
      DeviceType: apiDevice ? apiDevice.deviceType : null
    });
  }
}
