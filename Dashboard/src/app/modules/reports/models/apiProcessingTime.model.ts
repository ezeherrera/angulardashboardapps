// Processing Time - Cigarette Chart
export class ApiProcessingTimeData {
  output: ApiProcessingTimeOutput[];
  input: ApiProcessingTimeInput;
}

export class ApiProcessingTimeInput {
  constructor(
    public deviceType: string,
    public sites: string[],
    public testTypes: string[],
    public from: Date,
    public to: Date) {}
}
export class ApiProcessingTimeOutput {
  public caption: string;
  public calue: number;
  public order: string;
}

// Processing Time Trends - Line Chart
export class ApiProcessingTimeTrendsData {
  output: ApiProcessingTimeTrendsOutput;
  input: ApiProcessingTimeTrendsInput;
}
export class ApiProcessingTimeTrendsInput {
  constructor(
    public deviceType: string,
    public sites: string[],
    public testTypes: string[],
    public from: Date,
    public to: Date,
  ) {}
}
export class ApiProcessingTimeTrendsOutput {
  public startDate: Date;
  public endDate: Date;
  public maxValue: number;
  public minValue: number;
  public results: [{
    caption: string,
    order: number,
    referenceDate: Date,
    value: number,
    week: number,
    year: number,
  }[]];
}
