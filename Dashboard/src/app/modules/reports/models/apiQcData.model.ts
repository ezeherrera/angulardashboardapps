export class APIQcDataInput {
  constructor(
    public deviceType: string,
    public sites: string[],
    public testTypes: string[],
    public from: Date,
    public to: Date) {

  }
}

export interface APIQcDataOutput {
  controlType: string;
  ct: number;
  lotNumber: string;
  timestamp: Date;
}

export class APIQcDataParsedOutput {
  controlMean: number; // for ex. 200mg/dl (= 200)
  standardDeviation: number; // for ex. 4mg/dl (= 4)
  controlLimit: number; // for ex. x3 standard deviation (= 3)
  data: any[]; // array of results by control lots (see below)
  title?: string;
  xAxisLabel?: string;
  yAxisLabel?: string;
}
