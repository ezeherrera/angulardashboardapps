export interface APISite {
  name: string;
}


export class Site {
  public Name: string;

  public constructor(init?: Partial<Site>) {
    Object.assign(this, init);
  }

  public static parseSite(apiDevice: APISite): Site {
    return new Site({
      Name: apiDevice ? apiDevice.name : null
    });
  }
}
