export class ApiReportInput {
  constructor(
    public deviceType: string,
    public sites: string[],
    public testTypes: string[],
    public from: Date,
    public to: Date) {
  }
}

export interface ApiReportData {
  numberOfResults: number;
  remark: string;
  percentage: number;
  percentageTotal: number;
}

export class ApiReportOutput {
    results: ApiReportData[];
    invalidResults: ApiReportData[];
    totals: ApiReportData[];
}
