import { DashboardRoutingModule } from './dashboard-routing.module';

describe('DashboardRoutingModule', () => {
  let reportsRoutingModule: DashboardRoutingModule;

  beforeEach(() => {
    reportsRoutingModule = new DashboardRoutingModule();
  });

  it('should create an instance', () => {
    expect(reportsRoutingModule).toBeTruthy();
  });
});
