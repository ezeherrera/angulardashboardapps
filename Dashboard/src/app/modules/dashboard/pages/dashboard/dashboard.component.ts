import { Component, OnInit } from '@angular/core';
import { ChartsGaugeModel } from '../../../../shared/charts/gauge/models/gauge.model';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  dataset: ChartsGaugeModel[];

  constructor(
    private palette: ChartsPaletteService
  ) {}

  ngOnInit(): void {

    /* MOCK DATA */
    const colors: string[] = this.palette.getColors(5);

    this.dataset = [
      {
        title: 'Site A',
        total: 620,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '220',
            value: 221,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Site B',
        total: 200,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '125',
            value: 125,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Site C',
        total: 337,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '157',
            value: 157,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Site D',
        total: 501,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '412',
            value: 412,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Site E',
        total: 360,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '198',
            value: 198,
            fill: colors[2],
          },
        ],
      },
      {
        title: 'Totals',
        total: 2256,
        sections: [
          { id: 1,
            caption: 'Success',
            label: '1581',
            value: 1581,
            fill: colors[2],
          },
        ],
      },
    ];
  }
}
