import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../../environments/environment';
import { SessionService } from '../../../../core/session/session.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading: boolean;
  version: string;
  envName: string;
  isProduction: boolean;

  constructor(
    private sessionService: SessionService,
    private route: ActivatedRoute,
  ) {
    this.isProduction = environment.production;
    this.version = environment.version;
    this.envName = environment.name;
    this.loading = true;
  }

  ngOnInit() {
    let callback = false;
    this.route.data.subscribe( data => (callback = data.callback) );
    if (!callback) {
      this.loading = false;
    }
  }

  logIn() {
    this.sessionService.logIn();
  }

}
