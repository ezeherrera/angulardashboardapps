import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { SessionStorageService } from '../session-storage/session-storage.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  isLoggedIn: Observable<boolean>;
  loggedIn: boolean;
  sessionUser: object;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private sessionStorage: SessionStorageService,
  ) {
    this.authService.authEvents.subscribe( event => this.handleAuthEvents(event) );
    this.isLoggedIn = this.authService.isLoggedIn;
    this.isLoggedIn.subscribe(
      logged => {
        if (this.loggedIn && !logged) {
          this.router.navigate(['/logout']);
        } else if (this.loggedIn && logged) {
          this.loggedIn = true;
        } else {
          this.loggedIn = false;
        }
      }
    );
  }

  logIn(): void {
    this.authService.logIn();
  }

  logOut(): void {
    this.authService.logOut();
    this.sessionStorage.clearData();
  }

  handleAuthEvents(event): void {
    switch (event.type) {
      case 'token_expires':
      case 'logout':
        const state = this.router.routerState.snapshot;
        this.sessionStorage.setData('return-url', state.url);
        this.router.navigate(['/login']);
      break;

      case 'token_received':
        this.sessionUser = this.authService.getIdentityClaims();
        const returnUrl = this.sessionStorage.getData('return-url');
        const url =  returnUrl || '/reports';
        this.router.navigate([url]);
        this.sessionStorage.removeData('return-url');
      break;
    }
  }
}
