import { Injectable } from '@angular/core';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class SessionStorageService {

  private userKey: string;

  constructor(
    private authService: AuthenticationService
  ) {
    this.authService.userData.subscribe(
      data => {
        if (data) {
          this.userKey = data['sub'];
        }
      }
    );
  }

  public setData(key: string, value: Object): void {
    if (this.userKey) {
      const stringValue = JSON.stringify(value);
      sessionStorage.setItem(`${this.userKey}_${key}`, stringValue);
    }
  }

  public getData(key: string): any {
    const value = sessionStorage.getItem(`${this.userKey}_${key}`);
    return JSON.parse(value);
  }

  public removeData(key): void {
    sessionStorage.removeItem(`${this.userKey}_${key}`);
  }

  public clearData(): void {
    sessionStorage.clear();
  }
}
