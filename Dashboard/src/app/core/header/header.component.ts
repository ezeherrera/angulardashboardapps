import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from '../session/session.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;

  constructor(
    private sessionService: SessionService,
  ) { }

  ngOnInit() {
    this.isLoggedIn$ = this.sessionService.isLoggedIn;
  }

  logOut() {
    this.sessionService.logOut();
  }
}
