import { Component, OnInit } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ReportService } from '../../modules/reports/services/report/report.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  version: string;
  envName: string;
  isProduction: boolean;

  constructor(
    private reportService: ReportService,
  ) {
    this.isProduction = environment.production;
    this.version = environment.version;
    this.envName = environment.name;
  }

  ngOnInit() {}

  resetFilters(): void {
    this.reportService.resetFilter();
  }
}
