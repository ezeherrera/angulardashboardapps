import { environment } from '../../../environments/environment';
import { AuthConfig } from 'angular-oauth2-oidc';

export const AUTH_CONFIG: AuthConfig = {
  clientId: environment.clientId,
  redirectUri: environment.redirectUri,
  issuer: environment.issuer,
  responseType: environment.responseType,
  scope: environment.scope,
  options: {
    responseMode: environment.responseMode,
    state: environment.state,
    nonce: environment.nonce
  },
  disableAtHashCheck: environment.disableAtHashCheck,
  requireHttps: environment.requireHttps,
  showDebugInformation: environment.showDebugInformation
};
