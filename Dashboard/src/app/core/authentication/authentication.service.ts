import { Injectable } from '@angular/core';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { Observable, BehaviorSubject } from 'rxjs';
import { AUTH_CONFIG } from './authentication.config';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {

  private loggedIn = new BehaviorSubject<boolean>(false);
  public userData = new BehaviorSubject<object>({});
  public authEvents: Observable<any> = new Observable;

  constructor(
    private oauthService: OAuthService,
  ) {
    this.authEvents = this.oauthService.events;
    this.authEvents.subscribe( event => this.handleOAuthEvents(event) );
    this.loggedIn.next(this.hasValidIdToken());
    this.userData.next(this.getIdentityClaims());
  }

  logIn() {
    this.oauthService.initImplicitFlow();
  }

  logOut() {
    this.oauthService.logOut(true);
  }

  hasValidIdToken() {
    return this.oauthService.hasValidIdToken();
  }

  async configureWithAuthConfig() {
    this.oauthService.configure(AUTH_CONFIG);
    this.oauthService.tokenValidationHandler = new JwksValidationHandler();
    this.oauthService.loadDiscoveryDocumentAndTryLogin();
  }

  public get isLoggedIn() {
    this.loggedIn.next(this.hasValidIdToken());
    return this.loggedIn.asObservable();
  }

  public getIdentityClaims() {
    const claims = this.oauthService.getIdentityClaims();
    return claims;
  }

  handleOAuthEvents(event) {
    switch (event.type) {
      case 'token_expires':
      case 'logout':
        this.loggedIn.next(false);
      break;
      case 'token_received':
        this.userData.next(this.getIdentityClaims());
        this.loggedIn.next(this.hasValidIdToken());
      break;
      default:
        this.userData.next(this.getIdentityClaims());
        this.loggedIn.next(this.hasValidIdToken());
      break;
    }
  }

}
