import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SvgIconComponent } from './components/svg-icon.component';
import { MatIconModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatIconModule,
  ],
  entryComponents: [
    SvgIconComponent,
  ],
  declarations: [
    SvgIconComponent
  ],
  exports: [
    MatIconModule,
    SvgIconComponent,
  ]
})
export class SvgIconModule { }
