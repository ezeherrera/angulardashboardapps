import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
// import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { PagedTableEmptyDirective } from './paged-table-empty.directive';
import { PagedTableFetchingDirective } from './paged-table-fetching.directive';
import { PagedTableHeaderDirective } from './paged-table-header.directive';
import { PagedTableItemDirective } from './paged-table-item.directive';
import { PagedTableComponent } from './paged-table.component';

@NgModule({
  imports: [
    CommonModule,
    // InfiniteScrollModule
  ],
  declarations: [
    PagedTableComponent,
    PagedTableEmptyDirective,
    PagedTableFetchingDirective,
    PagedTableHeaderDirective,
    PagedTableItemDirective
  ],
  exports: [
    CommonModule,
    // InfiniteScrollModule,
    PagedTableComponent,
    PagedTableEmptyDirective,
    PagedTableFetchingDirective,
    PagedTableHeaderDirective,
    PagedTableItemDirective
  ]
})
export class PagedTableModule {}
