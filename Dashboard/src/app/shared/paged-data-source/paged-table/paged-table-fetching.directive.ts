import { Directive } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pagedTableFetching]'
})
export class PagedTableFetchingDirective {}
