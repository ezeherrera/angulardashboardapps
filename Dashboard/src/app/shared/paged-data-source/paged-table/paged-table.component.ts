import {
  AfterViewInit,
  Component,
  ContentChild,
  ElementRef,
  HostBinding,
  Input,
  OnInit,
  TemplateRef,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter } from 'rxjs/operators';
import { PagedTableEmptyDirective } from './paged-table-empty.directive';
import { PagedTableFetchingDirective } from './paged-table-fetching.directive';
import { PagedTableHeaderDirective } from './paged-table-header.directive';
import { PagedTableItemDirective } from './paged-table-item.directive';

@Component({
  selector: 'app-paged-table',
  templateUrl: './paged-table.component.html',
  styleUrls: ['./paged-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PagedTableComponent<T = any> implements OnInit, AfterViewInit {
  @Input() public dataSource: Observable<T[]>;

  @HostBinding('class.app-paged-table') public selfClass = true;
  @ViewChild('container') public container: ElementRef;
  @ViewChild('content') public content: ElementRef;

  @ContentChild(PagedTableHeaderDirective, { read: TemplateRef })
  public headerTemplate: PagedTableHeaderDirective;
  @ContentChild(PagedTableItemDirective, { read: TemplateRef })
  public itemTemplate: PagedTableItemDirective;
  @ContentChild(PagedTableEmptyDirective, { read: TemplateRef })
  public emptyTemplate: PagedTableEmptyDirective;
  @ContentChild(PagedTableFetchingDirective, { read: TemplateRef })
  public fetchingTemplate: PagedTableFetchingDirective;

  private viewInit$ = new BehaviorSubject<boolean>(false);

  public ngOnInit(): void {
  }

  public ngAfterViewInit(): void {
    this.viewInit$.next(true);
  }

  public onViewInit(): Observable<boolean> {
    return this.viewInit$.asObservable().pipe(filter(Boolean));
  }
}
