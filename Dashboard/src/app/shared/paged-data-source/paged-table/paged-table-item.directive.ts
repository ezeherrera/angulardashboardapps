import { Directive } from '@angular/core';

@Directive({
  // tslint:disable-next-line:directive-selector
  selector: '[pagedTableItem]'
})
export class PagedTableItemDirective {}
