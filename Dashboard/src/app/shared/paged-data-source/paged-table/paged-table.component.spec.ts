import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { of } from 'rxjs/observable/of';
import { PagedDataSource } from '../paged-data-source';
import { PagedViewStatus } from '../paged-view-status-store';
import { PagedTableComponent } from './paged-table.component';
import { PagedTableModule } from './paged-table.module';

describe('PagedTableComponent', () => {
  let component: PagedTableComponent<string, string>;
  let fixture: ComponentFixture<PagedTableComponent<string, string>>;

  const dataSourceStub = new PagedDataSource<string>({
    pageSize: 1,
    fetch: () => of(['foo'])
  });

  const dataSourceEmptyStub = new PagedDataSource<string>({
    pageSize: 1,
    fetch: () => of([])
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      imports: [PagedTableModule]
    }).compileComponents();

    fixture = TestBed.createComponent(PagedTableComponent) as ComponentFixture<
      PagedTableComponent<string, string>
    >;
    component = fixture.componentInstance;

    component.dataSource = dataSourceStub;
  });

  it('should create', () => {
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should not emit changes on init', () => {
    const statuses = [] as PagedViewStatus[];
    component.onChanges.subscribe(status => {
      statuses.push(status);
    });

    fixture.detectChanges();

    expect(statuses).toEqual([]);
  });

  it(
    'should emit changes when the data source emits changes',
    fakeAsync(() => {
      component.fetchingTemplate = {};
      component.emptyTemplate = {};

      const statuses = [] as PagedViewStatus[];
      component.onChanges.subscribe(status => {
        statuses.push(status);
      });

      fixture.detectChanges();

      component.dataSource.goToFirstPage();
      tick();

      expect(statuses).toEqual([
        {
          fetching: true,
          fetchingPage: true,
          empty: false,
          end: false,
          previousCancelled: false,
          fetchOriginChange: {
            predicate: false,
            reset: false,
            page: true
          },
          fetchEnabled: false,
          firstFetch: true,
          emptyTemplateVisible: false,
          fetchingTemplateVisible: true
        },
        {
          fetching: false,
          fetchingPage: false,
          empty: false,
          end: false,
          previousCancelled: false,
          fetchOriginChange: {
            predicate: false,
            reset: false,
            page: false
          },
          fetchEnabled: true,
          firstFetch: false,
          emptyTemplateVisible: false,
          fetchingTemplateVisible: false
        }
      ]);
    })
  );

  it('should fetch the next page on scroll', () => {
    const spyNextPage = spyOn(component.dataSource, 'nextPage');

    fixture.detectChanges();

    component.onInfiniteScroll();

    expect(spyNextPage).toHaveBeenCalledTimes(1);
  });

  it(
    'should clear the list without fetching items',
    fakeAsync(() => {
      component.dataSource = dataSourceEmptyStub;

      const spyReset = spyOn(component.dataSource, 'reset').and.callThrough();

      const statuses = [] as PagedViewStatus[];
      component.onChanges.subscribe(status => {
        statuses.push(status);
      });

      fixture.detectChanges();

      component.dataSource.reset({ fetch: false });
      tick();

      expect(statuses).toEqual([
        {
          fetching: false,
          fetchingPage: false,
          empty: false,
          end: false,
          previousCancelled: false,
          fetchOriginChange: {
            predicate: false,
            reset: true,
            page: false
          },
          fetchEnabled: false,
          firstFetch: false,
          emptyTemplateVisible: false,
          fetchingTemplateVisible: false
        }
      ]);
      expect(spyReset).toHaveBeenCalledTimes(1);
      expect(spyReset.calls.argsFor(0)).toEqual([{ fetch: false }]);
    })
  );
});
