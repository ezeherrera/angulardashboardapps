import { NgModule } from '@angular/core';
import { PagedTableModule } from './paged-table/paged-table.module';

@NgModule({
  exports: [PagedTableModule]
})
export class PagedDataSourceModule {}
