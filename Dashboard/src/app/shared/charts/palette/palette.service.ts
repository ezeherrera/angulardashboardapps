import { Injectable } from '@angular/core';
import * as chroma from 'chroma-js';

@Injectable()
export class ChartsPaletteService {

  /*
   * Based on Material Design color palette
   * https://www.materialpalette.com/
  */

  public materialPalette: string[] = [
    '#E91E63', // #00 Pink
    '#2196f3', // #01 Blue
    '#8bc34a', // #02 Light Green
    '#ffeb3b', // #03 Yellow
    '#ff5722', // #04 Deep Orange
    '#f44336', // #05 Red
    '#03a9f4', // #06 Light Blue
    '#4caf50', // #07 Green
    '#ffc107', // #08 Amber
    '#9e9e9e', // #09 Grey
    '#9c27b0', // #10 Purple
    '#3f51b5', // #11 Indigo
    '#009688', // #12 Teal
    '#ff9800', // #13 Orange
    '#795548', // #14 Brown
    '#673ab7', // #15 Deep Purple
    '#00bcd4', // #16 Cyan
    '#cddc39', // #17 Lime
    '#607d8b', // #18 Blue Grey
    '#455A64', // #19 Dark Blue Grey
  ];

  public palettePairing: string[][] = [
    [
      this.materialPalette[3], // yellow
      this.materialPalette[4], // dark orange
    ],
    [
      this.materialPalette[6], // light blue
      this.materialPalette[11], // indigo
    ],
    [
      this.materialPalette[17], // lime
      this.materialPalette[7], // green
    ],
    [
      this.materialPalette[0], // pink
      this.materialPalette[15], // deep purple
    ],
  ];

  constructor() { }

  getColors(num?: number) {
    const n = num ? num : this.materialPalette.length;
    /*
     * If the num of required colors is higher than existing this.palettePairing
     * a auto-generated scale from palette paired colors will be provided
    */
    if (num > this.materialPalette.length) {
      return this.palettePairing.reduce( (result, item, index, array) => {
        const newScale = chroma
                          .scale([item[0], item[1]])
                          .correctLightness()
                          .colors( Math.round(n / array.length) );
        result.push(...newScale);
        return result;
      }, []);
    } else {
      return this.materialPalette;
    }
  }
}
