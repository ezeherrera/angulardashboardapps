
import { Component, DoCheck, ElementRef, Input, IterableDiffers, OnInit, ViewChild, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { ChartsLineGraphDataModel } from '../models/line.model';

@Component({
  selector: 'app-charts-line-graph',
  templateUrl: './line-graph.component.html',
  styleUrls: ['./line-graph.component.scss']
})

export class ChartsLineGraphComponent implements OnInit, DoCheck {

  @Input() data: ChartsLineGraphDataModel[];
  @Input() customScale: number;
  @Input() title?: string;
  @Input() maxValue: number;
  @Input() minValue: number;
  @Input() startDate: Date;
  @Input() endDate: Date;
  @Input() xAxisLabel?: string;
  @Input() yAxisLabel?: string;
  @Input() tooltipTrigger?: object;
  @Input() filters: any[]; /* show/hide controls */

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  private container: any;
  private svg: any;
  private g: any;
  private iterableDiffer: any;
  private resizeTimeout: any;

  // chart constant parameters
  private chartConfig = {
    defaultRadio: 2.8,
    hoverRadio: 7.5,
    lineStroke: 2,
    mouseTransitionDuration: 75,
    transitionDuration: 200,
  };

  constructor(
    private _iterableDiffers: IterableDiffers
  ) {
    // this code allow to detect changes on filter array
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
    console.log(this.chartConfig.transitionDuration);
    window.setTimeout(() => {
      this.createChart();
    }, 0);
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.filters);
    if (changes) {
        changes.forEachAddedItem( change => {
          change.item.forEach( item => {
            this.toggleSource(item);
          });
        });
    }
  }

  createChart = () => {
    // Set elements referents
    this.container = this.containerRef.nativeElement;
    this.svg = d3.select(this.container);
    const tooltip = d3.select(this.tooltipRef.nativeElement);

    // Set the dimensions of the canvas / graph
    const svgWidth = parseInt(this.svg.style('width'), 10);
    const svgHeight = parseInt(this.svg.style('height'), 10);
    const margin = { top: (this.title) ? 20 : 0, right: 20, bottom: 75, left: 50 };
    const width = svgWidth - margin.left - margin.right;
    const height = svgHeight - margin.top - margin.bottom;

    // Set the ranges
    const x = d3.scaleTime()
                .rangeRound([0, width])
                .domain([this.startDate, this.endDate]);
    const yDomaiDistance = (this.minValue - this.maxValue) * (10 / 100); // let upper and lower margin in the line bounds
    const yUpperDomain = (this.maxValue + Math.abs(yDomaiDistance));
    const yLowerDomain = this.minValue + yDomaiDistance;
    const y = d3.scaleLinear()
                .rangeRound([height, 0])
                .domain([yLowerDomain, yUpperDomain]);

    // set line generator
    const line = d3.line()
                   .x(function(d: any) { return x(d.date); })
                   .y(function(d: any) { return y(0); });


    this.g = this.svg
      .append('g')
      .attr('transform', `translate(${ margin.left }, ${ margin.top })`);
    const g = this.g;

    // Add background layer
    g.append( 'rect' )
      .attr('cx', x( x.domain()[0] ) )
      .attr('cy', y( y.domain()[0] ) )
      .attr('width', x( x.domain()[1] ) )
      .attr('height', y(yLowerDomain) )
      .attr('fill', 'rgba(30,30,30,.025)');

    // Set x-axis and label
    const xAxis = g.append('g');
    xAxis.attr('transform', `translate(0, ${ height })`)
      .call(
        d3.axisBottom(x)
        .ticks(25)
        .tickFormat( (d: Date) => ( d3.timeFormat('%Y, %W')(d))) )
      .append('text')
      .attr('fill', '#000')
      .attr('transform', `translate(${(width / 2)}, ${margin.bottom})`)
      .attr('text-anchor', 'end')
      .text(this.xAxisLabel)
      .style('font-size', '1.2em')
      .style('fill', '#808080');
    xAxis.selectAll('.tick') // Set tiks on x-axis
      .select('text')
      .attr('transform', 'rotate(-67.5)')
      .attr('dy', '-.5em')
      .attr('dx', '-3.5em')
      .attr('fill', '#808080');

    // Set y-axis and its label
    const yAxis = g.append('g');
    yAxis.call(
        d3.axisLeft(y).ticks(10)
      )
      .append('text')
      .text(this.yAxisLabel)
      .attr('fill', '#808080')
      .attr('text-anchor', 'end')
      .attr('transform', 'rotate(-90)')
      .attr('dx', '-10px')
      .attr('dy', '-3em')
      .style('font-size', '1.2em')
      .style('fill', '#808080');
    yAxis.selectAll('.tick') // Set tiks on x-axis
    .select('text')
    .attr('fill', '#808080');

    // gridlines in y axis function
    function make_y_gridlines() {
      return d3.axisLeft(y);
    }
    // add the Y gridlines
    const yGrid = this.svg.append('g')
      .attr('transform', `translate(${ margin.left }, ${ margin.top })`)
      .attr('class', 'grid')
      .call(
        make_y_gridlines()
        .tickSize(-width)
      );
    yGrid.selectAll('line')
         .attr('stroke-opacity', '0.055')
         .attr('shape-rendering', 'crispEdges');
    yGrid.selectAll('.domain').remove();
    yGrid.selectAll('text').remove();

    // get tooltip coord
    const tooltipCoords = (element) => {
      let xPos = 0;
      let yPos = 0;
      const bodyRect = this.container.getBoundingClientRect();
      const elementRect = element.getBoundingClientRect();
      yPos = elementRect.top - bodyRect.top;
      xPos = elementRect.left - bodyRect.left;
      return { left: xPos, top: yPos };
    };

    const {
      defaultRadio,
      hoverRadio,
      lineStroke,
      mouseTransitionDuration,
      transitionDuration
    } = this.chartConfig;

    // generate the chart
    this.data.forEach( function(source, index) {
      // print lines
      g.append('path')
        .datum(source.values)
        .attr('class', 'source-' + index + '__line')
        .attr('fill', 'none')
        .attr('stroke', source.fill)
        .attr('stroke-linejoin', 'round')
        .attr('stroke-linecap', 'round')
        .attr('stroke-width', lineStroke)
        .attr('d', line)
        .transition()
        .delay(transitionDuration * index)
        .duration(transitionDuration)
        .ease(d3.easeLinear)
        .attr('d', d3.line()
                .x(function(d: any) { return x(d.date); })
                .y(function(d: any) { return y(d.value); })
              );

      // print dots
      g.selectAll('dot')
      .data(source.values)
      .enter()
      .append('circle')
        // .attr('class', `source-${index}__dot`)
        .attr('class', 'source-' + index)
        .attr('fill', source.fill )
        .attr('cx', function(d) { return x(d.date); })
        .attr('cy', function(d) { return y(0); })
        .attr('r', defaultRadio)
        .transition()
        .duration(transitionDuration)
        .delay(transitionDuration * index)
        .ease(d3.easeLinear)
        .attr('cy', function(d) { return y(d.value); })
        .style('cursor', 'pointer')
        .on('end', function(r) {
          d3.select(this)
          .on('mouseover', function(d) {
            // highlight current element
            d3.select(this)
              .transition()
              .duration(mouseTransitionDuration)
              .attr('r', hoverRadio);
          })
          .on('click', function(d) {
            d3.select(this)
              .transition()
              .duration(mouseTransitionDuration)
              .style('stroke-width', '20')
              .style('stroke-opacity', '.5')
              .style('stroke', source.fill);
            /* tooltip incoming */
            const coords = tooltipCoords(this);
            tooltip
              .style('transform', 'scale(1) translate(0%,0%)')
              .style('opacity', '1')
              .style('left', (coords.left - 2) + 'px')
              .style('top', (coords.top - 36) + 'px')
              .html(`${d3.timeFormat('%Y-%m-%d %H:%I')(r.date)}: <strong style="color:#${source.fill}">${r.value}</strong>`);
          })
          .on('mouseout', function(d) {
            // restore current section
            d3.select(this)
              .transition()
              .duration(mouseTransitionDuration)
              .attr('r', defaultRadio)
              .style('stroke-width', '0')
              .style('stroke-opacity', '1');
            // tooltip outcoming
            tooltip
              .style('transform', 'translate(0,33%)')
              .style('opacity', '0');
          });
        });
    });

  }

  toggleSource(item) {
    const {
      defaultRadio,
      lineStroke,
    } = this.chartConfig;
    const newRatio = (item.hide) ? 0 : defaultRadio;
    const newStroke = (item.hide) ? 0 : lineStroke;
    const transitionDuration = 100;
    this.g.selectAll('.source-' + item.id)
          .transition()
          .duration(transitionDuration)
          .ease(d3.easeLinear)
          .attr('r', newRatio);
    this.g.selectAll('.source-' + item.id + '__line')
          .transition()
          .duration(transitionDuration)
          .ease(d3.easeLinear)
          .attr('stroke-width', newStroke);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // remove previous chart if exists
    this.svg.selectAll('g').remove();
    if (this.resizeTimeout) {
      window.clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = window.setTimeout(this.createChart, 500);
  }

}
