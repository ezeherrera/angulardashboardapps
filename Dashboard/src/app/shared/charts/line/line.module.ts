import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsLegendModule } from '../legend/legend.module';
import { ChartsLineGraphComponent } from './components/line-graph.component';
import { ChartsLineComponent } from './components/line.component';

@NgModule({
  imports: [
    CommonModule,
    ChartsLegendModule,
  ],
  declarations: [
    ChartsLineGraphComponent,
    ChartsLineComponent,
  ],
  exports: [
    ChartsLineComponent
  ],
})

export class ChartsLineModule {
}
