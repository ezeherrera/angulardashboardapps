export class ChartsLineGraphModel {
  constructor(
    public data: ChartsLineGraphDataModel[], // array of data blocks (see below)
    public title?: string, // chart's title
    public maxValue?: number, // set the maximum y-axis value
    public minValue?: number, // set the minumum y-axis value
    public startDate?: Date,  // set the starting x-axis value
    public endDate?: Date, // set the ending x-axis value
    public xAxisLabel?: string, // set x-axis label
    public yAxisLabel?: string, // set y-axis label
  ) {}
}

export class ChartsLineGraphDataModel {
  constructor(
    public caption: string, // section title
    public fill: string,   // HEX color code
    public values: {
      date: Date,
      value: number,
      label: string,   // text to display on tooltip
    }[],   // logic value
  ) {}
}
