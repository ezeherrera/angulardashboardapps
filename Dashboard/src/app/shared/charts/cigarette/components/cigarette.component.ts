
import { Component, Input, OnInit } from '@angular/core';
import { ChartsLegendModel } from '../../legend/models/legend.model';
import { ChartsCigarreteBarModel } from '../models/cigarette.model';

@Component({
  selector: 'app-charts-cigarette',
  templateUrl: './cigarette.component.html',
  styleUrls: ['./cigarette.component.scss']
})

export class ChartsCigaretteComponent implements OnInit {

  @Input() data: ChartsCigarreteBarModel[];
  @Input() legend: ChartsLegendModel;
  @Input() xAxisLabel: string;
  customScale: number;
  tooltipDispatcher: object;

  constructor() { }

  ngOnInit() {
    // if there's multiple charts
    // look for the global scal
    if (this.data.length > 1) {
      const globalScale = this.data.reduce(
        ( scale, item ) => {
          const totalValue = item.sections.reduce(
            (localScale, section) => ( localScale + section.value )
        , 0);
        return (totalValue > scale) ? totalValue : scale;
      }, 0);

      this.customScale = (!this.customScale || this.customScale < globalScale) ? globalScale : this.customScale;
    }
  }

  triggerTooltip($event): void {
    const id = $event.data.id;
    const type = $event.type;
    this.tooltipDispatcher = ({ id, type });
  }
}
