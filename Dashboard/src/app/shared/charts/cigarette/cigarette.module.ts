import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsLegendModule } from '../legend/legend.module';
import { ChartsCigaretteBarComponent } from './components/cigarette-bar.component';
import { ChartsCigaretteComponent } from './components/cigarette.component';

@NgModule({
  imports: [
    CommonModule,
    ChartsLegendModule,
  ],
  declarations: [
    ChartsCigaretteBarComponent,
    ChartsCigaretteComponent,
  ],
  exports: [
    ChartsCigaretteComponent
  ],
})

export class ChartsCigaretteModule {
}
