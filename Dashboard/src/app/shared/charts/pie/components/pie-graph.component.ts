import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { ChartsPieModel } from '../models/pie.model';

@Component({
  selector: 'app-charts-pie-graph',
  templateUrl: './pie-graph.component.html',
  styleUrls: ['./pie-graph.component.scss']
})
export class ChartsPieGraphComponent implements OnInit, OnChanges {

  @Input() data: ChartsPieModel[];
  @Input() isDonut?: boolean;
  @Input() tooltipTrigger?: object;

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  private container: any;
  private svg: any;

  private resizeTimeout: any;

  constructor() { }

  ngOnInit() {
    window.setTimeout(() => {
      this.createChart();
    }, 0);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tooltipTrigger) {
      const next = changes.tooltipTrigger.currentValue;
      if (typeof next !== 'undefined') {
        this.triggerMouseEvent(next.id, next.type);
      }
    }
  }

  createChart = () => {
    this.container = this.containerRef.nativeElement;
    this.svg = d3.select(this.container);
    const data = this.data;
    const width = parseInt(this.svg.style('width'), 10);
    const height = parseInt(this.svg.style('height'), 10);
    const radius = Math.min(width, height) / 2;

    // Arc Generators
    const defaultRadius = radius - 10;
    const hoverRadius = radius;
    const innerRadius = (!this.isDonut) ? 0 : (radius / 3);
    const arc = d3
      .arc()
      .outerRadius(defaultRadius)
      .innerRadius(innerRadius);
    const arcLarge = d3
      .arc()
      .outerRadius(hoverRadius)
      .innerRadius(innerRadius);

    // Tooltip stuff
    const tooltip = d3.select(this.tooltipRef.nativeElement);
    const tooltipCoords = (element) => {
      let x = 0;
      let y = 0;
      const parentRect = this.container.getBoundingClientRect();
      const elementRect = element.getBoundingClientRect();
      y = elementRect.top - parentRect.top;
      x = elementRect.left - parentRect.left;
      return { left: x, top: y };
    };
    const toggleTooltip = function(d, t) {
      if (d.state) {
        const coords = tooltipCoords(t);
        tooltip
          .style('transform', 'scale(1) translate(0%,0%)')
          .style('opacity', '1')
          .style('left', (coords.left - 8) + 'px')
          .style('top', (coords.top - 25) + 'px')
          .html(`<strong>${d.data.caption}:</strong> ${d.data.label}`);
      } else {
        tooltip
          .style('transform', 'translate(0,33%)')
          .style('opacity', '0');
      }
    };

    // onMouse Events
    const onMouseToggle = (d, t): void => {
      d.state = !d.state;
      const nextArc = d.state ? arcLarge : arc;
      const nextOpacity = d.state ? .75 : 1;
      d3.select(t)
        .transition()
        .duration(100)
        .style('opacity', nextOpacity)
        .attr('d', nextArc);
      toggleTooltip(d, t);
    };

    // Pie Creator
    const transitionDuration = 500;
    const transitionDelay = 500 / data.length;
    const g = this.svg
      .append('g')
      .attr('transform', `translate(${ width / 2 }, ${ height / 2 })`);
    const pie = d3.pie<ChartsPieModel>()
                  .sort(null)
                  .value( d => d.value);
    const arcs = g.selectAll('arc')
                  .data(pie(data))
                  .enter();
    arcs
      .append('path')
      .attr('d',
        d3
        .arc()
        .outerRadius(innerRadius + 1)
        .innerRadius(innerRadius)
      )
      .attr('id', (d, index) => `arc_${index}`)
      .attr('fill', d => d.data.fill)
      .style('cursor', 'pointer')
      .transition()
      .delay((d, index) => transitionDelay * index)
      .duration(transitionDuration)
      .attr('d', arc)
      .on('end', function(r) {
        d3.select(this)
          .on('mouseenter', function(d) { onMouseToggle(d, this); })
          .on('mouseleave', function(d) { onMouseToggle(d, this); });
      } );
  }

  triggerMouseEvent = (id: string, mouseEvent: string): void => {
    if (this.svg) {
      const target = this.svg.select('#arc_' + id);
      target.dispatch(mouseEvent);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // remove previous chart if exists
    this.svg.selectAll('g').remove();
    if (this.resizeTimeout) {
      window.clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = window.setTimeout(this.createChart, 500);
  }

}
