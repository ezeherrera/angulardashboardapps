import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsPieGraphComponent } from './pie-graph.component';

describe('ChartsPieGraphComponent', () => {
  let component: ChartsPieGraphComponent;
  let fixture: ComponentFixture<ChartsPieGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsPieGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsPieGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
