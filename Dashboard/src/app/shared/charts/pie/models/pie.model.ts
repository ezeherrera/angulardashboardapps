export class ChartsPieModel {
  constructor(
    public id: number, // Arc unique Id
    public caption: string, // arc title
    public label: string,   // text to display on tooltip
    public value: number,   // logic value
    public fill: string,   // color to fill
  ) {}
}
