import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChartsLegendCaptionModel } from '../models/legend.model';

@Component({
  selector: 'app-charts-legend',
  templateUrl: './legend.component.html',
  styleUrls: ['./legend.component.scss']
})
export class ChartsLegendComponent implements OnInit {

  @Input() header?: string = null;
  @Input() captions: ChartsLegendCaptionModel[];
  @Input() activeClass?: string; // disabled:(opacity:.6) | active: (background: rgba(black, .2))
  @Input() inColumns?: boolean; // Allow responsive column styles
  highColumns = false;
  listHeight = null;
  isClickable: boolean;
  active: boolean[];

  @Output() clickEvent = new EventEmitter();
  @Output() mouseEvent = new EventEmitter();

  constructor() {}

  ngOnInit() {
    this.active = this.captions.map( item => false);
    this.isClickable = (this.clickEvent.observers.length > 0) || (this.mouseEvent.observers.length > 0);
    this.activeClass = (this.activeClass) ? this.activeClass : null;

    if (this.inColumns) {
      let columnsNum = 2;
      if (this.captions.length > 12) {
        this.highColumns = true;
        columnsNum = 3;
      }
      this.listHeight = Math.ceil(this.captions.length  / columnsNum) * 40;
    }
  }

  onClick(caption, $event) {
    if (this.clickEvent.observers.length > 0) {
      $event['data'] = caption;
      this.clickEvent.emit($event);
      this.active[caption.id] = !this.active[caption.id];
    }
  }

  onMouse(caption, $event) {
    if (this.mouseEvent.observers.length > 0) {
      $event['data'] = caption;
      this.mouseEvent.emit($event);
    }
  }
}
