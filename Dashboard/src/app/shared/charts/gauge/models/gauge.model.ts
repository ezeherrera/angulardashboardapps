export class ChartsGaugeModel {
  public sections: ChartsGaugeModelSection[];
  public total: number;
  public halfCircle?: boolean;
  public title?: string;
}


export class ChartsGaugeModelSection {
  constructor(
    public id: number, // Arc unique Id
    public caption: string, // arc title
    public label: string,   // text to display on tooltip
    public value: number,   // logic value
    public fill: string,   // color to fill
  ) {}
}
