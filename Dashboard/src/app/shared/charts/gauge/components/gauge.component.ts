import { Component, Input, OnInit } from '@angular/core';
import { ChartsGaugeModelSection } from '../models/gauge.model';

@Component({
  selector: 'app-charts-gauge',
  templateUrl: './gauge.component.html',
  styleUrls: ['./gauge.component.scss']
})
export class ChartsGaugeComponent implements OnInit {

  @Input() title?: string;
  @Input() halfCircle?: boolean;
  @Input() sections: ChartsGaugeModelSection[];
  @Input() total: number;

  tooltipDispatcher: object;
  noData: boolean;

  constructor() {
    this.noData = false;
  }

  ngOnInit() {
    if (!this.sections || !this.sections.length) {
      this.noData = true;
    } else {
      const totalSection: ChartsGaugeModelSection = this.sections.reduce(
        (totalItem, item) => {
          totalItem.value -= item.value;
          return totalItem;
        }, {
          id: this.sections.length,
          caption: 'Total',
          label: `${this.total}`,
          value: this.total,
          fill: 'transparent',
        }
      );
      this.sections.push(totalSection);
    }
  }

  triggerTooltip($event): void {
    const id = $event.data.id;
    const type = $event.type;
    this.tooltipDispatcher = ({ id, type });
  }

}
