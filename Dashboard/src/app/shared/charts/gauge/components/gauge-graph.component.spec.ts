import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsGaugeGraphComponent } from './gauge-graph.component';

describe('ChartsGaugeGraphComponent', () => {
  let component: ChartsGaugeGraphComponent;
  let fixture: ComponentFixture<ChartsGaugeGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsGaugeGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsGaugeGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
