import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChartsPaletteService } from './palette/palette.service';

import { ChartsLegendModule } from './legend/legend.module';
import { ChartsCigaretteModule } from './cigarette/cigarette.module';
import { ChartsLeveyJenningsModule } from './levey-jennings/levey-jennings.module';
import { ChartsPieModule } from './pie/pie.module';
import { ChartsLineModule } from './line/line.module';
import { ChartsGaugeModule } from './gauge/gauge.module';

@NgModule({
  imports: [
    CommonModule,
    ChartsLegendModule,
    ChartsCigaretteModule,
    ChartsLeveyJenningsModule,
    ChartsPieModule,
    ChartsGaugeModule,
  ],
  exports: [
    ChartsCigaretteModule,
    ChartsLegendModule,
    ChartsLeveyJenningsModule,
    ChartsLineModule,
    ChartsPieModule,
    ChartsGaugeModule,
  ],
  providers: [
    ChartsPaletteService,
  ]
})
export class ChartsModule { }
