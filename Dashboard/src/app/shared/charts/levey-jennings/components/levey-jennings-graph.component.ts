/*************************
 * Based on James Westgard's QC Levey-Jennings Control Chart
 * https://www.westgard.com/lesson12.htm
 *
*************************/

import { Component, DoCheck, ElementRef, Input, IterableDiffers, OnInit, ViewChild, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { ChartsLeveyJenningsGraphModel } from '../models/levey-jennings.model';

@Component({
  selector: 'app-charts-levey-jennings-graph',
  templateUrl: './levey-jennings-graph.component.html',
  styleUrls: ['./levey-jennings-graph.component.scss']
})

export class ChartsLeveyJenningsGraphComponent implements OnInit, DoCheck {

  @Input() data: ChartsLeveyJenningsGraphModel[];
  @Input() controlMean: number; /* value for control mean line */
  @Input() xAxisLabel: string; /* label text on x-axis */
  @Input() yAxisLabel: string; /* label text on y-axis */
  @Input() upperScale: number; /* max value on y-axis */
  @Input() lowerScale: number; /* min value on y-axis */
  @Input() upperLimit: number; /* upper control limit */
  @Input() lowerLimit: number; /* lower control limit */
  @Input() maxMeasurementScale?: any; /* max measurement on x-axis */
  @Input() minMeasurementScale?: any; /* min measurement on x-axis */
  @Input() filters: any[]; /* show/hide controls */

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  private container: any;
  private svg: any;
  private g: any;
  private iterableDiffer: any;
  private resizeTimeout: any;

  constructor(
    private _iterableDiffers: IterableDiffers
  ) {
    // this code allow to detect changes on filter array
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
    if (this.data) {
      window.setTimeout(() => {
        this.createChart();
      }, 0);
    }
  }

  ngDoCheck() {
      const changes = this.iterableDiffer.diff(this.filters);
      if (changes) {
          changes.forEachAddedItem( change => {
            change.item.forEach( item => {
              this.toggleControl(item);
            });
          });
      }
  }

  createChart = () => {
    this.container = this.containerRef.nativeElement;
    this.svg = d3.select(this.container);
    const tooltip = d3.select(this.tooltipRef.nativeElement);

    const svgWidth = parseInt(this.svg.style('width'), 10);
    const svgHeight = parseInt(this.svg.style('height'), 10);
    const margin = { top: 0, right: 20, bottom: 75, left: 50 };
    const width = svgWidth - margin.left - margin.right;
    const height = svgHeight - margin.top - margin.bottom;

    this.g = this.svg
              .append('g')
              .attr('transform', `translate(${ margin.left }, ${ margin.top })`);
    const g = this.g;

    /* TO DO
     * if x-axis units are dates, get scaleTime
     * * if x-axis unit is number, get scaleLinear
     * if (minMeasurementScale instanceof Date)
    */
   /* Get scales of the chart */
    const x = d3.scaleTime()
              .domain([this.minMeasurementScale, this.maxMeasurementScale])
              .rangeRound([0, width]);
    const y = d3.scaleLinear()
              .domain([this.lowerScale, this.upperScale])
              .rangeRound([height, 0]);

    /* Add background layer */
    g.append( 'rect' )
      .attr('cx', x( x.domain()[0] ) )
      .attr('cy', y( y.domain()[0] ) )
      .attr('width', x( x.domain()[1] ) )
      .attr('height', y(this.lowerScale) )
      .attr('fill', 'rgba(30,30,30,.025)');

    /* Set x-axis and label */
    const xAxis = g.append('g');
    xAxis.attr('transform', `translate(0, ${ height })`)
      .call(
        d3.axisBottom(x)
        .ticks(25)
        .tickFormat((d: Date) => (d3.timeFormat('%Y-%m-%d')(d)))
      )
      .append('text')
      .attr('fill', '#000')
      .attr('transform', `translate(${(width / 2)}, ${margin.bottom})`)
      .attr('text-anchor', 'end')
      .text(this.xAxisLabel)
      .style('font-size', '1.2em')
      .style('fill', '#808080');
    /* Set tiks on x-axis */
    xAxis.selectAll('.tick')
      .select('text')
      .attr('transform', 'rotate(-67.5)')
      .attr('dy', '-.5em')
      .attr('dx', '-3.5em')
      .attr('fill', '#808080');

    /* Set y-axis and its label */
    g.append('g')
      .call(d3.axisLeft(y))
      .append('text')
      .attr('fill', '#000')
      .attr('text-anchor', 'end')
      .attr('transform', 'rotate(-90)')
      .attr('dx', '-15%')
      .attr('dy', '-3em')
      .text(this.yAxisLabel)
      .style('font-size', '1.2em')
      .style('fill', '#808080');

    /* Set control mean line */
    g.append( 'line' )
      .attr('x1', x( x.domain()[0] ))
      .attr('x2', x( x.domain()[1] ))
      .attr('y1', y( this.controlMean ))
      .attr('y2', y( this.controlMean ))
      .style('stroke', 'green')
      .style('stroke-dasharray', ('4,10'));

    /* Set upper control limit */
    g.append( 'line' )
      .attr('x1', x( x.domain()[0] ) )
      .attr('x2', x( x.domain()[1] ) )
      .attr('y1', y( this.upperLimit ) )
      .attr('y2', y( this.upperLimit ) )
      .style('stroke', 'red')
      .style('stroke-dasharray', ('4,10'));

    /* Set lower control limit */
    g.append( 'line' )
      .attr('x1', x( x.domain()[0] ))
      .attr('x2', x( x.domain()[1] ))
      .attr('y1', y( this.lowerLimit ))
      .attr('y2', y( this.lowerLimit ))
      .style('stroke', 'red')
      .style('stroke-dasharray', ('4,10'));

    /* get tooltip coord */
    const tooltipCoords = (element) => {
      let xPos = 0;
      let yPos = 0;
      const bodyRect = this.container.getBoundingClientRect();
      const elementRect = element.getBoundingClientRect();
      yPos = elementRect.top - bodyRect.top;
      xPos = elementRect.left - bodyRect.left;
      return { left: xPos, top: yPos };
    };

    /* Insert data results into graph */
    const defaultRadio = 2.8;
    const hoverRadio = 7.5;
    const mouseTransitionDuration = 75;
    let accumulatedDuration = 0;
    this.data.forEach( function(control, index, data) {
      const transitionDuration = (1000 / control.values.length);
      g.selectAll('dot')
        .data(control.values)
        .enter()
        .append('circle')
          .attr('class', 'control-' + index)
          .attr('fill', control.fill )
          .attr('cx', function(d) { return x(d.measurement); })
          .attr('cy', function(d) { return y(d.value); })
          .attr('r', 0)
          .transition()
          .duration(transitionDuration)
          .ease(d3.easeLinear)
          .attr('r', defaultRadio)
          .delay((d, i) => {
            accumulatedDuration += ((transitionDuration / 100) * i);
            return accumulatedDuration;
          })
          .style('cursor', 'pointer')
          .on('end', function(r) {
            d3.select(this)
            .on('mouseover', function(d) {
              // highlight current element
              d3.select(this)
                .transition()
                .duration(mouseTransitionDuration)
                .attr('r', hoverRadio);
            })
            .on('click', function(d) {
              d3.select(this)
                .transition()
                .duration(mouseTransitionDuration)
                // .attr('r', hoverRadio)
                .style('stroke-width', '20')
                .style('stroke-opacity', '.5')
                .style('stroke', control.fill);
              /* tooltip incoming */
              const coords = tooltipCoords(this);
              tooltip
                .style('transform', 'scale(1) translate(0%,0%)')
                .style('opacity', '1')
                .style('left', (coords.left - 2) + 'px')
                .style('top', (coords.top - 36) + 'px')
                .html(`${d3.timeFormat('%Y-%m-%d %H:%I')(r.measurement)}: <strong style="color:#${control.fill}">${r.value}</strong>`);
            })
            .on('mouseout', function(d) {
              // restore current section
              d3.select(this)
                .transition()
                .duration(mouseTransitionDuration)
                .attr('r', defaultRadio)
                .style('stroke-width', '0')
                .style('stroke-opacity', '1');
              // tooltip outcoming
              tooltip
                .style('transform', 'translate(0,33%)')
                .style('opacity', '0');
            });
          });

      }, this);
  }

  toggleControl(item) {
    const newRatio = (item.hide) ? 0 : 2.8;
    const transitionDuration = 100;
    this.g.selectAll('.control-' + item.id)
          .transition()
          .duration(transitionDuration)
          .ease(d3.easeLinear)
          .attr('r', newRatio);
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // remove previous chart if exists
    this.svg.selectAll('g').remove();
    if (this.resizeTimeout) {
      window.clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = window.setTimeout(this.createChart, 500);
  }

}
