/*************************
 * Based on James Westgard's QC Levey-Jennings Control Chart
 * https://www.westgard.com/lesson12.htm
 *
*************************/

import { Component, Input, OnInit } from '@angular/core';
import { ChartsLegendModel } from '../../legend/models/legend.model';
import { ChartsLeveyJenningsGraphModel } from '../models/levey-jennings.model';

@Component({
  selector: 'app-chartslevey-jennings',
  templateUrl: './levey-jennings.component.html',
  styleUrls: ['./levey-jennings.component.scss']
})

export class ChartsLeveyJenningsComponent implements OnInit {

  @Input() controlLimit: number; // for ex. x3 standard deviation (= 3)
  @Input() controlMean: number; // for ex. 200mg/dl (= 200)
  @Input() data: ChartsLeveyJenningsGraphModel[];
  @Input() legend: ChartsLegendModel;
  @Input() title?: string;
  @Input() standardDeviation: number; // for ex. 4mg/dl (= 4)
  @Input() xAxisLabel?: string;
  @Input() yAxisLabel?: string;
  lowerLimit: number;
  upperLimit: number;
  lowerScale: number;
  upperScale: number;
  minMeasurementScale: any;
  maxMeasurementScale: any;
  filters: any[]; /* control chart filters status */
  filtersDispatcher: any[]; /* dispatch filter changes to graph component */
  noData: boolean;

  constructor() {
    this.filters = [];
    this.filtersDispatcher = [];
    this.noData = false;
  }

  ngOnInit() {
    if (!this.data || !this.data.length) {
      this.noData = true;
    } else {
      const { minValue: minMeasurement, maxValue: maxMeasurement } = this.getMinMaxValueFromProp(this.data, 'measurement');
      this.minMeasurementScale = minMeasurement;
      this.maxMeasurementScale = maxMeasurement;
      this.upperLimit = this.controlMean + (this.standardDeviation * this.controlLimit);
      this.lowerLimit = this.controlMean - (this.standardDeviation * this.controlLimit);
      this.upperScale = this.upperLimit + (this.standardDeviation * 1.25);
      this.lowerScale = this.lowerLimit - (this.standardDeviation * 1.25);
    }
  }

  getMinMaxValueFromProp(data, propName) {
    return data.reduce(
      (output, item) => {
        item.values.reduce( (comparator, value) => {
          comparator.minValue =
            (!comparator.minValue || value[propName] < comparator.minValue) ?
            value[propName] : comparator.minValue;
          comparator.maxValue =
            (!comparator.maxValue || value[propName] > comparator.maxValue) ?
            value[propName] : comparator.maxValue;
          return comparator;
        }, output);
        return output;
      },
      { minValue: null, maxValue: null }
    );
  }

  filterChart(event): void {
    const data = event.data;
    if (!this.filters[data.id]) {
      this.filters[data.id] = {
        id: data.id,
        hide: true,
      };
    } else {
      if (this.filters[data.id].hide) {
        this.filters[data.id].hide = false;
      } else {
        this.filters[data.id].hide = true; }
    }
    this.filtersDispatcher.push(this.filters);
  }

}
