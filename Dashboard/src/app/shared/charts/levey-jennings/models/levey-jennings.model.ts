
export class ChartsLeveyJenningsModel {
  constructor(
    public controlMean: number, // for ex. 200mg/dl (= 200)
    public standardDeviation: number, // for ex. 4mg/dl (= 4)
    public controlLimit: number, // for ex. x3 standard deviation (= 3)
    public data: ChartsLeveyJenningsGraphModel[], // array of results by control lots (see below)
    public title?: string,
    public xAxisLabel?: string,
    public yAxisLabel?: string,
  ) {}
}

export class ChartsLeveyJenningsGraphModel {
  constructor(
    public caption: string,
    public fill: string,
    public values: {
      measurement: any, // section title
      value: number,   // text to display on tooltip
    }[],
  ) {}
}
