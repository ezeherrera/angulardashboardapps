import { SelectionModel } from '@angular/cdk/collections';
import { Observable, ReplaySubject } from 'rxjs';
import { map } from 'rxjs/operators';

export interface CheckboxGroupOptions<T> {
  selector?: (item: T) => any;
}

export class CheckboxGroup<T = any> {
  private items: T[] = [];
  private selectorValues = new Map<any, T>();
  private selectedItems = new SelectionModel<T>(true, []);
  private onSelection$ = new ReplaySubject<T[]>(1);

  public constructor({ selector }: CheckboxGroupOptions<T> = {}) {
    if (selector) {
      this.selector = selector;
    }

    this.selectedItems.onChange
      .pipe(
        map(change =>
          change.source.selected.map(itemSelectorValue =>
            this.selectorValues.get(itemSelectorValue)
          )
        )
      )
      .subscribe(selectedItems => {
        this.onSelection$.next(selectedItems.slice());
      });
  }

  public setItems(items: T[]): void {
    this.items = items;
  }

  public getItems(): T[] {
    return this.items;
  }

  public onSelection(): Observable<T[]> {
    return this.onSelection$.asObservable();
  }

  public toggleAll(): void {
    if (this.isAllSelected()) {
      this.clear();
    } else {
      this.selectAll();
    }
  }

  public selectAll(): void {
    this.select(...this.items);
  }

  public getSelected(): T[] {
    return this.selectedItems.selected.map(itemSelectorValue =>
      this.selectorValues.get(itemSelectorValue)
    );
  }

  public clear(): void {
    this.selectorValues.clear();
    this.selectedItems.clear();
  }

  public select(...items: T[]): void {
    items.forEach(item => {
      this.selectorValues.set(this.selector(item), item);
    });
    this.selectedItems.select(...items.map(item => this.selector(item)));
  }

  public toggle(item: T): void {
    this.selectorValues.set(this.selector(item), item);
    this.selectedItems.toggle(this.selector(item));
  }

  public isSelected(item: T): boolean {
    return this.selectedItems.isSelected(this.selector(item));
  }

  public isNotAllSelected(): boolean {
    return (
      this.selectedItems.selected.length > 0 &&
      this.items.length !== this.selectedItems.selected.length
    );
  }

  public isAllSelected(): boolean {
    return (
      this.selectedItems.selected.length > 0 &&
      this.items.length === this.selectedItems.selected.length
    );
  }

  public isNoneSelected(): boolean {
    return this.selectedItems.selected.length === 0;
  }

  private selector(item: T): any {
    return item;
  }
}
