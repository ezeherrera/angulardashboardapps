/**
 * Checks whteher a value is null or undefined.
 */
export function isNil(value: any): boolean {
  return value === null || value === undefined;
}

/**
 * Checks whteher a string is null nor undefined nor empty.
 */
export function isNilString(value: string): boolean {
  return value === null || value === undefined || value === '';
}


export function parseApiDate(date: Date): Date {
  return date ? new Date(date) : null;
}

/**
 * @description Inputs validations string keys
 */
export enum ErrorMessage {
  REQUIRED = 'This field is required',
  EMAIL = 'E-mail is invalid',
  PATTERN = 'Field contains invalid characters',
  MISMATCH = 'Password mismatch'
}

export function getDateUTC(date?: Date): Date {
  if (isNil(date)) {
    date = new Date();
  }
  const utcDate = new Date(
    Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0, 0)
  );
  return utcDate;
}

export function getDateUTCToISOString(date?: Date): string {
  return getDateUTC(date).toISOString();
}
