import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

/* istanbul ignore next */
@Injectable()
export class EventLoop {
  public onNextTick(): Observable<void> {
    return Observable.create(observer => {
      setTimeout(() => {
        observer.next();
        observer.complete();
      });
    });
  }
}
