/*  core modules */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { environment } from '../environments/environment';

/*  app root component */
import { AppComponent } from './app.component';

/* Services */
import { OAuthModule } from 'angular-oauth2-oidc';
import { HttpClientModule } from '@angular/common/http';

/* app core components */
import { HeaderComponent } from './core/header/header.component';
import { NavigationComponent } from './core/navigation/navigation.component';
import { SidenavComponent } from './core/sidenav/sidenav.component';

/* app shared modules */
import { MaterialModule } from './shared/material/material.module';
import { SvgIconModule } from './shared/svg-icon/svg-icon.module';

/* app feature modules */
import { LoginModule } from './modules/login/login.module';
import { DashboardModule } from './modules/dashboard/dashboard.module';
import { ReportsModule } from './modules/reports/reports.module';
import { PlaygroundModule } from './modules/playground/playground.module';

/***
 * The bootstraping module
 ***/
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OAuthModule.forRoot({
      resourceServer: {
          allowedUrls: [environment.apiReportingServiceUrl],
          sendAccessToken: true
      }
    }),
    HttpClientModule,
    LoginModule,
    ReportsModule,
    PlaygroundModule,
    AppRoutingModule,
    MaterialModule,
    SvgIconModule,
    DashboardModule,
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
