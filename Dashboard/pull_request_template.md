<!--

# Title
- Put [WIP] in front of the Pull Request that you are working on
- Clear [WIP] after you completed and ping the reviewer

-->

#### Status

READY / IN DEVELOPMENT / HOLD / FIXED

#### Which tasks does this PR accomplish?

- [x] #XXX (stories/task IDs will be recognized by TFS)
- [ ] #XXX
- [ ] ...


### Steps to reproduce

1. First step
2. Second step
3. Third step

### Previous behaviour

[ What happens before PR... ]

### Expected behaiour

[ What is expected to happens after PR... ]

### Actual behaviour

[ What actually happens after PR ]


#### Testing

- [x] Chrome
- [] Firefox
- [] IE
- [] Opera (...really?)

#### Builds

- [] Dev
- [] Test
- [] Production

#### More info
 
 (Links to wiki or any other related information)

#### Screenshots (if appropriate)
