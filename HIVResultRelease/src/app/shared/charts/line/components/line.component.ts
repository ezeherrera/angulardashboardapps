
import { Component, Input, OnInit } from '@angular/core';
import { ChartsLineGraphModel } from '../models/line.model';
import { ChartsLegendModel } from '../../legend/models/legend.model';

@Component({
  selector: 'app-charts-line',
  templateUrl: './line.component.html',
  styleUrls: ['./line.component.scss']
})

export class ChartsLineComponent implements OnInit {

  @Input() dataset: ChartsLineGraphModel;
  @Input() legend: ChartsLegendModel;
  filters: any[] = []; /* control chart filters status */
  filtersDispatcher: any[] = []; /* dispatch filter changes to graph component */

  constructor() { }

  ngOnInit() {
  }

  filterChart(event): void {
    const data = event.data;
    if (!this.filters[data.id]) {
      this.filters[data.id] = {
        id: data.id,
        hide: true,
      };
    } else {
      if (this.filters[data.id].hide) {
        this.filters[data.id].hide = false;
      } else {
        this.filters[data.id].hide = true; }
    }
    this.filtersDispatcher.push(this.filters);
  }
}
