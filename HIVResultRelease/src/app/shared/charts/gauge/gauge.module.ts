import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsGaugeGraphComponent } from './components/gauge-graph.component';
import { ChartsGaugeComponent } from './components/gauge.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ChartsGaugeComponent,
    ChartsGaugeGraphComponent,
  ],
  exports: [
    ChartsGaugeComponent,
  ],
})
export class ChartsGaugeModule { }
