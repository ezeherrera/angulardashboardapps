import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChartsGaugeComponent } from './gauge.component';

describe('ChartsGaugeComponent', () => {
  let component: ChartsGaugeComponent;
  let fixture: ComponentFixture<ChartsGaugeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChartsGaugeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChartsGaugeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
