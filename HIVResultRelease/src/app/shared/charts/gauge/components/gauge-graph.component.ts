import { Component, ElementRef, Input, OnInit, ViewChild, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { ChartsGaugeModelSection } from '../models/gauge.model';

@Component({
  selector: 'app-charts-gauge-graph',
  templateUrl: './gauge-graph.component.html',
  styleUrls: ['./gauge-graph.component.scss']
})
export class ChartsGaugeGraphComponent implements OnInit {

  @Input() data: ChartsGaugeModelSection[] = [];
  @Input() total: number;
  @Input() halfCircle?: boolean;
  @Input() tooltipTrigger?: object;

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  private container: any;
  private svg: any;

  private resizeTimeout: any;

  constructor() { }

  ngOnInit() {
    window.setTimeout(() => {
      this.createChart();
    }, 0);
  }

  /*****************
  * @description Chart genereator function
  * **************/
  createChart = () => {
    this.container = this.containerRef.nativeElement;
    this.svg = d3.select(this.container);
    const data = this.data;
    const partial: number = data.reduce(
      (p, item) => {
        p += item.value;
        return p;
      }, 0);
    const width = parseInt(this.svg.style('width'), 10);
    const height = parseInt(this.svg.style('height'), 10);
    const radius = Math.min(width, height) / 2;

    /*****************
     * @description Arc Generators
     * **************/
    const defaultRadius = radius - 10;
    const hoverRadius = radius;
    const innerRadius = (radius * 0.8);
    const arc = d3
      .arc()
      .outerRadius(defaultRadius)
      .innerRadius(innerRadius);
    const arcLarge = d3
      .arc()
      .outerRadius(hoverRadius)
      .innerRadius(innerRadius);

    /*****************
     * @description Tooltip stuff
     * **************/
    const tooltip = d3.select(this.tooltipRef.nativeElement);
    const tooltipClass = tooltip.attr('class');
    const tooltipCoords = (element) => {
      let xlPos = null;
      let xrPos = null;
      let yPos = 0;
      const bodyRect = this.container.getBoundingClientRect();
      const elementRect = element.getBoundingClientRect();
      yPos = elementRect.top - bodyRect.top;
      if (elementRect.left - bodyRect.left > bodyRect.width / 2) {
        xrPos = bodyRect.right - elementRect.right + (elementRect.width / 2);
      } else {
        xlPos = elementRect.left - bodyRect.left + (elementRect.width / 2);
      }
      return { left: xlPos, right: xrPos, top: yPos };
    };

    const toggleTooltip = function(d, t) {
      if (d.state) {
        const coords = tooltipCoords(t);
        tooltip
          .attr('class', () => coords.left ? tooltipClass + ` ${tooltipClass}--left` : tooltipClass + ` ${tooltipClass}--right` )
          .style('transform', 'scale(1) translate(0%,0%)')
          .style('opacity', '1')
          .style('left', () => coords.left ? (coords.left - 8) + 'px' : 'initial')
          .style('right', () => coords.right ? (coords.right - 8) + 'px' : 'initial')
          .style('top', (coords.top - 35) + 'px')
          .html(`<strong>${d.data.caption}:</strong> ${d.data.label}`);
      } else {
        tooltip
          .style('transform', 'translate(0,33%)')
          .style('opacity', '0')
          .attr('class', tooltipClass);
      }
    };

    /*****************
     * @description onMouse Events
     * **************/
    const onMouseToggle = (d, t): void => {
      d.state = !d.state;
      const nextArc = d.state ? arcLarge : arc;
      d3.select(t)
        .transition()
        .duration(100)
        .attr('d', nextArc);
      toggleTooltip(d, t);
    };

    /*****************
     * @description Creating Gauge path
     * **************/
    const defaultStartAngle = (this.halfCircle) ? -1 * Math.PI / 2 : -1 * Math.PI;
    const defaultEndAngle = (this.halfCircle) ? 1 * Math.PI / 2 : 1 * Math.PI;
    const g = this.svg
      .append('g')
      .attr('transform', `translate(${ width / 2 }, ${ height / 2 })`);
    const pie = d3.pie<ChartsGaugeModelSection>()
                  .startAngle(defaultStartAngle)
                  .endAngle(defaultEndAngle)
                  .sort(null)
                  .value( d => d.value);
    const arcs = g.selectAll('arc')
                  .data(pie(data))
                  .enter();
    // Nackground path
    g.append('path')
     .datum({startAngle: defaultStartAngle, endAngle: defaultEndAngle})
     .attr('fill', '#ddd')
     .attr('d', arc);

    /*****************
     * @description Generating Gauge sections
     * **************/
    const transitionDuration = 1000;
    let accumulatedDelay = 0;
    arcs
      .append('path')
      .attr('d', d => {
        const newD = JSON.parse(JSON.stringify(d));
        newD.endAngle = newD.startAngle;
        return arc(newD);
      })
      .attr('id', (d, index) => `arc_${index}`)
      .attr('fill', d => d.data.fill)
      .style('cursor', 'pointer')
      .transition()
      .delay((d, index) => {
        if (index < data.length) {
          const delay = accumulatedDelay;
          accumulatedDelay += transitionDuration * (d.value / partial);
          return delay;
        } else {
          return 0;
        }
      })
      .ease(d3.easeLinear)
      .duration( (d, index) => {
        if (index < data.length) {
          return transitionDuration * (d.value / partial);
        } else {
          return 0;
        }
      })
      .attrTween('d', d => {
        const interpolate = d3.interpolate(d.startAngle, d.endAngle);
        return function(t) {
          d.endAngle = interpolate(t);
          return arc(d);
        };
      })
      .on('end', function(r) {
        d3.select(this)
          .on('mouseenter', function(d) { onMouseToggle(d, this); })
          .on('mouseleave', function(d) { onMouseToggle(d, this); });
      } );

      const partialData = JSON.parse(JSON.stringify(data));
            partialData.splice(-1); // data without total
      const text = this.svg.append('g')
                           .attr('transform', `translate(${ width / 2 }, 0)`)
                           .selectAll('text')
                           .data(pie(partialData))
                           .enter();
      text
        .append('text')
          .text(d => `${d.data.caption} ${d.data.label}`)
          .attr('y', (d, i) => {
            const center = (height + 30) / 2;
            const topY = center - ((24 * partialData.length) / 2) ;
            const currentY = topY + (24 * i);
            return currentY;
          })
          .attr('text-anchor', 'middle')
          .attr('fill', d => d.data.fill)
          .style('font-size', '1em')
          .style('font-weight', '800')
          .insert('tspan')
            .text(d => `/${this.total}`)
            .style('font-weight', '400')
            .attr('fill', '#808080');
  }

  triggerMouseEvent = (id: string, mouseEvent: string): void => {
    if (this.svg) {
      const target = this.svg.select('#arc_' + id);
      target.dispatch(mouseEvent);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // remove previous chart if exists
    this.svg.selectAll('g').remove();
    if (this.resizeTimeout) {
      window.clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = window.setTimeout(this.createChart, 500);
  }

}
