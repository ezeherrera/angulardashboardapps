import { inject, TestBed } from '@angular/core/testing';

import { ChartsPaletteService } from './palette.service';

describe('ChartsPaletteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChartsPaletteService]
    });
  });

  it('should be created', inject([ChartsPaletteService], (service: ChartsPaletteService) => {
    expect(service).toBeTruthy();
  }));
});
