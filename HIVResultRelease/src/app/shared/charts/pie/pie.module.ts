import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsLegendModule } from '../legend/legend.module';
import { ChartsPieGraphComponent } from './components/pie-graph.component';
import { ChartsPieComponent } from './components/pie.component';

@NgModule({
  imports: [
    CommonModule,
    ChartsLegendModule,
  ],
  declarations: [
    ChartsPieComponent,
    ChartsPieGraphComponent,
  ],
  exports: [
    ChartsPieComponent,
  ],
})
export class ChartsPieModule { }
