import { Component, Input, OnInit } from '@angular/core';
import { ChartsLegendModel } from '../../legend/models/legend.model';
import { ChartsPieModel } from '../models/pie.model';

@Component({
  selector: 'app-charts-pie',
  templateUrl: './pie.component.html',
  styleUrls: ['./pie.component.scss']
})
export class ChartsPieComponent implements OnInit {

  @Input() legend: ChartsLegendModel;
  @Input() data: ChartsPieModel[];
  @Input() isDonut?: boolean;
  @Input() title?: string;
  @Input() legendInColumns?: boolean;
  tooltipDispatcher: object;
  noData: boolean;

  constructor() {
    this.noData = false;
  }

  ngOnInit() {
    if (!this.data || !this.data.length) {
      this.noData = true;
    }
    this.legendInColumns = (this.legendInColumns === false) ? false : true;
  }

  triggerTooltip($event): void {
    const id = $event.data.id;
    const type = $event.type;
    this.tooltipDispatcher = ({ id, type });
  }

}
