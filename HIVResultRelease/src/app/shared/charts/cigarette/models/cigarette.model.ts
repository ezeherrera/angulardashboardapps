export class ChartsCigarreteBarModel {
  constructor(
    public sections: ChartsCigarreteBarSectionModel[], // array of sections (see below)
    public title?: string,
  ) {}
}

export class ChartsCigarreteBarSectionModel {
  constructor(
    public caption: string, // section title
    public label: string,   // text to display on tooltip
    public value: number,   // logic value
    public fill: string,   // logic value
  ) {}
}
