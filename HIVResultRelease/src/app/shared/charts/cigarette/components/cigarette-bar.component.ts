
import { Component, ElementRef, Input, OnChanges, OnInit, SimpleChanges, ViewChild, HostListener } from '@angular/core';
import * as d3 from 'd3';
import { ChartsCigarreteBarSectionModel } from '../models/cigarette.model';

@Component({
  selector: 'app-charts-cigarette-bar',
  templateUrl: './cigarette-bar.component.html',
  styleUrls: ['./cigarette-bar.component.scss']
})

export class ChartsCigaretteBarComponent implements OnInit, OnChanges {

  @Input() sections: ChartsCigarreteBarSectionModel[];
  @Input() customScale: number;
  @Input() xAxisLabel: number;
  @Input() tooltipTrigger?: object;

  @ViewChild('container') containerRef: ElementRef;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  private container: any;
  private svg: any;
  private resizeTimeout: any;

  constructor() {}

  ngOnInit() {
    window.setTimeout(() => {
      this.createChart();
    }, 0);
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.tooltipTrigger) {
      const next = changes.tooltipTrigger.currentValue;
      if (typeof next !== 'undefined') {
        this.triggerMouseEvent(next.id, next.type);
      }
    }
  }

  createChart = () => {
    this.container = this.containerRef.nativeElement;
    this.svg = d3.select(this.container);

    const tooltip = d3.select(this.tooltipRef.nativeElement);
    const tooltipClass = tooltip.attr('class');
    const svgWidth = parseInt(this.svg.style('width'), 10);
    const svgHeight = parseInt(this.svg.style('height'), 10);
    const sections = this.sections;
    const xScale = d3.scaleLinear()
      .domain([this.customScale || d3.sum(sections.map(i => i.value)), 0])
      .range([svgWidth, 0]);
    // get tooltip coord
    const tooltipCoords = (element) => {
      let xlPos = null;
      let xrPos = null;
      let yPos = 0;
      const bodyRect = this.container.getBoundingClientRect();
      const elementRect = element.getBoundingClientRect();
      yPos = elementRect.top - bodyRect.top;
      if (bodyRect.width - elementRect.left < 0) {
        xrPos = bodyRect.right - elementRect.right;
      } else {
        xlPos = elementRect.left - bodyRect.left;
      }
      return { left: xlPos, right: xrPos, top: yPos };
    };
    const transitionDuration = 65;

    let accumulatedValue = 0;

    const g = this.svg.append('g');

    g.selectAll('rect')
      .data(sections)
      .enter()
      // create sections of the cigarette
      .append('rect')
        .attr('id', (d, index) => `section_${index}`)
        .attr('y', 0)
        .attr('x', (d) => {
          const x = xScale(accumulatedValue);
          accumulatedValue += d.value;
          return x;
         })
         .attr('cursor', 'pointer')
        .attr('height', svgHeight - 50)
        .attr('transform', 'translate(0, 10)')
        .attr('fill', (d) => d.fill)
        // chart loading animation
        .transition()
        .duration(transitionDuration)
        .delay((d, i) => transitionDuration * i)
        .ease(d3.easeLinear)
        .attr('width', (d) => xScale(d.value))
        .on('end', function(r) {
          d3.select(this).on('mouseenter', function(d) {
            // highlight current element
            d3.select(this)
              .transition()
              .duration(125)
              .attr('height', svgHeight - 40)
              .attr('transform', 'translate(0, 0)')
              .style('opacity', '.7');
            const coords = tooltipCoords(this);
            tooltip
              .attr('class', () =>
                coords.left !== null ? tooltipClass + ` ${tooltipClass}--left` : tooltipClass + ` ${tooltipClass}--right`
              )
              .style('transform', 'scale(1) translate(0%,0%)')
              .style('opacity', '1')
              .style('left', () => coords.left ? coords.left + 'px' : 'initial')
              .style('right', () => coords.right ? coords.right + 'px' : 'initial')
              .style('top', (coords.top - 25) + 'px')
              .html(`<strong>${r.caption}:</strong> ${r.label}`);
          })
          .on('mouseleave', function(d) {
            // restore current section
            d3.select(this)
              .transition()
              .duration(125)
              .attr('height', svgHeight - 50)
              .attr('transform', 'translate(0, 10)')
              .style('opacity', '1');
            tooltip
              .style('transform', 'translate(0,33%)')
              .style('opacity', '0')
              .attr('class', tooltipClass);
          });
        });

    // Add Axis
    const xAxis = d3.axisBottom(xScale).scale(xScale).ticks(20);
    const yAxisTranslate = svgHeight - 35;
    g.append('g')
      .attr('class', 'charts-cigarette-bar__axis')
      .attr('transform', 'translate(0, ' + yAxisTranslate + ')')
      .call(xAxis)
      .selectAll('.tick')
      .select('text')
      .attr('fill', '#808080');
    g.select('.charts-cigarette-bar__axis')
      .select('.domain')
      .attr('stroke', '#808080');

    // Add the label under the Axis
    g.append('text')
      .attr('y', (svgHeight - 5))
      .attr('x', (svgWidth / 2))
      // .attr('transform', `translate(${(svgWidth / 2)}, ${(svgHeight - 5)})`)
      .style('text-anchor', 'middle')
      .style('font-size', '.9em')
      .style('fill', '#808080')
      .text(this.xAxisLabel);
  }

  editChart = () => {
    /* TO DO. */
  }

  triggerMouseEvent = (id: string, mouseEvent: string): void => {
    if (this.svg) {
      const target = this.svg.select('#section_' + id);
      target.dispatch(mouseEvent);
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    // remove previous chart if exists
    this.svg.selectAll('g').remove();
    if (this.resizeTimeout) {
      window.clearTimeout(this.resizeTimeout);
    }
    this.resizeTimeout = window.setTimeout(this.createChart, 500);
  }

}
