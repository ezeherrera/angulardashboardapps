import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ChartsLegendComponent } from './components/legend.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    ChartsLegendComponent
  ],
  exports: [
    ChartsLegendComponent
  ]
})
export class ChartsLegendModule { }
