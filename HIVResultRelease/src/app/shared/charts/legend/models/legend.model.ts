export class ChartsLegendModel {
  header?: string;
  captions: ChartsLegendCaptionModel[];
}

export class ChartsLegendCaptionModel {
  constructor(
    public caption: string, // Caption title
    public color: string,   // HEX color code
    public id: any, // Caption title
  ) {}
}
