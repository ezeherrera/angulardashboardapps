import { ChartsLegendModule } from './legend.module';

describe('ChartsLegendModule', () => {
  let chartsLegendModule: ChartsLegendModule;

  beforeEach(() => {
    chartsLegendModule = new ChartsLegendModule();
  });

  it('should create an instance', () => {
    expect(chartsLegendModule).toBeTruthy();
  });
});
