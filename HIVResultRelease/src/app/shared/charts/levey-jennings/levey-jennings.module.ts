import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ChartsLegendModule } from '../legend/legend.module';
import { ChartsLeveyJenningsGraphComponent } from './components/levey-jennings-graph.component';
import { ChartsLeveyJenningsComponent } from './components/levey-jennings.component';

@NgModule({
  imports: [
    CommonModule,
    ChartsLegendModule,
  ],
  declarations: [
    ChartsLeveyJenningsComponent,
    ChartsLeveyJenningsGraphComponent,
  ],
  exports: [
    ChartsLeveyJenningsComponent,
  ],
})

export class ChartsLeveyJenningsModule {
}
