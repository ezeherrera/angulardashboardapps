import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MessageDialogComponent } from './components/message-dialog.component';
import { MatDialogModule, MatButtonModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    MatDialogModule,
    MatButtonModule,
  ],
  entryComponents: [MessageDialogComponent],
  declarations: [
    MessageDialogComponent,
  ],
  exports: [
    MessageDialogComponent,
  ]
})
export class MessageDialogModule { }
