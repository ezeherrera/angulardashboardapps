import { MessageDialogModule } from './message-dialog.module';

describe('MessageDialogModule', () => {
  let messageDialogModule: MessageDialogModule;

  beforeEach(() => {
    messageDialogModule = new MessageDialogModule();
  });

  it('should create an instance', () => {
    expect(messageDialogModule).toBeTruthy();
  });
});
