import { Component, Input, OnInit } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';

@Component({
  selector: 'app-svg-icon',
  templateUrl: './svg-icon.component.html',
  styleUrls: ['./svg-icon.component.scss']
})
export class SvgIconComponent implements OnInit {

  @Input() svgIcon: string;

  constructor(
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
  ) {
    // import Roche Logo
    iconRegistry.addSvgIcon( 'ROCHE', sanitizer.bypassSecurityTrustResourceUrl('assets/images/logo/roche.svg'));
  }

  ngOnInit() {}
}
