import { Component, OnInit } from '@angular/core';
// import { AuthenticationService } from '../authentication/authentication.service';
import { Observable } from 'rxjs';
import { SessionService } from '../session/session.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;
  sessionUser: object;
  roles: object = {
    resultrelease: false,
    administrator: false,
    analytics: false,
  };
  name: boolean;

  constructor(
    // private authService: AuthenticationService,
    private session: SessionService
  ) { }

  ngOnInit() {
    this.isLoggedIn$ = this.session.isLoggedIn;
    this.session.sessionUser.subscribe(
      (user) => {
        if (user) {
        this.name = user['sub'];
        user['role'].forEach(
          item => { this.roles[item] = true; }
        );
      }}
    );
  }

  logOut() {
    this.session.logOut();
  }
}
