import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SessionService } from '../session/session.service';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  isLoggedIn$: Observable<boolean>;
  isOpen = true;

  constructor(
    private sessionService: SessionService,
  ) { }

  ngOnInit() {
    this.isLoggedIn$ = this.sessionService.isLoggedIn;
    this.isOpen = true;
  }
}
