export class NavigationItem {
  path: string;
  title: string;
  icon?: string;
  children?: NavigationItem[];
}
