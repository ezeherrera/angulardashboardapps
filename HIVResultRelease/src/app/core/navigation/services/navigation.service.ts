import { Injectable } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute, Route } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { NavigationItem } from './navigation.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})


export class NavigationService {

  activeNavigationMenu$: Observable<NavigationItem[]>;
  activeNavigationItem$: Observable<NavigationItem>;

  constructor(
    private router: Router,
    private titleService: Title,
  ) {
    this.activeNavigationItem$ = this.router.events
    .pipe(
      filter(e => e instanceof NavigationEnd),
      map(_ => this.router.routerState.root),
      map(route => {
        const active = this.lastRouteWithNavigationItem(route.root);
          if (active && active.title) {
            this.titleService.setTitle(`${active.title} - ${environment.title}`);
          } else {
            this.titleService.setTitle(environment.title);
          }
          return active;
      })
    );
    this.activeNavigationItem$.subscribe();

    this.activeNavigationMenu$ = this.router.events
    .pipe(
      filter(e => e instanceof NavigationEnd),
      map(_ => this.router.routerState.root),
      map(route => {
        if (route && route.firstChild) {
          const cfg = route.firstChild.routeConfig;
          return this.getNavigationItems([cfg]);
        }
        return [];
      })
    );
  }

  public getNavigationItems(routerConfig?: Route[]): NavigationItem[] {
    const urlTree = routerConfig || this.router.config;
    const menu = this.generateNavigationTree(urlTree);
    return menu;
  }

  private lastRouteWithNavigationItem(route: ActivatedRoute): NavigationItem {
    let lastNavigation;
    do { lastNavigation = this.extractNavigation(route) || lastNavigation; }
    while (route = route.firstChild);
    return lastNavigation;
  }

  private extractNavigation(route: ActivatedRoute): NavigationItem {
      const cfg = route.routeConfig;
      return cfg && cfg.data && cfg.data.title
          ? { path: cfg.path, title: cfg.data.title, icon: cfg.data.icon }
          : undefined;
  }

  private generateNavigationTree(urlTree: Route[]): NavigationItem[] {
    return urlTree
      .filter(route => route.data && route.data.title)
      .map(route => {
        let children = null;
        if (route.children && route.children.length) {
          children = this.generateNavigationTree(route.children);
        }
        return {
            path: route.path,
            title: route.data.title,
            icon: route.data.icon,
            children: children,
        };
      });
  }
}

