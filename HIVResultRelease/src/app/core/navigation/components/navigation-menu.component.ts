import { Component, OnInit, Input } from '@angular/core';
import { NavigationItem } from '../services/navigation.model';

@Component({
  selector: 'app-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent implements OnInit {

  @Input() navItem: NavigationItem;
  @Input() previousPath?: string;

  constructor() { }

  ngOnInit() {
    this.previousPath = this.previousPath ? this.previousPath : this.navItem.path;
  }

}
