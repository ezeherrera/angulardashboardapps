import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { NavigationService } from '../services/navigation.service';
import { NavigationItem } from '../services/navigation.model';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  public mainNavigationItems: Observable<NavigationItem[]>;

  constructor(
    private navigationService: NavigationService,
  ) {
    this.mainNavigationItems = this.navigationService.activeNavigationMenu$;
  }

  ngOnInit() {
  }

}
