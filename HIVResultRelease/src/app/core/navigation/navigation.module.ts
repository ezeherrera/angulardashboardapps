import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationComponent } from './components/navigation.component';
import { RouterModule } from '@angular/router';
import { NavigationService } from './services/navigation.service';
import { NavigationMenuComponent } from './components/navigation-menu.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [
    NavigationComponent,
    NavigationMenuComponent,
  ],
  exports: [
    NavigationComponent,
  ],
  providers: [
    NavigationService,
  ]
})
export class NavigationModule { }
