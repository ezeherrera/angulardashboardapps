import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { SessionStorageService } from '../session-storage/session-storage.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  isLoggedIn: Observable<boolean>;
  loggedIn: boolean;
  sessionUser: Observable<object>;

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private sessionStorage: SessionStorageService,
  ) {
    this.authService.authEvents.subscribe( event => this.handleAuthEvents(event) );
    this.sessionUser = this.authService.userData.pipe(
      map(user => {
        if (user) {
          user['role'] = Array.isArray(user['role']) ? user['role'] : user['role'].replace(/ /g, '').split(','); } return user;
        }
      )
    );
    this.isLoggedIn = this.authService.isLoggedIn;
    this.isLoggedIn.subscribe(
      logged => {
        if (this.loggedIn && !logged) {
          this.router.navigate(['/logout']);
        } else if (this.loggedIn && logged) {
          this.loggedIn = true;
        } else {
          this.loggedIn = false;
        }
      }
    );
  }

  logIn(): void {
    this.authService.logIn();
  }

  logOut(): void {
    this.authService.logOut();
    this.sessionStorage.clearData();
    this.router.navigate(['/login']);
  }

  sessionExpires(lastUrl): void {
    this.authService.logOut();
    this.sessionStorage.setData('return-url', lastUrl);
    this.router.navigate(['/login']);
  }

  handleAuthEvents(event): void {
    switch (event.type) {
      case 'token_expires':
        const state = this.router.routerState.snapshot;
        this.sessionExpires(state.url);
      break;

      case 'token_received':
        const returnUrl = this.sessionStorage.getData('return-url');
        const url =  returnUrl || this.authService.getHomeRoute();
        this.router.navigate([url]);
        this.sessionStorage.removeData('return-url');
      break;
    }
  }
}
