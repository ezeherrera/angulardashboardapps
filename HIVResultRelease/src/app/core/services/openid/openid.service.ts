import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OpenidService {
  constructor(
      private http: HttpClient,
    ) { }

  getOpenidWellKnownConfiguration() {
    const self = this;
    const promise = new Promise(function(resolve, reject) {
      self.http.get(environment.issuer + '/.well-known/openid-configuration').subscribe((openidResponse) => {
        resolve(openidResponse);
      }, error => reject(error));
    });

    return promise;
  }
}
