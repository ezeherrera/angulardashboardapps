import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { SessionService } from '../session/session.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(
    private router: Router,
    private authService: AuthenticationService,
    private session: SessionService,
  ) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {
      const area = state.url.split('/')[1];
      if (this.authService.hasValidIdToken()) {
        if (this.authService.hasUserAccess(area)) {
          return true;
        } else {
          const home = this.authService.getHomeRoute();
          this.router.navigate([home]);
        }
      } else {
        this.session.sessionExpires(state.url);
      }
      return false;
  }
}
