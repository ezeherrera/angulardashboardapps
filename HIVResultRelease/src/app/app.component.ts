import { Component } from '@angular/core';
import { AuthenticationService } from './core/authentication/authentication.service';
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(
    private authService: AuthenticationService,
    private titleService: Title,
  ) {
    this.authService.configureWithAuthConfig();
    this.titleService.setTitle(environment.title);
  }

}
