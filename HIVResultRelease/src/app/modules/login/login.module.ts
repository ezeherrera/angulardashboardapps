import { NgModule } from '@angular/core';
import { LoginComponent } from './/pages/login/login.component';
import { LoginRoutingModule } from './login-routing.module';
import { MaterialModule } from '../../shared/material/material.module';
import { CommonModule } from '@angular/common';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { SvgIconModule } from '../../shared/svg-icon/svg-icon.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    MaterialModule,
    SvgIconModule,
  ],
  declarations: [LoginComponent],
  providers: [AuthenticationService]
})
export class LoginModule { }
