import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../../../core/authentication/authentication.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loading: boolean;
  logo: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
  ) {
    this.loading = true;
    this.logo = 'assets/images/logo/roche.svg';
  }

  ngOnInit() {
    let callback = false;
    if (this.authService.hasValidIdToken()) {
      const home = this.authService.getHomeRoute();
      this.router.navigate([home]);
    } else {
      this.route.data.subscribe( data => (callback = data.callback) );
      if (!callback) {
        this.loading = false;
      }
    }
  }

  logIn() {
    this.authService.logIn();
  }

  isLoggedIn() { }
}
