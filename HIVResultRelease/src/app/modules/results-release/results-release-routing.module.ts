import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './pages/list/list.component';
import { ResultsReleaseComponent } from './results-release.component';
import { AuthGuard } from '../../core/guards/auth.guard';
import { UnreleaseBarcodeComponent } from './pages/unrelease-barcode/unrelease-barcode.component';

const routes: Routes = [
  { path: 'resultrelease',
    canActivate: [AuthGuard],
    component: ResultsReleaseComponent,
    data: { title: 'Result Release' },
    children: [
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'list',
    },
    { path: 'list',
      canActivate: [AuthGuard],
      component: ListComponent,
      data: { title: 'Unreleased Results' }
    },
    { path: 'search',
      canActivate: [AuthGuard],
      component: UnreleaseBarcodeComponent,
      data: { title: 'Search Barcode' }
    },
  ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class ResultsReleaseRoutingModule { }
