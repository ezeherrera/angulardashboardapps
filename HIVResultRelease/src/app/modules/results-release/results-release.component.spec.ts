import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsReleaseComponent } from './results-release.component';

describe('ResultsReleaseComponent', () => {
  let component: ResultsReleaseComponent;
  let fixture: ComponentFixture<ResultsReleaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsReleaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsReleaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
