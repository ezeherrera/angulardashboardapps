import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from './../../../../environments/environment';
import { SessionStorageService } from '../../../core/session-storage/session-storage.service';
import { MatSnackBar } from '@angular/material';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }
)};

@Injectable()
export class ResultsReleaseService {

  public page: BehaviorSubject<number> = new BehaviorSubject(0);
  public pagesize = 30;
  public filter: BehaviorSubject<string> = new BehaviorSubject(null);
  public listSubject: BehaviorSubject<any[]> = new BehaviorSubject(null);
  private list: any[] = [];

  constructor(
    private http: HttpClient,
    private snackBar: MatSnackBar,
    private store: SessionStorageService,
  ) {
    const storedFilter = this.store.getData('filter') || null;
    const storedPage = this.store.getData('page') || 1;
    if (storedFilter) {
      this.setFilter(storedFilter, storedPage);
    } else {
      this.setPage(storedPage);
    }
  }

  setPage(sum: number) {
    const newPage = this.page.getValue() + sum;
    this.page.next(newPage);
    this.store.setData('page', newPage);
    this.getResults();
  }

  setFilter(str: string, page?: number): void {
    this.store.setData('filter', str);
    this.filter.next(str);
    this.page.next(page || 1);
    this.setPage(0);
  }

  // GET: /results?page=[page]&pagesize=[pagesize]
  getResults(page?: number, pagesize?: number): void {
    page = page || this.page.getValue();
    pagesize = pagesize || this.pagesize;
    let url = `${environment.apiResultsServiceUrl}/playground/results/?page=${page}&pagesize=${pagesize}`;
    const filter = this.filter.getValue() || '';
    if (filter) {
      url = `${environment.apiResultsServiceUrl}/playground/search/${filter}?page=${page}&pagesize=${pagesize}`;
    }
    this.http.get(url).subscribe((response: any[]) => {
      this.list = response;
      this.listSubject.next(this.list);
    },
    (error): void => {
      if (error.status === 401) {
        this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
          duration: 3000
        });
      }
      this.listSubject.next([]);
    });
  }

  // GET: /results?page=[page]&pagesize=[pagesize]
  search(barcode): Observable<any> {
    const url = `${environment.apiResultsServiceUrl}/playground/results/${barcode}`;
    return this.http.get(url);
  }

  // POST: /results/release
  release(barcodes: string[]): Observable<any> {
    const url = environment.apiResultsServiceUrl + '/playground/results/release/';
    const params = barcodes;
    return this.http.post(url, params, httpOptions);
  }

  // POST: /results/unrelease
  unrelease(barcode: string): Observable<any> {
    const url = environment.apiResultsServiceUrl + '/playground/results/unrelease/' + barcode;
    const params = {};
    return this.http.post(url, params, httpOptions);
  }

  // POST: /results/changebarcode
  edit(index: number, oldBarcode: string, newBarcode: string): Observable<any> {
    const url = environment.apiResultsServiceUrl + '/playground/results/changebarcode';
    const params = {
      'oldBarcode': oldBarcode,
      'newBarcode': newBarcode
    };
    return this.http.post(url, params, httpOptions);
  }
}
