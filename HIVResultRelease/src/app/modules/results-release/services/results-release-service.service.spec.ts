import { inject, TestBed } from '@angular/core/testing';

import { ResultsReleaseService } from './results-release-service.service';

describe('ResultReleaseService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ResultsReleaseService]
    });
  });

  it('should be created', inject([ResultsReleaseService], (service: ResultsReleaseService) => {
    expect(service).toBeTruthy();
  }));
});
