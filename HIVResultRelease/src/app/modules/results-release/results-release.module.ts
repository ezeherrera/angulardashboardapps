import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule,
        MatCheckboxModule,
        MatExpansionModule,
        MatInputModule,
        MatProgressBarModule,
        MatTabsModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
      } from '@angular/material';
import { ResultsReleaseRoutingModule } from './/results-release-routing.module';
import { ResultsReleaseService } from './services/results-release-service.service';
import { ResultsReleaseComponent } from './results-release.component';
import { DetailComponent } from './pages/detail/detail.component';
import { ListComponent } from './pages/list/list.component';
import { UnreleaseBarcodeComponent } from './pages/unrelease-barcode/unrelease-barcode.component';

@NgModule({
  imports: [
    CommonModule,
    ResultsReleaseRoutingModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatCheckboxModule,
    MatExpansionModule,
    MatIconModule,
    MatInputModule,
    MatProgressBarModule,
    MatTabsModule,
    MatToolbarModule,
    FormsModule,
  ],
  declarations: [
    ListComponent,
    DetailComponent,
    ResultsReleaseComponent,
    UnreleaseBarcodeComponent,
  ],
  providers: [
    ResultsReleaseService,
  ]
})
export class ResultsReleaseModule { }
