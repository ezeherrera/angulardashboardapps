import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ResultsReleaseService } from '../../services/results-release-service.service';
import { MatDialog, MatSnackBar } from '@angular/material';
import { MessageDialogComponent } from '../../../../shared/message-dialog/components/message-dialog.component';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-unrelease-barcode',
  templateUrl: './unrelease-barcode.component.html',
  styleUrls: ['./unrelease-barcode.component.scss']
})
export class UnreleaseBarcodeComponent implements OnInit {

  public loading = false;
  public notFound = false;
  public barcode: string;
  public item: Object;

  constructor(
    private service: ResultsReleaseService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
  ) { }

  ngOnInit() {
  }

  onSubmit(f: NgForm) {
    const { barcode } = f.value;
    this.unrelease(barcode);
  }

  search(f: NgForm): void {
    const { barcode } = f.value;
    if (!barcode || !barcode.replace(/\s/g, '')) { return null; }
    this.loading = true;
    this.item = null;
    this.service.search(barcode)
      .subscribe(response => {
        this.item = null;
        if (response[0]) {
          this.notFound = false;
          this.item = response[0];
        } else {
          this.item = null;
          this.notFound = true;
        }
        this.loading = false;
      },
      (error): void => {
        this.loading = false;
        this.item = null;
        this.notFound = true;
        if (error.status === 401) {
          this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
            duration: 3000
          });
        }
        const message = `<strong>${barcode}</strong>: Server returned an error`;
        this.openDialog({ message, title: 'Searching barcode', error, type: 'error' });
      });
  }

  unrelease(f: NgForm): void {
    this.loading = true;
    const { barcode } = f.value;
    const observer$: Observable<any> = this.service.unrelease(barcode);
    observer$.subscribe(response => {
      const message = `<strong>${barcode}</strong>: ${response}`;
      this.openDialog({ title: 'Unreleasing barcode', message});
      this.loading = false;
    },
    (error): void => {
      if (error.status === 401) {
        this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
          duration: 3000
        });
      }
      if (error.status === 404) {
        const message = `<strong>${barcode}</strong>: Barcode not found.`;
        this.openDialog({ message, title: 'Unreleasing barcode' });
      } else {
        const message = `<strong>${barcode}</strong>: Server returned an error`;
        this.openDialog({ message, title: 'Unreleasing barcode', error, type: 'error' });
      }
      this.service.getResults();
      this.loading = false;
    });
  }

  release(f: NgForm) {
    const { barcode } = f.value;
    this.loading = true;
    const observer$: Observable<any> = this.service.release([barcode]);
    observer$.subscribe(response => {
      const message = this.getFormattedMessage(response);
      this.openDialog({ title: 'Releasing barcode', message});
      this.loading = false;
    },
    (error): void => {
      if (error.status === 401) {
        this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
          duration: 3000
        });
      }
      if (error.status === 404) {
        const message = `<strong>${barcode}</strong>: Barcode not found.`;
        this.openDialog({ message, title: 'Releasing barcode' });
      } else {
        const message = `<strong>${barcode}</strong>: Server returned an error`;
        this.openDialog({ message, title: 'Releasing barcode', error, type: 'error' });
      }
      this.loading = false;
    });
  }

  handleSubmitEvent(data: any): void {
    const { item, values, index } = data;
    const oldBarcode = item.barcode;
    const newBarcode = values.barcode;
    if (newBarcode !== oldBarcode) {
      this.editBarcode(index, oldBarcode, newBarcode, item);
    }
  }

  editBarcode(index, oldBarcode, newBarcode, item) {
    item.loading = true;
    const observer$: Observable<any> = this.service.edit(index, oldBarcode, newBarcode);
    observer$.subscribe((): void => {
      item.barcode = newBarcode;
      item.loading = false;
    },
    (error): void => {
      this.openDialog({ message: error.error, title: 'Edit Barcode', type: 'error' });
      item.loading = false;
    });
  }

  openDialog(data): void {
    this.dialog.open(MessageDialogComponent, {
      width: 'auto',
      data
    });
  }

  getFormattedMessage(input: {barcode: string, alreadyReleased: boolean}[]): string {
    let output = '';
    output = input.reduce( (prev, item) => {
      return prev += item.alreadyReleased ?
                    `<strong>${item.barcode}</strong>: Already released <br/>` :
                    `<strong>${item.barcode}</strong>: Release Ok <br/>`;
    }, output);
    return output;
  }

}
