import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnreleaseBarcodeComponent } from './unrelease-barcode.component';

describe('UnreleaseBarcodeComponent', () => {
  let component: UnreleaseBarcodeComponent;
  let fixture: ComponentFixture<UnreleaseBarcodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnreleaseBarcodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnreleaseBarcodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
