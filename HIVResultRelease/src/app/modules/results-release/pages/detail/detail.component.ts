import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {

  @Output() submitEvent = new EventEmitter();
  @Input() item: Object;
  @Input() index: number;
  @Input() loading?: number;
  public itemProps: { key: string | number, value: string | number }[];
  public barcode: string;

  constructor() { }

  ngOnInit() {
    const itemClone = JSON.parse(JSON.stringify(this.item));
    this.itemProps = this.propsToArray(itemClone);
    this.barcode = itemClone.barcode;
  }

  propsToArray(obj: Object): any[] {
    return Object.keys(obj).map( (key) => ({ key, value: obj[key] }) );
  }

  onSubmit(f: NgForm): void {
    if (this.loading) {
      return null;
    }
    const data = {
      item: this.item,
      values: f.value,
      index: this.index,
    };
    this.submitEvent.emit(data);
  }

}
