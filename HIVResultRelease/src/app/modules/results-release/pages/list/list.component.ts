import { Component, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Observable } from 'rxjs';
import { ResultsReleaseService } from '../../services/results-release-service.service';
import { MessageDialogComponent } from '../../../../shared/message-dialog/components/message-dialog.component';

@Component({
  selector: 'app-results-release-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  public barcode: string = null;
  public filter: string = null;
  public list$: Observable<any[]> = new Observable<any[]>();
  public list: any[] = [];
  public loading = true;
  public page: number;
  public pagesize: number;
  public selection = 0;
  public selectAll = false;
  public selectAllIndeterminate = false;
  public delay = null;
  public noData = false;

  constructor(
    private service: ResultsReleaseService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
  ) {
    this.service.page.subscribe(page => this.page = page);
    this.pagesize = this.service.pagesize;
    this.service.filter.subscribe(filter => this.filter = filter);
  }

  ngOnInit() {
    this.loading = true;
    this.list$ = this.service.listSubject;
    this.barcode = this.filter;
    this.list$
      .subscribe((data) => {
        this.loading = true;
        this.list = data;
        this.selection = 0;
        this.checkSelection();
        if (this.list) {
          this.loading = false;
          if (!this.list.length) {
            this.noData = true;
          } else {
            this.noData = false;
          }
        }
      },
      (error): void => {
        if (error.status === 401) {
          this.loading = false;
          this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
            duration: 3000
          });
        }
        this.noData = true;
      });
  }

  getResults() {
    this.loading = true;
    this.service.getResults();
  }

  filterList(str): void {
    str = str || null;
    clearTimeout(this.delay);
    this.delay = setTimeout(
      () => {
        this.loading = true;
        this.service.setFilter(str);
      },
      250);
  }

  clearFilter() {
    this.loading = true;
    this.service.setFilter(null);
    this.barcode = null;
    this.filter = null;
    window.scrollTo(0, 0);
  }

  nextPage() {
    this.loading = true;
    this.service.setPage(+1);
    window.scrollTo(0, 0);
  }

  previousPage() {
    this.loading = true;
    this.service.setPage(-1);
    window.scrollTo(0, 0);
  }

  addSelection(item) {
    if (!item.selected) {
      item.selected = true;
      this.selection++;
    }
  }

  removeSelection(item) {
    if (item.selected) {
      item.selected = false;
      this.selection--;
    }
  }

  toggleSelection(item, i) {
    if (!item.selected) {
      this.addSelection(item);
    } else {
      this.removeSelection(item);
    }
    this.checkSelection();
  }

  toggleSelectAll(newValue) {
    const removeAll = (newValue === false);
    this.list
      .filter(
        item => {
          if (!this.barcode) { return item; }
          return (item.barcode.toUpperCase().indexOf(this.barcode.toUpperCase()) > 0);
        }
      )
      .forEach((item) => {
        if (removeAll) {
          this.removeSelection(item);
        } else {
          this.addSelection(item);
        }
      });
    this.checkSelection();
  }

  checkSelection() {
    if (this.selection && this.selection < this.list.length) {
      this.selectAllIndeterminate = true;
      this.selectAll = false;
    } else if (this.selection && this.selection === this.list.length) {
      this.selectAll = true;
      this.selectAllIndeterminate = false;
    } else {
      this.selectAllIndeterminate = false;
      this.selectAll = false;
    }
  }

  release(items: any[]): void {
    let message = '';
    const barcodes = [];
    this.loading = true;
    items.forEach(
      (item) => {
        barcodes.push(item.barcode);
      });
    const observer$: Observable<any> = this.service.release(barcodes);
    observer$.subscribe(results => {
      message  = this.getFormattedMessage(results);
      this.openDialog({ title: `Release ${items.length} items`, message});
      this.getResults();
    });
  }

  releaseSelection() {
    const items = this.list.reduce( (array, item) => {
      if (item.selected) {
        array.push(item);
      }
      return array;
    }, []);

    this.release(items);
  }

  handleSubmitEvent(data: any): void {
    const { item, values, index } = data;
    const oldBarcode = item.barcode;
    const newBarcode = values.barcode;
    if (newBarcode !== oldBarcode) {
      this.editBarcode(index, oldBarcode, newBarcode, item);
    }
  }

  editBarcode(index, oldBarcode, newBarcode, item) {
    item.loading = true;
    const observer$: Observable<any> = this.service.edit(index, oldBarcode, newBarcode);
    observer$.subscribe((): void => {
      item.barcode = newBarcode;
      item.loading = false;
    },
    (error): void => {
      this.openDialog({ message: error.error, title: 'Edit Barcode', type: 'error' });
      item.loading = false;
    });
  }

  openDialog(data): void {
    this.dialog.open(MessageDialogComponent, {
      width: '400px',
      data
    });
  }

  getFormattedMessage(input: {barcode: string, alreadyReleased: boolean}[]): string {
    let output = '';
    output = input.reduce( (prev, item) => {
      return prev += item.alreadyReleased ?
                    `<strong>${item.barcode}</strong>: Already released <br/>` :
                    `<strong>${item.barcode}</strong>: Release Ok <br/>`;
    }, output);
    return output;
  }
}
