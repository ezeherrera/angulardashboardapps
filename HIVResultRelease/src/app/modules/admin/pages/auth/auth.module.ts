import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ClientModule } from './clients/client.module';
import { AuthComponent } from './auth.component';
import { ScopeModule } from './scopes/scope.module';
import { SharedScopeModule } from '../../shared/scope/scope.module';
import { SharedClientModule } from '../../shared/client/client.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    SharedScopeModule,
    SharedClientModule,
    ClientModule,
    ScopeModule,
    RouterModule,
  ],
  declarations: [
    AuthComponent
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class AuthModule { }
