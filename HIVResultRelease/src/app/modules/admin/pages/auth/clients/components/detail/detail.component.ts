import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-client-detail',
  templateUrl: './detail.component.html',
  styleUrls: []
})
export class DetailComponent implements OnInit {
  navLinks;
  constructor(
  private route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    const self = this;
    self.navLinks = [];
    self.route.params.subscribe(params => {
      const clientId = params['id'];
      self.navLinks.push({
        path: '/administrator/auth/clients/' + clientId + '/general',
        label: 'General settings'
      });
      self.navLinks.push({
        path: '/administrator/auth/clients/' + clientId + '/scopes',
        label: 'Scopes'
      });
    });
  }
}
