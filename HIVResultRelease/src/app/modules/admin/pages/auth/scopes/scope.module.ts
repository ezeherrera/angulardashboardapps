import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ScopeComponent } from './scope.component';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { SharedScopeModule } from '../../../shared/scope/scope.module';
import { MaterialModule } from '../../../../../shared/material/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    SharedScopeModule,
    RouterModule
  ],
  declarations: [
    ScopeComponent,
    ListComponent,
    DetailComponent
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class ScopeModule { }
