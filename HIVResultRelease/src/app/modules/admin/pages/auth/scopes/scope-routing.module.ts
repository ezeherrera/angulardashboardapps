import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ScopeComponent } from './scope.component';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';

const routes: Routes = [
  { path: 'auth/scopes', component: ScopeComponent, canActivate: [AuthGuard], children: [
    { path: '', component: ListComponent },
    { path: ':id', component: DetailComponent }
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class ScopeRoutingModule { }
