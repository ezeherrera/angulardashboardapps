import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { ClientModule } from './clients/client.module';
import { UsersModule } from './users/users.module';
import { ScopeModule } from './scopes/scope.module';
import { ClaimModule } from './claims/claim.module';

@NgModule({
  imports: [
    CommonModule,
    ClientModule,
    UsersModule,
    ScopeModule,
    ClaimModule,
  ],
  declarations: [
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class OpenidModule { }
