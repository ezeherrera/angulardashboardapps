import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';

export interface User {
  subject: string;
  email: string;
  name: string;
  update_datetime: string;
}

@Component({
  selector: 'app-users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  displayedColumns: string[];
  isLoading: boolean;
  page: number;
  pageSize: number;
  pageIndex: number;
  length: number;
  dataSource;
  constructor(
    private service: UsersService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.displayedColumns = [ 'isChecked', 'login', 'email', 'name', 'update_datetime', 'link' ];
    this.page = 0;
    this.length = 0;
    this.pageSize = 5;
    this.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    const getClaim = function(claimName, claims, defaultValue) {
      for (const i in claims) {
        if (claims[i]) {
          const claim = claims[i];
          if (claim.key === claimName) {
            return claim.value;
          }
        }
      }

      return defaultValue;
    };

    const startIndex = self.page * self.pageSize;
    const request = { start_index: startIndex, count: self.pageSize, order: { target: 'update_datetime', type: 1 } };
    self.service.searchUsers(request).then(function(su) {
        const users = [];
        if (su['content']) {
          su['content'].forEach(function(user) {
            users.push({
              isChecked: false,
              login: user['login'],
              email: getClaim('email', user['claims'], '-'),
              name: getClaim('name', user['claims'], '-'),
              picture: getClaim('picture', user['claims'], ''),
              update_datetime: user['update_datetime'],
              link: '/administrator/openid/users/' + user['login']
            });
          });
        }

        self.dataSource = new MatTableDataSource(users);
        self.length = su['count'];
        self.isLoading = false;
    }).catch(function(e) {
      self.isLoading = false;
      self.snackBar.open('An error occured while to get the users', 'Undo', {
        duration: 3000
      });
    });
  }

  handleRemoveUsers() {
    const self = this;
    const usersToBeRemoved = self.dataSource.data.filter(function(u) { return u.isChecked ; }).map(function(u) { return u.login; });
    const ops = [];
    usersToBeRemoved.forEach(function(user) {
      ops.push(self.service.removeUser(user));
    });
    self.isLoading = true;
    Promise.all(ops).then(function() {
      self.isLoading = false;
      self.snackBar.open('the selected users have been removed', 'Undo', {
        duration: 3000
      });
      self.refreshData();
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to remove the selected users', 'Undo', {
        duration: 3000
      });
    });
  }

  handleAddUser() {
    const self = this;
    const dialogRef = self.dialog.open(AddUserDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(r => {
      if (!r && !r.isInserted) {
        return;
      }

      self.refreshData();
    });
  }

  onChangePage(e) {
    this.page = e.pageIndex;
    this.refreshData();
  }

  navigate(url) {
    this.router.navigate([url]);
  }
}

@Component({
  selector: 'app-adduserdialog',
  templateUrl: 'adduserdialog.html'
})
export class AddUserDialogComponent {
  login: string;
  password: string;
  isLoading: boolean;
  constructor(
    public dialogRef: MatDialogRef<AddUserDialogComponent>,
    private service: UsersService,
    private snackBar: MatSnackBar) {
    this.isLoading = false;
  }
  handleAddUser() {
    const self = this;
    self.isLoading = true;
    const request = { sub : self.login, password: self.password };
    self.service.addUser(request).then(function() {
      self.isLoading = false;
      self.snackBar.open('User has been added', 'Undo', {
        duration: 3000
      });
      self.dialogRef.close({isInserted : true});
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to add the user', 'Undo', {
        duration: 3000
      });
    });
  }
}
