import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { UsersComponent } from './users.component';
import { UsersService } from './services/users.service';
import { ListComponent, AddUserDialogComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { MaterialModule } from '../../../../../shared/material/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    ListComponent,
    UsersComponent,
    AddUserDialogComponent,
    DetailComponent
  ],
  providers: [
    UsersService
  ],
  entryComponents: [
    AddUserDialogComponent
  ]
})
export class UsersModule { }
