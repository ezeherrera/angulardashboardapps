import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class UsersService {
  constructor(
      private http: HttpClient,
      private authService: OAuthService
    ) { }

  searchUsers(request) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/resource_owners/.search';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
    }

    updateClaims(request) {
      const accessToken = this.authService.getAccessToken();
      const httpOptions = {
        headers: new HttpHeaders(
          { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
        )
      };
      const self = this;
      const promise = new Promise(function (resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/resource_owners/claims';
        self.http.put(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
      });

      return promise;
    }
  addUser(request) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/resource_owners';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }

  removeUser(sub) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/resource_owners/' + sub;
        self.http.delete(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }

  getUser(sub) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/resource_owners/' + sub;
        self.http.get(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }
}
