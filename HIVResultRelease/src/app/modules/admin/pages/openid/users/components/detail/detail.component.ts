import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { OpenidService } from 'src/app/core/services/openid/openid.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-user-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  dataSource;
  isLoading: boolean;
  userId: string;
  login: string;
  displayedColumns;
  constructor(
    private route: ActivatedRoute,
      private service: UsersService,
      private snackBar: MatSnackBar,
      private openidService: OpenidService
    ) { }

    ngOnInit() {
    const self = this;
      self.displayedColumns = [ 'isChecked', 'key', 'value' ];
    self.route.params.subscribe(params => {
    self.userId = params['id'];
    self.refreshData();
    });
    }

    save() {
      const self = this;
      self.isLoading = true;
      const claims = self.dataSource.data.filter(function (f) { return f.isChecked; }).map(function (c) {
        return {
          key: c.key,
          value: c.value
        };
      });
      const request = { login: self.login, claims: claims };
      self.service.updateClaims(request).then(function () {
        self.isLoading = false;
        self.snackBar.open('The user has been updated', 'Undo', {
          duration: 3000
        });
        self.refreshData();
      }).catch(function () {
        self.isLoading = false;
        self.snackBar.open('An error occured while trying to update the user', 'Undo', {
          duration: 3000
        });
      });
    }

    refreshData() {
    const self = this;
    self.isLoading = true;
      Promise.all([ self.openidService.getOpenidWellKnownConfiguration(), self.service.getUser(self.userId) ]).then(function(values) {
        const wellKnownConfiguration = values[0];
        const user = values[1];
        self.isLoading = false;
        self.login = user['login'];
        const dataSource = [];
        const userClaims = user['claims'];
        const claims = wellKnownConfiguration['claims_supported'];
        claims.forEach(function(claim) {
          const ucl = userClaims.filter(c => c.key === claim);
          const record = {
            key: claim,
            value: '',
            isChecked: false
          };
          if (ucl.length > 0) {
            record['value'] = ucl[0].value;
            record['isChecked'] = true;
          }

          dataSource.push(record);
        });
        self.dataSource = new MatTableDataSource(dataSource);
      }).catch(function(e) {
        self.isLoading = false;
        self.snackBar.open('An error occured while trying to get the user', 'Undo', {
          duration: 3000
        });
      });
    }
}
