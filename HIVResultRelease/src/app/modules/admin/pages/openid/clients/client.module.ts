import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { GeneralComponent } from './components/detail/components/general/general.component';
import { ScopesComponent } from './components/detail/components/scopes/scopes.component';
import { SharedClientModule } from '../../../shared/client/client.module';
import { MaterialModule } from '../../../../../shared/material/material.module';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    MaterialModule,
    SharedClientModule,
  ],
  declarations: [
    ListComponent,
    DetailComponent,
    GeneralComponent,
    ScopesComponent
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class ClientModule { }
