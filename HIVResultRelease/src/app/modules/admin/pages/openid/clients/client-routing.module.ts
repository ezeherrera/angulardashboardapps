import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { GeneralComponent } from './components/detail/components/general/general.component';
import { ScopesComponent } from './components/detail/components/scopes/scopes.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';

const routes: Routes = [
  { path: 'openid/clients', canActivate: [AuthGuard], children: [
    { path: '', component: ListComponent },
    { path: ':id', component: DetailComponent, children : [
      { path : '', redirectTo: 'general', pathMatch: 'full' },
      { path : 'general', component: GeneralComponent },
      { path : 'scopes', component: ScopesComponent }
    ] }
  ] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class ClientRoutingModule { }
