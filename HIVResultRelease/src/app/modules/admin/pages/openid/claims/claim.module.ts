import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListComponent, AddClaimDialogComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { ClaimsService } from './services/claim.service';
import { MaterialModule } from 'src/app/shared/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    ListComponent,
    DetailComponent,
    AddClaimDialogComponent
  ],
  providers: [
      ClaimsService
  ],
  entryComponents: [
    AddClaimDialogComponent
  ]
})
export class ClaimModule { }
