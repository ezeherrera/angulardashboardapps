import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class ClaimsService {
  constructor(
      private http: HttpClient,
      private authService: OAuthService
    ) { }

  searchClaims(request) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
      const promise = new Promise(function (resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/claims/.search';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  addClaim(request) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/claims';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }

  removeClaim(key) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/claims/' + key;
        self.http.delete(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }

  getClaim(key) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/claims/' + key;
        self.http.get(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => { reject(error); });
     });

    return promise;
  }
}
