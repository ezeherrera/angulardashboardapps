import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog, MatDialogRef, MatSnackBar } from '@angular/material';
import { ClaimsService } from '../../services/claim.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  displayedColumns: string[];
  isLoading: boolean;
  page: number;
  pageSize: number;
  pageIndex: number;
  length: number;
  dataSource;
  constructor(
    private service: ClaimsService,
    public dialog: MatDialog,
    private snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit() {
    this.displayedColumns = [ 'isChecked', 'code', 'update_datetime', 'link' ];
    this.page = 0;
    this.length = 0;
    this.pageSize = 5;
    this.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    const startIndex = self.page * self.pageSize;
    const request = { start_index: startIndex, count: self.pageSize, order: { target: 'update_datetime', type: 1 } };
    self.service.searchClaims(request).then(function(res) {
        const claims = [];
        if (res['content']) {
          res['content'].forEach(function(cl) {
            claims.push({
              isChecked: false,
              code: cl['key'],
              update_datetime: cl['update_datetime'],
              link: '/administrator/openid/claims/' + cl['key']
            });
          });
        }

        self.dataSource = new MatTableDataSource(claims);
        self.length = res['count'];
        self.isLoading = false;
    }).catch(function(e) {
      self.isLoading = false;
      self.snackBar.open('An error occured while to get the claims', 'Undo', {
        duration: 3000
      });
    });
  }

  handleRemoveClaims() {
    const self = this;
    const claimsToBeRemoved = self.dataSource.data.filter(function(u) { return u.isChecked ; }).map(function(u) { return u.code; });
    const ops = [];
    claimsToBeRemoved.forEach(function(claim) {
      ops.push(self.service.removeClaim(claim));
    });
    self.isLoading = true;
    Promise.all(ops).then(function() {
      self.isLoading = false;
      self.snackBar.open('the selected claims have been removed', 'Undo', {
        duration: 3000
      });
      self.refreshData();
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to remove the claims users', 'Undo', {
        duration: 3000
      });
    });
  }

  handleAddClaim() {
    const self = this;
    const dialogRef = self.dialog.open(AddClaimDialogComponent, {
      width: '250px'
    });
    dialogRef.afterClosed().subscribe(r => {
      if (!r && !r.isInserted) {
        return;
      }

      self.refreshData();
    });
  }

  onChangePage(e) {
    this.page = e.pageIndex;
    this.refreshData();
  }

  navigate(url) {
    this.router.navigate([url]);
  }
}

@Component({
  selector: 'app-addclaimdialog',
  templateUrl: 'addclaimsdialog.html'
})
export class AddClaimDialogComponent {
  claimCode: string;
  isLoading: boolean;
  constructor(
    public dialogRef: MatDialogRef<AddClaimDialogComponent>,
    private service: ClaimsService,
    private snackBar: MatSnackBar) {
    this.isLoading = false;
  }
  handleAddClaim() {
    const self = this;
    self.isLoading = true;
    const request = { key : self.claimCode };
    self.service.addClaim(request).then(function() {
      self.isLoading = false;
      self.snackBar.open('Claim has been added', 'Undo', {
        duration: 3000
      });
      self.dialogRef.close({isInserted : true});
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to add the claim', 'Undo', {
        duration: 3000
      });
    });
  }
}
