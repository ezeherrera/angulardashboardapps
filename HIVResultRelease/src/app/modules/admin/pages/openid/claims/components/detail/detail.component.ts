import { Component, OnInit } from '@angular/core';
import { ClaimsService } from '../../services/claim.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

interface ClaimDetail {
  key: string;
  updateDateTime: string;
}

@Component({
  selector: 'app-claim-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  dataSource;
  isLoading: boolean;
  claimName: string;
  claimDetail: ClaimDetail;
  constructor(
    private route: ActivatedRoute,
      private snackBar: MatSnackBar,
        private claimService: ClaimsService
    ) { }

    ngOnInit() {
      const self = this;
      self.route.params.subscribe(params => {
        self.claimName = params['id'];
        self.refreshData();
      });
    }

    refreshData() {
      const self = this;
      self.isLoading = true;
      self.claimService.getClaim(self.claimName).then(function(r) {
        self.isLoading = false;
        self.claimDetail = {
          key: r['key'],
          updateDateTime: r['update_datetime']
        };
      }).catch(function() {
          self.isLoading = false;
          self.snackBar.open('An error occured while trying to get the claim', 'Undo', {
            duration: 3000
          });
      });
    }
}
