import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ListComponent } from './components/list/list.component';
import { DetailComponent } from './components/detail/detail.component';
import { SharedScopeModule } from '../../../shared/scope/scope.module';
import { MaterialModule } from '../../../../../shared/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedScopeModule,
    MaterialModule,
  ],
  declarations: [
    ListComponent,
    DetailComponent
  ],
  providers: [
  ],
  entryComponents: [
  ]
})
export class ScopeModule { }
