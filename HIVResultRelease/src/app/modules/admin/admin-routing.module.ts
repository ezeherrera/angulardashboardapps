import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../../core/guards/auth.guard';
import { AdminComponent } from './pages/admin/admin.component';
import { ScopesComponent } from './shared/client/detail/components/scopes/scopes.component';

import { ListComponent as AuthClientList } from './pages/auth/clients/components/list/list.component';
import { DetailComponent as AuthClientDetail } from './pages/auth/clients/components/detail/detail.component';
import { GeneralComponent as AuthGeneral } from './pages/auth/clients/components/detail/components/general/general.component';
import { ListComponent as AuthScopeList } from './pages/auth/scopes/components/list/list.component';
import { DetailComponent as AuthScopeDetail } from './pages/auth/scopes/components/detail/detail.component';

import { ListComponent as OpenidUserList } from './pages/openid/users/components/list/list.component';
import { DetailComponent as OpenidUserDetail } from './pages/openid/users/components/detail/detail.component';
import { ListComponent as OpenidScopeList } from './pages/openid/scopes/components/list/list.component';
import { DetailComponent as OpenidScopeDetail } from './pages/openid/scopes/components/detail/detail.component';
import { ListComponent as OpenidClaimList } from './pages/openid/claims/components/list/list.component';
import { DetailComponent as OpenidClaimDetail } from './pages/openid/claims/components/detail/detail.component';
import { ListComponent as OpenidClientList } from './pages/openid/clients/components/list/list.component';
import { DetailComponent as OpenidClientDetail } from './pages/openid/clients/components/detail/detail.component';
import { GeneralComponent as OpenidClientGeneral } from './pages/openid/clients/components/detail/components/general/general.component';
import { ScopesComponent as OpenidClientScopes } from './pages/openid/clients/components/detail/components/scopes/scopes.component';


const routes: Routes = [
  { path: 'administrator',
    component: AdminComponent,
    canActivate: [AuthGuard],
    data: {
      title: 'Admin'
    },
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'openid' },

      /* OPENID */
      { path: 'openid',
        canActivate: [AuthGuard],
        data: {
          title: 'OpenId Servers'
        },
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'users' },
          { path: 'users',
            canActivate: [AuthGuard],
            data: {
              title: 'Users'
            },
            children: [
              { path: '', component: OpenidUserList },
              { path: ':id', component: OpenidUserDetail }
            ],
          },
          { path: 'claims',
            canActivate: [AuthGuard],
            data: {
              title: 'Claims'
            },
            children: [
              { path: '', component: OpenidClaimList },
              { path: ':id', component: OpenidClaimDetail }
            ],
          },
          { path: 'clients',
            canActivate: [AuthGuard],
            data: {
              title: 'OpenId Clients'
            },
            children: [
              { path: '', component: OpenidClientList },
              { path: ':id',
                component: OpenidClientDetail,
                children : [
                  { path : '', redirectTo: 'general', pathMatch: 'full' },
                  { path : 'general', component: OpenidClientGeneral },
                  { path : 'scopes', component: OpenidClientScopes }
                ] }
            ],
          },
          { path: 'scopes',
            canActivate: [AuthGuard],
            data: {
              title: 'OpenId Scopes'
            },
            children: [
              { path: '', component: OpenidScopeList },
              { path: ':id', component: OpenidScopeDetail }
            ],
          },
        ],
      },

      /* AUTHORIZATION */
      { path: 'auth',
        canActivate: [AuthGuard],
        data: {
          title: 'Authorization Servers'
        },
        children: [
          { path: '', pathMatch: 'full', redirectTo: 'clients' },
          { path: 'clients',
            canActivate: [AuthGuard],
            data: {
              title: 'Auth. Clients'
            },
            children: [
              { path: '', component: AuthClientList },
              { path: ':id',
                component: AuthClientDetail,
                children: [
                  { path : '', redirectTo: 'general', pathMatch: 'full' },
                  { path : 'general', component: AuthGeneral },
                  { path : 'scopes', component: ScopesComponent }
                ]
              }
            ]},
          { path: 'scopes',
            canActivate: [AuthGuard],
            data: {
              title: 'Auth. Scopes'
            },
            children: [
              { path: '', component: AuthScopeList },
              { path: ':id', component: AuthScopeDetail }
            ]},
        ],
      },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
