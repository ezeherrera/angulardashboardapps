import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './pages/admin/admin.component';
import { AuthModule } from './pages/auth/auth.module';
import { OpenidModule } from './pages/openid/openid.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    AdminRoutingModule,
    AuthModule,
    OpenidModule,
  ],
  declarations: [
    AdminComponent,
  ],
})
export class AdminModule { }
