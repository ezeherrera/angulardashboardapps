import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource,  } from '@angular/material';
import { ScopeService } from '../services/scope.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-scope-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SharedListComponent implements OnInit {
  isLoading: boolean;
  page: number;
  pageSize: number;
  pageIndex: number;
  length: number;
  dataSource;
  displayedColumns;
  @Input() type;
  constructor(
    private scopeService: ScopeService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) {
  }

  ngOnInit() {
    const self = this;
    self.page = 0;
    self.length = 0;
    self.pageSize = 5;
    this.displayedColumns = [ 'isChecked', 'name', 'type', 'update_datetime', 'link' ];
    self.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    const startIndex = self.page * self.pageSize;
    const request = { start_index: startIndex, count: self.pageSize, order: { target: 'update_datetime', type: 1 } };
    self.scopeService.searchScopes(request, self.type).then(function(resp) {
      const scopes = [];
      if (resp['content']) {
        resp['content'].forEach(function(sc) {
          scopes.push({
            isChecked: false,
            name: sc['name'],
            type: sc['is_openid_scope'] ? 'Openid' : 'API',
            update_datetime: sc['update_datetime'],
            link: '/administrator/' + self.type + '/scopes/' + sc['name']
          });
        });
      }

      self.dataSource = new MatTableDataSource(scopes);
      self.length = resp['count'];
      self.isLoading = false;
    }).catch(function(e) {
      self.isLoading = false;
        self.snackBar.open('an error occured while trying to get the scopes', 'Undo', {
          duration: 3000
        });
    });
  }

  handleRemoveScopes() {
    const self = this;
    const scopesToBeRemoved = self.dataSource.data.filter(function(c) { return c.isChecked ; }).map(function(c) { return c.name; });
    const ops = [];
    scopesToBeRemoved.forEach(function(scope) {
      ops.push(self.scopeService.removeScope(scope, self.type));
    });
    self.isLoading = true;
    Promise.all(ops).then(function() {
      self.isLoading = false;
      self.snackBar.open('the selected scopes have been removed', 'Undo', {
        duration: 3000
      });
      self.refreshData();
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to remove the selected scopes', 'Undo', {
        duration: 3000
      });
    });
  }

  handleAddNewScope() {
    const self = this;
    const dialogRef = self.dialog.open(AddScopeDialogComponent, {
      width: '250px'
    });
    dialogRef.componentInstance.type = self.type;
    dialogRef.afterClosed().subscribe(r => {
      if (!r && !r.isInserted) {
        return;
      }

      self.refreshData();
    });
  }

  onChangePage(e) {
    this.page = e.pageIndex;
    this.refreshData();
  }

  navigate(url) {
    this.router.navigate([url]);
  }
}

@Component({
  selector: 'app-add-scope-dialog',
  templateUrl: 'AddScopeDialog.html'
})
export class AddScopeDialogComponent {
  isLoading: boolean;
  scope: string;
  scopeDescription: string;
  type: string;
  callbacks: string[];
  constructor(
    public dialogRef: MatDialogRef<AddScopeDialogComponent>,
    private snackBar: MatSnackBar,
    private scopeService: ScopeService
    ) {
    this.isLoading = false;
  }
  handleAddScope() {
    const self = this;
    self.isLoading = true;
    const request = { name : self.scope, description: self.scopeDescription };
    self.scopeService.addScope(request, self.type).then(function() {
      self.isLoading = false;
      self.snackBar.open('Scope has been added', 'Undo', {
        duration: 3000
      });
      self.dialogRef.close({isInserted : true});
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to add the scope', 'Undo', {
        duration: 3000
      });
    });
  }
}
