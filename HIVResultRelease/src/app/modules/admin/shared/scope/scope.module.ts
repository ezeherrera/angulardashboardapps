import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedListComponent, AddScopeDialogComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { ScopeService } from './services/scope.service';
import { OpenidService } from 'src/app/core/services/openid/openid.service';
import { MaterialModule } from '../../../../shared/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    SharedListComponent,
    AddScopeDialogComponent,
    DetailComponent
  ],
  providers: [
    ScopeService,
    OpenidService
  ],
  entryComponents: [
    AddScopeDialogComponent
  ],
  exports: [
    SharedListComponent,
    DetailComponent
  ]
})
export class SharedScopeModule { }
