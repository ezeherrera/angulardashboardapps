import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class ScopeService {
  constructor(
      private http: HttpClient,
      private authService: OAuthService
    ) { }

  getUrl(type) {
    if (type === 'openid') {
      return environment.apiOpenidManagerUrl;
    }

    return environment.apiAuthManagerUrl;
  }

  getAllScopes(type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/scopes';
        self.http.get(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  getScope(scope, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/scopes/' + scope;
        self.http.get(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  searchScopes(request, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/scopes/.search';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  addScope(request, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/scopes';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  removeScope(scope, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/scopes/' + scope;
        self.http.delete(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }
}
