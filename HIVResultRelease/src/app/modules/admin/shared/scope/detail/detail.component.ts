import { Component, OnInit, Input } from '@angular/core';
import { ScopeService } from '../services/scope.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

interface ScopeInformation {
  scopeName: string;
  scopeDescription: string;
  scopeType: string;
  claims: string[];
}

@Component({
  selector: 'app-scope-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {
  @Input() type;
  scope: ScopeInformation;
  scopeName: string;
  isLoading: boolean;
  constructor(
    private scopeService: ScopeService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar
  ) {
  }

  ngOnInit() {
    const self = this;
    const params = self.route.params;
    self.scopeName = params['value']['id'];
    self.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    self.scopeService.getScope(self.scopeName, self.type).then(function(s) {
      self.scope = {
        scopeName: s['name'],
        scopeDescription: s['description'],
        scopeType: s['is_openid_scope'] ? 'OPENID' : 'API',
        claims: s['claims']
      };
      self.isLoading = false;
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to get the scope', 'Undo', {
        duration: 3000
      });
    });
  }
}
