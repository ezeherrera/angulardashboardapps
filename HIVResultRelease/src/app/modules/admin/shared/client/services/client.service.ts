import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { OAuthService } from 'angular-oauth2-oidc';

@Injectable()
export class ClientService {
  constructor(
    private http: HttpClient,
    private authService: OAuthService
    ) { }

  getUrl(type) {
    if (type === 'openid') {
      return environment.apiOpenidManagerUrl;
    }

    return environment.apiAuthManagerUrl;
  }

  searchClients(request, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/clients/.search';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  addClient(request, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/clients';
        self.http.post(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  updateClient(request, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/clients';
        self.http.put(url, request, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  deleteClient(clientId, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/clients/' + clientId;
        self.http.delete(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }

  getClient(clientId, type) {
    const accessToken = this.authService.getAccessToken();
    const httpOptions = {
       headers: new HttpHeaders(
         { 'Content-Type': 'application/json', 'Authorization': 'Bearer ' + accessToken }
       )
     };
     const self = this;
     const promise = new Promise(function(resolve, reject) {
        const url = environment.apiOpenidManagerUrl + '/api/clients/' + clientId;
        self.http.get(url, httpOptions).subscribe((resp) => {
          resolve(resp);
        }, error => reject(error));
     });

    return promise;
  }
}
