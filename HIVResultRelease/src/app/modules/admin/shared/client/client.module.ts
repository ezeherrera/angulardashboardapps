import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedListComponent, AddClientDialogComponent } from './list/list.component';
import { GeneralComponent } from './detail/components/general/general.component';
import { ScopesComponent } from './detail/components/scopes/scopes.component';
import { ClientService } from './services/client.service';
import { ScopeService } from './services/scope.service';
import { OpenidService } from 'src/app/core/services/openid/openid.service';
import { MaterialModule } from '../../../../shared/material/material.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
  ],
  declarations: [
    AddClientDialogComponent,
    SharedListComponent,
    GeneralComponent,
    ScopesComponent
  ],
  providers: [
    ClientService,
    ScopeService,
    OpenidService
  ],
  entryComponents: [
    AddClientDialogComponent
  ],
  exports: [
    SharedListComponent,
    GeneralComponent,
    ScopesComponent
  ]
})
export class SharedClientModule { }
