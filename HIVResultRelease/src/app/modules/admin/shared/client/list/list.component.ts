import { Component, OnInit, Input } from '@angular/core';
import { MatDialog, MatDialogRef, MatSnackBar, MatTableDataSource,  } from '@angular/material';
import { ClientService } from '../services/client.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-client-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class SharedListComponent implements OnInit {
  isLoading: boolean;
  page: number;
  pageSize: number;
  pageIndex: number;
  length: number;
  dataSource;
  displayedColumns;
  @Input() type;
  constructor(
    private clientService: ClientService,
    private snackBar: MatSnackBar,
    public dialog: MatDialog,
    private router: Router
  ) {
  }

  ngOnInit() {
    const self = this;
    self.page = 0;
    self.length = 0;
    self.pageSize = 5;
    this.displayedColumns = [ 'isChecked', 'logo_uri', 'client_name', 'application_type', 'update_datetime', 'link' ];
    self.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    const startIndex = self.page * self.pageSize;
    const request = { start_index: startIndex, count: self.pageSize, order: { target: 'update_datetime', type: 1 } };
    self.clientService.searchClients(request, self.type).then(function(resp) {
      const clients = [];
      if (resp['content']) {
        resp['content'].forEach(function(cl) {
          clients.push({
            isChecked: false,
            client_id: cl['client_id'],
            logo_uri: cl['logo_uri'],
            client_name: cl['client_name'],
            application_type: cl['application_type'],
            update_datetime: cl['update_datetime'],
            link: '/administrator/' + self.type + '/clients/' + cl['client_id']
          });
        });
      }

      self.dataSource = new MatTableDataSource(clients);
      self.length = resp['count'];
      self.isLoading = false;
    }).catch(function(e) {
      self.isLoading = false;
        self.snackBar.open('an error occured while trying to get the clients', 'Undo', {
          duration: 3000
        });
    });
  }

  handleRemoveClients() {
    const self = this;
    const clientsToBeRemoved = self.dataSource.data.filter(function(c) { return c.isChecked ; }).map(function(c) { return c.client_id; });
    const ops = [];
    clientsToBeRemoved.forEach(function(client) {
      ops.push(self.clientService.deleteClient(client, self.type));
    });
    self.isLoading = true;
    Promise.all(ops).then(function() {
      self.isLoading = false;
      self.snackBar.open('the selected clients have been removed', 'Undo', {
        duration: 3000
      });
      self.refreshData();
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to remove the selected clients', 'Undo', {
        duration: 3000
      });
    });
  }

  handleAddClient() {
    const self = this;
    const dialogRef = self.dialog.open(AddClientDialogComponent, {
      width: '250px'
    });
    dialogRef.componentInstance.type = self.type;
    dialogRef.afterClosed().subscribe(r => {
      if (!r && !r.isInserted) {
        return;
      }

      self.refreshData();
    });
  }

  onChangePage(e) {
    this.page = e.pageIndex;
    this.refreshData();
  }

  navigate(url) {
    this.router.navigate([url]);
  }
}

@Component({
  selector: 'app-add-client-dialog',
  templateUrl: 'addclientdialog.html'
})
export class AddClientDialogComponent {
  isLoading: boolean;
  newCallback: string;
  type: string;
  callbacks: string[];
  constructor(
    public dialogRef: MatDialogRef<AddClientDialogComponent>,
    private snackBar: MatSnackBar,
    private clientService: ClientService
    ) {
    this.isLoading = false;
    this.callbacks = [];
  }
  handleRemove(callback) {
    const index = this.callbacks.indexOf(callback);
    if (index >= 0) {
      this.callbacks.splice(index, 1);
    }
  }
  handleAddCallback(event) {
    const input = event.input;
    const value = event.value;
    this.callbacks.push(value);
    input.value = '';
  }
  handleAddClient() {
    const self = this;
    self.isLoading = true;
    const request = { redirect_uris : self.callbacks };
    self.clientService.addClient(request, self.type).then(function() {
      self.isLoading = false;
      self.snackBar.open('Client has been added', 'Undo', {
        duration: 3000
      });
      self.dialogRef.close({isInserted : true});
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to add the client', 'Undo', {
        duration: 3000
      });
    });
  }
}
