import { Component, OnInit, Input } from '@angular/core';
import { ClientService } from '../../../services/client.service';
import { OpenidService } from 'src/app/core/services/openid/openid.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar } from '@angular/material';

interface Client {
  clientName: string;
  clientId: string;
  clientSecret: string;
  clientLogo: string;
  applicationType: string;
  redirectUris: string[];
  grantTypes: string[];
  idTokenSignedResponseAlg: string;
  tokenEndpointAuthMethod: string;
}

@Component({
  selector: 'app-client-general-detail',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {
  @Input() type;
  isLoading: boolean;
  clientId: string;
  client: Client;
  clientTypes: string[];
  supportedGrantTypes: string[];
  idTokenSigningAlgValuesSupported: string[];
  tokenEndpointAuthMethodsSupported: string[];
  clientResponse;
  constructor(
    private clientService: ClientService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private openidService: OpenidService
  ) {
  }

  ngOnInit() {
    const self = this;
    const params = self.route.parent.params;
    self.clientId = params['value']['id'];
    self.clientTypes = [ 'web', 'native' ];
    self.refreshData();
  }

  refreshData() {
    const self = this;
    self.isLoading = true;
    Promise.all([
      self.openidService.getOpenidWellKnownConfiguration(),
      self.clientService.getClient(self.clientId, self.type) ]).then(function(values) {
        const openidWellKnownConfiguration = values[0];
        self.clientResponse = values[1];
        self.client = {
          clientName: self.clientResponse['client_name'],
          clientId: self.clientResponse['client_id'],
          clientSecret: self.clientResponse['secrets'][0].value,
          clientLogo: self.clientResponse['client_logo'],
          applicationType: self.clientResponse['application_type'],
          redirectUris: self.clientResponse['redirect_uris'],
          grantTypes: self.clientResponse['grant_types'],
          idTokenSignedResponseAlg : self.clientResponse['id_token_signed_response_alg'],
          tokenEndpointAuthMethod: self.clientResponse['token_endpoint_auth_method']
        };
        self.supportedGrantTypes = openidWellKnownConfiguration['grant_types_supported'];
        self.idTokenSigningAlgValuesSupported = openidWellKnownConfiguration['id_token_signing_alg_values_supported'];
        self.tokenEndpointAuthMethodsSupported = openidWellKnownConfiguration['token_endpoint_auth_methods_supported'];
        self.isLoading = false;
    }).catch(function() {
      self.isLoading = false;
      self.snackBar.open('An error occured while trying to get the client', 'Undo', {
        duration: 3000
      });
    });
  }
  handleRemoveRedirectUri(redirectUri) {
    const index = this.client.redirectUris.indexOf(redirectUri);
    if (index >= 0) {
      this.client.redirectUris.splice(index, 1);
    }
  }
  handleRemoveGrantType(grantType) {
    const index = this.client.grantTypes.indexOf(grantType);
    if (index >= 0) {
      this.client.grantTypes.splice(index, 1);
    }
  }
  handleAddCallback(event) {
    const input = event.input;
    const value = event.value;
    this.client.redirectUris.push(value);
    input.value = '';
  }
  handleAddGrantType(event) {
    this.client.grantTypes.push(event.option.viewValue);
  }
  save() {
    const self = this;
    self.isLoading = true;
    self.clientResponse['client_name'] = self.client.clientName;
    self.clientResponse['client_logo'] = self.client.clientLogo;
    self.clientResponse['application_type'] = self.client.applicationType;
    self.clientResponse['redirect_uris'] = self.client.redirectUris;
    self.clientResponse['grant_types'] = self.client.grantTypes;
    self.clientResponse['id_token_signed_response_alg'] = self.client.idTokenSignedResponseAlg;
    self.clientResponse['token_endpoint_auth_method'] = self.client.tokenEndpointAuthMethod;
    self.clientService.updateClient(self.clientResponse, self.type).then(function() {
      self.isLoading = true;
          self.snackBar.open('The client has been updated', 'Undo', {
            duration: 3000
          });
          self.refreshData();
    }).catch(function() {
          self.isLoading = false;
          self.snackBar.open('An error occured while trying to update the client', 'Undo', {
            duration: 3000
          });
    });
  }
}
