import { Component, OnInit, Input } from '@angular/core';
import { ClientService } from '../../../services/client.service';
import { ScopeService } from '../../../services/scope.service';
import { ActivatedRoute } from '@angular/router';
import { MatSnackBar, MatTableDataSource } from '@angular/material';

@Component({
 selector: 'app-client-scopes-detail',
 templateUrl: './scopes.component.html',
 styleUrls: ['./scopes.component.scss']
})
export class ScopesComponent implements OnInit {
 @Input() type;
 clientId: string;
 isLoading: boolean;
 clientResponse;
 displayedColumns;
 dataSource;
 constructor(
  private clientService: ClientService,
   private scopeService: ScopeService,
  private snackBar: MatSnackBar,
  private route: ActivatedRoute,
 ) {
 }

 ngOnInit() {
  const self = this;
   const params = self.route.parent.params;
   self.clientId = params['value']['id'];
   this.displayedColumns = [ 'isChecked', 'name', 'type', 'update_datetime' ];
   self.refreshData();
 }

 refreshData() {
  const self = this;
  self.isLoading = true;
   Promise.all([
    self.scopeService.getAllScopes(self.type),
    self.clientService.getClient(self.clientId, self.type) ]).then(function(values) {
      const scopeResponse = values[0];
      self.clientResponse = values[1];
      const allowedScopes = self.clientResponse['allowed_scopes'];
      const scopes = [];
      if (scopeResponse) {
        for (let i = 0; i < scopeResponse['length']; i++) {
          const s = scopeResponse[i];
          scopes.push({
          name: s['name'],
          type: s['is_openid_scope'] ? 'openid' : 'api',
          update_datetime: s['update_datetime'],
          isChecked: allowedScopes.filter(cs => cs === s['name']).length > 0
          });
        }
      }

      self.dataSource = new MatTableDataSource(scopes);
      self.isLoading = false;
   }).catch(function() {
     self.isLoading = false;
     self.snackBar.open('An error occured while trying to get the client', 'Undo', {
      duration: 3000
     });
   });
 }

 save() {
  const self = this;
  self.isLoading = true;
  self.clientResponse['allowed_scopes'] = self.dataSource.data.filter(r => r.isChecked).map(r => r.name);
  self.clientService.updateClient(self.clientResponse, self.type).then(function() {
   self.isLoading = true;
     self.snackBar.open('The client has been updated', 'Undo', {
      duration: 3000
     });
     self.refreshData();
  }).catch(function() {
     self.isLoading = false;
     self.snackBar.open('An error occured while trying to update the client', 'Undo', {
      duration: 3000
     });
  });
 }
}
