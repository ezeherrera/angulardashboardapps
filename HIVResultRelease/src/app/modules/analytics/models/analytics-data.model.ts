export class AnalyticsDataInput {
  // eventType: string;
  rangeStart?: Date;
  rangeEnd?: Date;
}

export class AnalyticsDataOutput {
  id: number;
  payload: AnalyticsDataPayload;
  eventType: string;
}

export class AnalyticsDataPayload {
  source: string;
  timeStamp: string;
  UserId?: string;
  errorMessage?: string;
  args?;
}
