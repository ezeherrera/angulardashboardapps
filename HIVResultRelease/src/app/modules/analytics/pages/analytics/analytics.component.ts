import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AnalyticsService } from '../../services/analytics.service';
import { SessionStorageService } from 'src/app/core/session-storage/session-storage.service';

@Component({
  selector: 'app-analytics',
  templateUrl: './analytics.component.html',
  styleUrls: ['./analytics.component.scss']
})
export class AnalyticsComponent implements OnInit {

  public startDate: Date;
  public endDate: Date;

  constructor(
    private service: AnalyticsService,
    private storage: SessionStorageService,
  ) { }

  ngOnInit() {
    if (this.storage.getData('startDate') && this.storage.getData('endDate')) {
      this.startDate = new Date(this.storage.getData('startDate'));
      this.endDate = new Date(this.storage.getData('endDate'));
    } else {
      if (!this.service.startDate || !this.service.endDate) {
        this.startDate = new Date();
        this.startDate.setDate(1);
        this.startDate.setMonth( this.startDate.getMonth() - 1 );
        this.endDate = new Date(this.service.formatDate(new Date(), true));
      } else {
        this.startDate = this.service.startDate;
        this.endDate = this.service.endDate;
      }
    }
    this.storage.setData('startDate', this.startDate.getTime());
    this.storage.setData('endDate', this.endDate.getTime());
    this.service.fetchData(this.startDate, this.endDate);
  }

  applyDateRange(f: NgForm): void {
    const { startDate, endDate } = f.value;
    this.startDate = startDate;
    this.endDate = endDate;
    this.storage.setData('startDate', startDate.getTime());
    this.storage.setData('endDate', endDate.getTime());
    this.service.fetchData(startDate, endDate);
  }

}
