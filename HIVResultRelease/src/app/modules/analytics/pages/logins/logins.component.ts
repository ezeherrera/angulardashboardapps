import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnalyticsDataPayload } from '../../models/analytics-data.model';
import { ChartsLineGraphModel } from '../../../../shared/charts/line/models/line.model';
import { AnalyticsService } from '../../services/analytics.service';
import { ChartsPaletteService } from '../../../../shared/charts/palette/palette.service';

@Component({
  selector: 'app-logins',
  templateUrl: './logins.component.html',
  styleUrls: ['./logins.component.scss']
})
export class LoginsComponent implements OnInit {

  data$: Observable<AnalyticsDataPayload[]>;
  data2$: Observable<AnalyticsDataPayload[]>;
  dataset: ChartsLineGraphModel;
  legend: object;
  loading: boolean;

  constructor(
    private service: AnalyticsService,
    private palette: ChartsPaletteService,
  ) { }

  ngOnInit(): void {
    this.data$ = this.service.getData('UserLoggedIn');
    this.data$.subscribe(
      fetchedData => {
        const title = '';
        const startDateObj = this.service.startDate;
        const endDateObj = this.service.endDate;
        const xAxisLabel = 'Month, Day';
        const yAxisLabel = 'Num. Success. Logins';
        this.dataset = new ChartsLineGraphModel(
          null,
          title,
          null,
          null,
          startDateObj,
          endDateObj,
          xAxisLabel,
          yAxisLabel
        );
        if (fetchedData.length) {
          const color = this.palette.getColors(2)[1];
          const { data, maxValue, minValue } = this.service.getParsedData(fetchedData, color);
          this.dataset.data = data;
          this.dataset.maxValue = maxValue;
          this.dataset.minValue = minValue;
          this.legend = {
            header: `From ${ this.service.labelDate(this.service.startDate) }
                    to ${ this.service.labelDate(this.service.endDate) }`,
            captions: [{
              caption: ` Successful Logins`,
              color: color,
              id: 0
            }]
          };
        }
        this.loading = this.service.loading;
      }
    );
    this.data2$ = this.service.getData('FailedLoginAttempts');
    this.data2$.subscribe(
      fetchedData => {
        if (fetchedData.length) {
          const color = this.palette.getColors(2)[0];
          const { data, maxValue, minValue } = this.service.getParsedData(fetchedData, color);
          this.dataset.data.push(data[0]);
          this.dataset.maxValue = (maxValue > this.dataset.maxValue) ? maxValue : this.dataset.maxValue;
          this.dataset.minValue = (minValue > this.dataset.minValue) ? minValue : this.dataset.minValue;
          this.legend['captions'].push( {
              caption: ` Failed Login Attempts`,
              color: color,
              id: 1
            });
        }
        this.loading = this.service.loading;
      }
    );
  }
}
