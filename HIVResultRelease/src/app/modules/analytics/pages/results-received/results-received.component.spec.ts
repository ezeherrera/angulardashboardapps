import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsReceivedComponent } from './results-received.component';

describe('ResultsReceivedComponent', () => {
  let component: ResultsReceivedComponent;
  let fixture: ComponentFixture<ResultsReceivedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsReceivedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsReceivedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
