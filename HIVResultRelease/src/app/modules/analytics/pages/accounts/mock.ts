const MOCKDATA = {
    startDate: '2016-01-18T00:00:00',
    endDate: '2018-02-05T00:00:00',
    maxValue: 35.53,
    minValue: 1.1,
    results: [
        [
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2016-01-18T00:00:00',
                value: 4.64,
                week: 4,
                year: 2016
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2017-01-02T00:00:00',
                value: 1.1,
                week: 2,
                year: 2017
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2017-12-04T00:00:00',
                value: 35.53,
                week: 50,
                year: 2017
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2017-12-11T00:00:00',
                value: 6.3,
                week: 51,
                year: 2017
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2017-12-18T00:00:00',
                value: 5.17,
                week: 52,
                year: 2017
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2017-12-25T00:00:00',
                value: 3.47,
                week: 53,
                year: 2017
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-01-01T00:00:00',
                value: 2.33,
                week: 1,
                year: 2018
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-01-08T00:00:00',
                value: 2.49,
                week: 2,
                year: 2018
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-01-15T00:00:00',
                value: 2.59,
                week: 3,
                year: 2018
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-01-22T00:00:00',
                value: 3.38,
                week: 4,
                year: 2018
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-01-29T00:00:00',
                value: 1.3,
                week: 5,
                year: 2018
            },
            {
                caption: 'Pre Analytics',
                order: 1,
                referenceDate: '2018-02-05T00:00:00',
                value: 2.79,
                week: 6,
                year: 2018
            }
        ],
        [
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2016-01-18T00:00:00',
                value: 0.69,
                week: 4,
                year: 2016
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2017-01-02T00:00:00',
                value: 0.69,
                week: 2,
                year: 2017
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2017-12-04T00:00:00',
                value: 0.69,
                week: 50,
                year: 2017
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2017-12-11T00:00:00',
                value: 0.69,
                week: 51,
                year: 2017
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2017-12-18T00:00:00',
                value: 0.69,
                week: 52,
                year: 2017
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2017-12-25T00:00:00',
                value: 0.69,
                week: 53,
                year: 2017
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-01-01T00:00:00',
                value: 0.69,
                week: 1,
                year: 2018
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-01-08T00:00:00',
                value: 0.69,
                week: 2,
                year: 2018
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-01-15T00:00:00',
                value: 0.69,
                week: 3,
                year: 2018
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-01-22T00:00:00',
                value: 0.69,
                week: 4,
                year: 2018
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-01-29T00:00:00',
                value: 0.69,
                week: 5,
                year: 2018
            },
            {
                caption: 'Preparation',
                order: 2,
                referenceDate: '2018-02-05T00:00:00',
                value: 0.69,
                week: 6,
                year: 2018
            }
        ],
        [
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2016-01-18T00:00:00',
                value: 1.25,
                week: 4,
                year: 2016
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2017-01-02T00:00:00',
                value: 1.36,
                week: 2,
                year: 2017
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2017-12-04T00:00:00',
                value: 0.68,
                week: 50,
                year: 2017
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2017-12-11T00:00:00',
                value: 0.98,
                week: 51,
                year: 2017
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2017-12-18T00:00:00',
                value: 0.71,
                week: 52,
                year: 2017
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2017-12-25T00:00:00',
                value: 0.71,
                week: 53,
                year: 2017
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-01-01T00:00:00',
                value: 1.11,
                week: 1,
                year: 2018
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-01-08T00:00:00',
                value: 0.98,
                week: 2,
                year: 2018
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-01-15T00:00:00',
                value: 0.77,
                week: 3,
                year: 2018
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-01-22T00:00:00',
                value: 0.73,
                week: 4,
                year: 2018
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-01-29T00:00:00',
                value: 0.71,
                week: 5,
                year: 2018
            },
            {
                caption: 'Transit',
                order: 3,
                referenceDate: '2018-02-05T00:00:00',
                value: 0.76,
                week: 6,
                year: 2018
            }
        ],
        [
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2016-01-18T00:00:00',
                value: 3.07,
                week: 4,
                year: 2016
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2017-01-02T00:00:00',
                value: 3.07,
                week: 2,
                year: 2017
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2017-12-04T00:00:00',
                value: 3.09,
                week: 50,
                year: 2017
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2017-12-11T00:00:00',
                value: 3.1,
                week: 51,
                year: 2017
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2017-12-18T00:00:00',
                value: 3.09,
                week: 52,
                year: 2017
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2017-12-25T00:00:00',
                value: 3.11,
                week: 53,
                year: 2017
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-01-01T00:00:00',
                value: 3.12,
                week: 1,
                year: 2018
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-01-08T00:00:00',
                value: 3.1,
                week: 2,
                year: 2018
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-01-15T00:00:00',
                value: 3.11,
                week: 3,
                year: 2018
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-01-22T00:00:00',
                value: 3.1,
                week: 4,
                year: 2018
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-01-29T00:00:00',
                value: 3.18,
                week: 5,
                year: 2018
            },
            {
                caption: 'Processing',
                order: 4,
                referenceDate: '2018-02-05T00:00:00',
                value: 3.11,
                week: 6,
                year: 2018
            }
        ],
        [
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2016-01-18T00:00:00',
                value: 3.96,
                week: 4,
                year: 2016
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2017-01-02T00:00:00',
                value: 6.95,
                week: 2,
                year: 2017
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2017-12-04T00:00:00',
                value: 2.35,
                week: 50,
                year: 2017
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2017-12-11T00:00:00',
                value: 1.73,
                week: 51,
                year: 2017
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2017-12-18T00:00:00',
                value: 9.31,
                week: 52,
                year: 2017
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2017-12-25T00:00:00',
                value: 3.66,
                week: 53,
                year: 2017
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-01-01T00:00:00',
                value: 1.69,
                week: 1,
                year: 2018
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-01-08T00:00:00',
                value: 2.77,
                week: 2,
                year: 2018
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-01-15T00:00:00',
                value: 2.33,
                week: 3,
                year: 2018
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-01-22T00:00:00',
                value: 2.19,
                week: 4,
                year: 2018
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-01-29T00:00:00',
                value: 1.07,
                week: 5,
                year: 2018
            },
            {
                caption: 'Time To Release',
                order: 5,
                referenceDate: '2018-02-05T00:00:00',
                value: 2.02,
                week: 6,
                year: 2018
            }
        ]
    ],
};

export default MOCKDATA;
