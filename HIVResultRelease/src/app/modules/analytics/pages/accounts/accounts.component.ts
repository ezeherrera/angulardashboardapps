import { Component, OnInit } from '@angular/core';
import { ChartsLineGraphModel } from 'src/app/shared/charts/line/models/line.model';
import { ChartsPaletteService } from 'src/app/shared/charts/palette/palette.service';
import { Observable } from 'rxjs';
import { AnalyticsDataPayload } from '../../models/analytics-data.model';
import { AnalyticsService } from '../../services/analytics.service';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {

  data$: Observable<AnalyticsDataPayload[]>;
  dataset: ChartsLineGraphModel;
  legend: object;
  loading: boolean;

  constructor(
    private service: AnalyticsService,
    private palette: ChartsPaletteService,
  ) {}

  ngOnInit(): void {
    this.loading = this.service.loading;
    this.data$ = this.service.getData('UserLoggedIn');
    this.data$.subscribe(
      fetchedData => {
        this.loading = this.service.loading;
        const title = '';
        const startDateObj = this.service.startDate;
        const endDateObj = this.service.endDate;
        const xAxisLabel = 'Month, Day';
        const yAxisLabel = 'Num. Accounts Created';
        this.dataset = new ChartsLineGraphModel(
          null,
          title,
          null,
          null,
          startDateObj,
          endDateObj,
          xAxisLabel,
          yAxisLabel
        );
        if (fetchedData.length) {
          const color = this.palette.getColors(2)[1];
          const { data, maxValue, minValue } = this.service.getParsedDataByUser(fetchedData, color);
          this.dataset.data = data;
          this.dataset.maxValue = maxValue;
          this.dataset.minValue = minValue;
          this.legend = {
            captions: [{
              caption: `Accounts created
                        from ${ this.service.labelDate(this.service.startDate) }
                        to ${ this.service.labelDate(this.service.endDate) }`,
              color: color,
              id: 0
            }]
          };
        }
        this.loading = this.service.loading;
      }
    );

  }
}


