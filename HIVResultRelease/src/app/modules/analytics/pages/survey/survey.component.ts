import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnalyticsDataPayload } from '../../models/analytics-data.model';
import { ChartsLineGraphModel } from 'src/app/shared/charts/line/models/line.model';
import { AnalyticsService } from '../../services/analytics.service';
import { ChartsPaletteService } from 'src/app/shared/charts/palette/palette.service';
import { ChartsLegendModel } from 'src/app/shared/charts/legend/models/legend.model';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  data$: Observable<AnalyticsDataPayload[]>;
  dataset: ChartsLineGraphModel;
  legend: ChartsLegendModel = new ChartsLegendModel();
  loading: boolean;

  constructor(
    private service: AnalyticsService,
    private palette: ChartsPaletteService,
  ) {
    this.loading = true;
  }

  ngOnInit(): void {
    this.loading = this.service.loading;
    this.data$ = this.service.getData('ExperienceRated');
    this.data$.subscribe(
      fetchedData => {
        const startDateObj = this.service.startDate;
        const endDateObj = this.service.endDate;
        const xAxisLabel = 'Month, Day';
        const yAxisLabel = 'Survey Rating';
        this.dataset = new ChartsLineGraphModel(
          null, null, null, null,
          startDateObj,
          endDateObj,
          xAxisLabel,
          yAxisLabel
        );
        this.legend.header = `From ${ this.service.labelDate(this.service.startDate) }
                              to ${ this.service.labelDate(this.service.endDate) }`;
        if (fetchedData.length) {
          const experiences = [];
          const byExperienceData = fetchedData.reduce(
            (currentArray, item) => {
              const idx = experiences.indexOf(item['experience']);
              if (idx < 0) {
                experiences.push(item['experience']);
                currentArray[experiences.length - 1] = [item];
              } else {
                currentArray[idx].push(item);
              }
              return currentArray;
            }, []);
          this.legend.captions = [];
          this.dataset.maxValue = 5;
          this.dataset.minValue = 1;
          this.dataset.data = [];
          byExperienceData.forEach( (experienceData, index, array) => {
            const color = this.palette.getColors(array.length)[index];
            const { data } = this.service.getParsedRatingData(experienceData, color);
            this.dataset.data.push(...data);
            this.legend.captions.push({
                caption: experienceData[0].experience,
                color: color,
                id: index
              });
          } );
        }
        this.loading = this.service.loading;
      }
    );
  }
}
