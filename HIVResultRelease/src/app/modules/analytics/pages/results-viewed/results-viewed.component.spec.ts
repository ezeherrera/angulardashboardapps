import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResultsViewedComponent } from './results-viewed.component';

describe('ResultsViewedComponent', () => {
  let component: ResultsViewedComponent;
  let fixture: ComponentFixture<ResultsViewedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResultsViewedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResultsViewedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
