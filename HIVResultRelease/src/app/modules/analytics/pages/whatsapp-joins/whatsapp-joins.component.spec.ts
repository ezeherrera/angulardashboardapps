import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatsappJoinsComponent } from './whatsapp-joins.component';

describe('WhatsappJoinsComponent', () => {
  let component: WhatsappJoinsComponent;
  let fixture: ComponentFixture<WhatsappJoinsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsappJoinsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatsappJoinsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
