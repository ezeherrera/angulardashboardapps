import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AnalyticsDataPayload } from '../../models/analytics-data.model';
import { ChartsLineGraphModel } from 'src/app/shared/charts/line/models/line.model';
import { AnalyticsService } from '../../services/analytics.service';
import { ChartsPaletteService } from 'src/app/shared/charts/palette/palette.service';

@Component({
  selector: 'app-whatsapp-joins',
  templateUrl: './whatsapp-joins.component.html',
  styleUrls: ['./whatsapp-joins.component.scss']
})
export class WhatsappJoinsComponent implements OnInit {

  data$: Observable<AnalyticsDataPayload[]>;
  dataset: ChartsLineGraphModel;
  legend: object;
  loading: boolean;

  constructor(
    private service: AnalyticsService,
    private palette: ChartsPaletteService,
  ) { }

  ngOnInit(): void {
    this.data$ = this.service.getData('WhatsappJoined');
    this.data$.subscribe(
      fetchedData => {
        const title = '';
        const startDateObj = this.service.startDate;
        const endDateObj = this.service.endDate;
        const xAxisLabel = 'Month, Day';
        const yAxisLabel = 'Num. Whatsapp Joins';
        this.dataset = new ChartsLineGraphModel(
          null,
          title,
          null,
          null,
          startDateObj,
          endDateObj,
          xAxisLabel,
          yAxisLabel
        );
        if (fetchedData.length) {
          const color = this.palette.getColors(2)[1];
          const { data, maxValue, minValue } = this.service.getParsedData(fetchedData, color);
          this.dataset.data = data;
          this.dataset.maxValue = maxValue;
          this.dataset.minValue = minValue;
          this.legend = {
            captions: [{
              caption: `Whatsapp Joins
                        from ${ this.service.labelDate(this.service.startDate) }
                        to ${ this.service.labelDate(this.service.endDate) }`,
              color: color,
              id: 0
            }]
          };
        }
        this.loading = this.service.loading;
      }
    );
  }
}
