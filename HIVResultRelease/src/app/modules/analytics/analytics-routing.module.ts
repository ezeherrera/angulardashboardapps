import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AnalyticsComponent } from './pages/analytics/analytics.component';
import { AuthGuard } from 'src/app/core/guards/auth.guard';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { LoginsComponent } from './pages/logins/logins.component';
import { ScansComponent } from './pages/scans/scans.component';
import { ResultsReceivedComponent } from './pages/results-received/results-received.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { ResultsViewedComponent } from './pages/results-viewed/results-viewed.component';
import { WhatsappJoinsComponent } from './pages/whatsapp-joins/whatsapp-joins.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SurveyComponent } from './pages/survey/survey.component';

const routes: Routes = [
  {
    path: 'analytics',
    canActivate: [AuthGuard],
    component: AnalyticsComponent,
    data: {
      title: 'Analytics',
    },
  children: [
    {
      path: '',
      canActivate: [AuthGuard],
      component: DashboardComponent,
      data: {
        title: 'Dashboard',
        },
    },
    {
      path: 'accounts',
      canActivate: [AuthGuard],
      component: AccountsComponent,
      data: {
        title: 'Accounts',
        },
    },
    {
      path: 'logins',
      canActivate: [AuthGuard],
      component: LoginsComponent,
      data: {
        title: 'Logins',
        },
    },
    {
      path: 'scans',
      canActivate: [AuthGuard],
      component: ScansComponent,
      data: {
        title: 'Scans',
        },
    },
    {
      path: 'results-received',
      canActivate: [AuthGuard],
      component: ResultsReceivedComponent,
      data: {
        title: 'Results received',
        },
    },
    {
      path: 'notifications',
      canActivate: [AuthGuard],
      component: NotificationsComponent,
      data: {
        title: 'Notifications',
        },
    },
    {
      path: 'results-viewed',
      canActivate: [AuthGuard],
      component: ResultsViewedComponent,
      data: {
        title: 'Results viewed',
        },
    },
    {
      path: 'whatsapp',
      canActivate: [AuthGuard],
      component: WhatsappJoinsComponent,
      data: {
        title: 'Whatsapp Joins',
        },
    },
    {
      path: 'survey',
      canActivate: [AuthGuard],
      component: SurveyComponent,
      data: {
        title: 'Likert Survey',
        },
    },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AnalyticsRoutingModule {}
