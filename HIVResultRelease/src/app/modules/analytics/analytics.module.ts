import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnalyticsRoutingModule } from './analytics-routing.module';
import { AnalyticsComponent } from './pages/analytics/analytics.component';
import { AnalyticsService } from './services/analytics.service';
import { AccountsComponent } from './pages/accounts/accounts.component';
import { LoginsComponent } from './pages/logins/logins.component';
import { ScansComponent } from './pages/scans/scans.component';
import { ResultsReceivedComponent } from './pages/results-received/results-received.component';
import { NotificationsComponent } from './pages/notifications/notifications.component';
import { ResultsViewedComponent } from './pages/results-viewed/results-viewed.component';
import { WhatsappJoinsComponent } from './pages/whatsapp-joins/whatsapp-joins.component';
import { MaterialModule } from '../../shared/material/material.module';
import { ChartsModule } from '../../shared/charts/charts.module';
import { FormsModule } from '@angular/forms';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SurveyComponent } from './pages/survey/survey.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    ChartsModule,
    AnalyticsRoutingModule,
    FormsModule,
  ],
  declarations: [
    AnalyticsComponent,
    AccountsComponent,
    LoginsComponent,
    ScansComponent,
    ResultsReceivedComponent,
    NotificationsComponent,
    ResultsViewedComponent,
    WhatsappJoinsComponent,
    SurveyComponent,
    DashboardComponent
  ],
  providers: [
    AnalyticsService,
  ],
})
export class AnalyticsModule { }
