import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AnalyticsDataOutput } from '../models/analytics-data.model';
import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/core/authentication/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  httpOptions;
  constructor(
    private http: HttpClient,
    authService: AuthenticationService
  ) {}

  // GET: /sites
  public get(start, end): Observable<AnalyticsDataOutput[]> {
    const dateFrom = new Date(start).getTime() / 1000;
    const dateTo = new Date(end).getTime() / 1000;
    const url = environment.apiAnalyticsServiceUrl + '/events/search?dateFrom=' + dateFrom + '&dateTo=' + dateTo;
    return this.http.get<AnalyticsDataOutput[]>(url).pipe(map(t => t));
  }

}
