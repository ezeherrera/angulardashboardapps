import { Injectable } from '@angular/core';
import { AnalyticsDataOutput, AnalyticsDataPayload } from '../models/analytics-data.model';
import { ApiService } from './api.service';
import { map } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { ChartsLineGraphDataModel } from 'src/app/shared/charts/line/models/line.model';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  public startDate: Date;
  public endDate: Date;
  public data: BehaviorSubject<AnalyticsDataOutput[]> = new BehaviorSubject<AnalyticsDataOutput[]>([]);
  public loading: boolean;

  constructor(
    private apiService: ApiService,
    private snackBar: MatSnackBar,
  ) {
    this.loading = true;
  }

  public setDateRange(start, end): void {
    this.startDate = start;
    this.endDate = end;
  }

  public fetchData(start, end) {
    this.setDateRange(start, end);
    this.loading = true;
    this.data.next([]);
    this.apiService.get(start, end).subscribe(
      (data) => {
        this.loading = false;
        return this.data.next(data);
      },
      error => {
        if (error.status === 401) {
          this.loading = false;
          this.snackBar.open('You are unauthorized to request this data. Please check your permissions with the Administrator.', 'Ok', {
            duration: 3000
          });
        }
        return this.data.next([]);
      }
    );
  }

  public getData(eventType: string): Observable<AnalyticsDataPayload[]> {
    return this.data.pipe(
      map( data => data.filter( item => item.eventType === eventType) ),
      map( data => data.map(item => item.payload)),
      map( data => data.sort((a, b) => {
        const dateA = new Date(a.timeStamp);
        const dateB = new Date(b.timeStamp);
        return (dateA > dateB) ? 1 : (dateA < dateB) ? -1 : 0;
      })),
      map( data => data.filter(
        item => {
          const date = new Date(item.timeStamp);
          return this.formatDate(date) >= this.formatDate(this.startDate) && this.formatDate(date) <= this.formatDate(this.endDate);
        })
      ),
    );
  }

  public getParsedDataByUser(rawData, color) {
    const uniqueIds = [];
    const uniqueDates = [];
    const data: ChartsLineGraphDataModel = {
      caption: null,
      fill: color,
      values: []
    };
    let maxValue = null;
    const minValue = 1;
    data.values = rawData.reduce( (parsedData, item, index) => {
      if ( uniqueIds.indexOf(item.source) < 0 ) {
        uniqueIds.push(item.source);
        const date = new Date(item.timeStamp);
        const dateId = uniqueDates.indexOf(this.formatDate(date));
        if ( dateId < 0 ) {
          uniqueDates.push(this.formatDate(date));
          const value = {
            date: new Date(this.formatDate(date, true)),
            value: 1,
            label: this.labelDate(date),
            };
          parsedData.push(value);
          maxValue = maxValue || 1;
        } else {
          parsedData[dateId].value += 1;
          maxValue = (parsedData[dateId].value > maxValue) ? parsedData[dateId].value : maxValue;
        }
      }
      return parsedData;
    }, []);
    return { data: [data], maxValue, minValue };
  }

  public getParsedData(rawData, color) {
    const uniqueDates = [];
    const data: ChartsLineGraphDataModel = {
      caption: null,
      fill: color,
      values: []
    };
    let maxValue = null;
    const minValue = 1;
    data.values = rawData.reduce( (parsedData, item, index) => {
      const date = new Date(item.timeStamp);
      const dateId = uniqueDates.indexOf(this.formatDate(date));
      if ( dateId < 0 ) {
        uniqueDates.push(this.formatDate(date));
        const value = {
          date: new Date(this.formatDate(date, true)),
          value: 1,
          label: this.labelDate(date),
          };
        parsedData.push(value);
        maxValue = maxValue || 1;
      } else {
        parsedData[dateId].value += 1;
        maxValue = (parsedData[dateId].value > maxValue) ? parsedData[dateId].value : maxValue;
      }
      return parsedData;
    }, []);
    return { data: [data], maxValue, minValue };
  }

  public getParsedRatingData(rawData, color) {
    const uniqueDates = [];
    const avgByDate = [];
    const data: ChartsLineGraphDataModel = {
      caption: null,
      fill: color,
      values: []
    };
    data.values = rawData.reduce( (parsedData, item, index) => {
      const date = new Date(item.timeStamp);
      const dateId = uniqueDates.indexOf(this.formatDate(date));
      if ( dateId < 0 ) {
        uniqueDates.push(this.formatDate(date));
        avgByDate.push({ total: item.rating, sources: 1 });
        const value = {
          date: new Date(this.formatDate(date, true)),
          value: item.rating,
          label: this.labelDate(date) + ', 1 source',
          };
        parsedData.push(value);
      } else {
        avgByDate[dateId].total += item.rating;
        avgByDate[dateId].sources++;
        parsedData[dateId].value = Math.round( avgByDate[dateId].total / avgByDate[dateId].sources * 10) / 10;
        parsedData[dateId].label = this.labelDate(date) + `, ${avgByDate[dateId].sources} sources`;
      }
      return parsedData;
    }, []);
    data.values.sort((a, b) => {
      if (a.date.getTime() > b.date.getTime()) {
        return 1;
      }
      if (a.date.getTime() < b.date.getTime()) {
        return -1;
      }
      return 0;
    });
    return { data: [data] };
  }

  public formatDate = (date: Date, dashes?: Boolean) => {
    const yyyy = date.getFullYear().toString();
    const mm = (date.getMonth() + 1).toString();
    const dd  = date.getDate().toString();
    return dashes
            ? yyyy + '-' + ( mm[1] ? mm : '0' + mm[0] ) + '-' + (dd[1] ? dd : '0' + dd[0]) + ' 00:00:00'
            : yyyy + ( mm[1] ? mm : '0' + mm[0] ) + (dd[1] ? dd : '0' + dd[0]);
  }

  public labelDate = (date: Date) => {
    const dayNames = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
    const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    const day = dayNames[date.getDay()];
    const yy = date.getFullYear().toString();
    const mm = monthNames[date.getMonth()];
    const dd  = date.getDate().toString();
    return `${day} ${dd} ${mm} ${yy}`;
  }

}
