import { AnalyticsRoutingModule } from './analytics-routing.module';

describe('AnalyticsRoutingModule', () => {
  let analyticsRoutingModule: AnalyticsRoutingModule;

  beforeEach(() => {
    analyticsRoutingModule = new AnalyticsRoutingModule();
  });

  it('should create an instance', () => {
    expect(analyticsRoutingModule).toBeTruthy();
  });
});
