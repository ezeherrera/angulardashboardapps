/*  core modules */
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './/app-routing.module';

/*  app modules */
import { LoginModule } from './modules/login/login.module';

/*  app root component */
import { AppComponent } from './app.component';

/* Services */
import { environment } from '../environments/environment';
import { OAuthModule } from 'angular-oauth2-oidc';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './shared/material/material.module';
import { SvgIconModule } from './shared/svg-icon/svg-icon.module';
import { MessageDialogModule } from './shared/message-dialog/message-dialog.module';
import { NavigationModule } from './core/navigation/navigation.module';
import { HeaderComponent } from './core/header/header.component';
import { SidenavComponent } from './core/sidenav/sidenav.component';
import { NavigationService } from './core/navigation/services/navigation.service';
import { ResultsReleaseModule } from './modules/results-release/results-release.module';
import { AnalyticsModule } from './modules/analytics/analytics.module';
import { AdminModule } from './modules/admin/admin.module';

/***
 * The bootstraping module
 ***/
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    OAuthModule.forRoot({
      resourceServer: {
          allowedUrls: [
            environment.apiResultsServiceUrl,
            environment.apiAnalyticsServiceUrl,
            environment.apiOpenidManagerUrl,
            environment.apiAuthManagerUrl,
          ],
          sendAccessToken: true
      }
    }),
    MaterialModule,
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    NavigationModule,
    LoginModule,
    ResultsReleaseModule,
    AnalyticsModule,
    AdminModule,
    AppRoutingModule,
    MessageDialogModule,
    SvgIconModule,
  ],
  providers: [
    NavigationService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
