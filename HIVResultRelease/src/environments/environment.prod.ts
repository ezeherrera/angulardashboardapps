export const environment = {
  title: 'Ops Center',

  production: true,
  settingsFileUrl: '/assets/settings.json',
  localeFileUrl: '/assets/locales/messages.${locale}.xlf',
  clientId: 'ReportingDashboard',
  redirectUri: 'https://resultrelease.projectschweitzer.net/callback',
  issuer: 'https://identity.projectschweitzer.net',
  responseType: 'id_token code',
  scope: 'openid role',
  responseMode: 'fragment',
  state: '75BCNvRlEGHpQRCT',
  nonce: 'nonce',
  disableAtHashCheck: true,
  requireHttps: false,
  showDebugInformation: true,

  apiResultsServiceUrl: 'https://results.projectschweitzer.net',
  apiAnalyticsServiceUrl: 'https://analytics.projectschweitzer.net',
  apiOpenidManagerUrl: 'https://adminui.projectschweitzer.net/openidmanager',
  apiAuthManagerUrl: 'https://adminui.projectschweitzer.net/oauthmanager'
};
