// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.service.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  title: 'Ops Center (Test)',

  production: false,
  localeFileUrl: '/assets/locales/messages.${locale}.xlf',
  settingsFileUrl: '/assets/settings.json',
  clientId: 'ReportingDashboard',
  redirectUri: 'https://staging-resultrelease.projectschweitzer.net/callback',
  issuer: 'https://identitystaging.projectschweitzer.net',
  responseType: 'id_token code',
  scope: 'openid role',
  responseMode: 'fragment',
  state: '75BCNvRlEGHpQRCT',
  nonce: 'nonce',
  disableAtHashCheck: true,
  requireHttps: false,
  showDebugInformation: true,

  apiResultsServiceUrl: 'https://staging-results.projectschweitzer.net',
  apiAnalyticsServiceUrl: 'https://staging-analytics.projectschweitzer.net',
  apiOpenidManagerUrl: 'https://adminuistaging.projectschweitzer.net/openidmanager',
  apiAuthManagerUrl: 'https://adminuistaging.projectschweitzer.net/oauthmanager',
};
