# HivMonitor Result Release

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.

# Local SetUp and Run
```
git clone https://inst-arch.visualstudio.com/DefaultCollection/HivMonitor/_git/ResultRelease
cd ResultRelease
npm i
ng serve -o
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.